class InteractionPage {

    constructor(map, config,popup) {

        let self = this;
        this._map = map;
        this._config = config;
        this.interactionPopup = popup;

        this.tabs = [].slice.call(document.getElementsByClassName('switch-content'));
        this.containers = [].slice.call(document.getElementsByClassName('tab-container'));
        this.map = document.getElementById('map-container');

        this.tabs.forEach( (t) => {

            t.addEventListener("click", function () {
                self._switch(t.getAttribute('data-content'));
            }, false);

        })
    }

    _switch(tabSlug) {



        for (let container of this.containers) {
            container.style.display = 'none';
        }

        for (let tab of this.tabs) {
            tab.classList.remove('active');
        }

        this.containers.find( (c) => {
            return c.getAttribute('data-content') === tabSlug;
        }).style.display = 'flex';

        this.tabs.find( (t) => {
            return t.getAttribute('data-content') === tabSlug;
        }).classList.add('active');



        if (tabSlug === 'future' || tabSlug === 'under_construction') {

            this.map.remove();
         //   map = document.getElementById('map-container');

            this.containers.find( (c) => {
                return c.getAttribute('data-content') === tabSlug;
            }).appendChild(this.map);
        }

        if (tabSlug === 'planning') {

            document.getElementById('datavisplanning').innerHTML = '';
            new window.vraGraphModule.InitGraph();
        }
    }
}

const lichtgrijs = 'rgb(230,230,230)';
const grijs = 'rgb(220,220,220)';
const donkergrijs = 'rgb(100,100,100)';
const zwart = 'rgb(51,51,51)';
const wit = 'rgb(255,255,255)';

const lichtgeel = 'rgb(255,224,99)';
const geel = 'rgb(255,203,0)';
const donkergeel = 'rgb(248,175,0)';
const lichtoranje = 'rgb(242,145,0)';
const oranje = 'rgb(237,114,3)';
const lichtrood = 'rgb(231,78,15)';
const rood =  'rgb(229,50,18)';
const donkerrood = 'rgb(209,10,17)';
const lichtblauw = 'rgb(156,198,212)';
const blauw = 'rgb(0,154,200)';
const donkerblauw = 'rgb(0,111,157)';
const navy = 'rgb(0,85,120)';

const paars = 'rgb(184,21,112)';

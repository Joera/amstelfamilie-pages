class Map {

    constructor(element, data, renderEnv) {

        this.element = document.getElementById(element);
        this.renderEnv = renderEnv;

        if(!this.element) {
            return;
        }

        try {
            let dataConfig = this.element.getAttribute('data-config');
            this.customConfig = JSON.parse(dataConfig);
        } catch(err) {
            this.customConfig = [];
            console.log(err);
        }

        let self = this,
            activeItems = [];

        switch (renderEnv) {

            case 'amstelveenlijn' :

                this.config = {
                    accessToken: 'pk.eyJ1Ijoid2lqbmVtZW5qZW1lZSIsImEiOiJjaWgwZjB4ZGwwMGdza3FseW02MWNxcmttIn0.l-4VI25pfA5GKukRQTXnWA',
                    style: 'mapbox://styles/wijnemenjemee/cixls3wf0001o2ro0xjvltjaj',
                    hostContainer: element,
                    center: [4.875, 52.3],
                    zoom: 13,
                    pitch: 0,
                    bearing: 0,
                    scrollZoom: false,
                    interactive: true
                };
                break;

            case 'uithoornlijn' :

                this.config = {
                    accessToken: 'pk.eyJ1Ijoid2lqbmVtZW5qZW1lZSIsImEiOiJjaWgwZjB4ZGwwMGdza3FseW02MWNxcmttIn0.l-4VI25pfA5GKukRQTXnWA',
                    style: 'mapbox://styles/wijnemenjemee/cjx251nik0ass1cp5nhkapnuo',
                    hostContainer: element,
                    center: [4.8225, 52.25],
                    zoom: 13.2,
                    pitch: 0,
                    bearing: 0,
                    scrollZoom: false,
                    interactive: true
                };
                break;
        }

        this.options = {
            // interaction: interaction,
            filters : true,
            referenceList : true
        };

        //data from argument in footer scripts (datasets)
        if (data) {
            this.config.dataset = JSON.parse(data);
        } else {
            this.config.dataset = {};
        }

       // self.init()

    }


    init() {

        let self = this;

        if(webgl_detect()) {

            console.log('has_web_gl');

            const mapWebGL = new MapWebGL(self.config);

            self._map = mapWebGL.create();

            self._lines = new Lines(self._map, self.config, self.renderEnv);
            self._points = new Points(self._map, self.config);
            self._haltes = new Haltes(self._map, self.config);
            self._works = new Works(self._map, self.config);
            self._interactionPopup = new InteractionPopup();
            self.interactionPage = new InteractionPage(self._map, self.config);

            self._map.on('style.load', function () {

               self._lines.init();

               if (self.renderEnv === 'uithoornlijn') {

                   self._haltes.init();
                   self._works.init(self._interactionPopup);
               //
               } else if (self.config.dataset && self.config.dataset.features.length > 0) {
                    self._points.draw(self.config.dataset,self.renderEnv);
               }
            });
        } else {
            console.log('no web gl allowed')
        }
    }

    center(workSlug,lng,lat) {

        this._map.flyTo({
            center: [
                lng,
                lat
            ],
            essential: false // this animation is considered essential with respect to prefers-reduced-motion
        });

        // andere kleur
  //      this._map.setFilter('works-active', ['==','slug',workSlug]);


    }
}

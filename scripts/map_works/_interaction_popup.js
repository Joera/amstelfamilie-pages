class InteractionPopup {

    constructor() {
        // this._map = map;
        // this._config = config;
        this.popup = null;
        this.popups = [];
    }

    createPopup(item) {

        let url;

        return `<div>
                    <h3>
                        ` + item.properties.name + `
                    </h3>
                    <span class="datum">` + moment(item.properties.startDate).format('DD-MM-YYYY') + `</span>
                     - <span class="datum">` + moment(item.properties.endDate).format('DD-MM-YYYY') + `</span>
               </div>`;

    }

    openPopup(map,html,lngLat,type,offset,closeOthers) {

        let self = this;
        if (closeOthers && self.popup) { self.popup.remove(); }
        self.popup = new mapboxgl.Popup({offset:[0,20],anchor:'top',closeButton:false})
            .setLngLat(lngLat)
            .setHTML(html)
            .addTo(map);
        self.popup._container.classList.add(type);

        self.popups.push(self.popup);
    }

    closePopup() {

        let self = this;
        if (self.popup) {
            self.popup.remove();
        }
    }

    // closeAllPopups(map) {
    //
    //
    //     // loop door lagen
    //     // verzamel iconen
    //     // verzamel open popups
    //
    //     let popup,
    //         activeIcons = map.queryRenderedFeatures({ layers: ['icons-active'] });
    //
    //     activeIcons.forEach( function(icon) {
    //
    //      //   popup = icon.getPopup();
    //         console.log(icon);
    //     })
    //
    // }


    // addToArray(popup) {
    //
    //     let self = this;
    //     self.popups.push(self.popup);
    // }
    //
    // emptyArray(map) {
    //
    //     let self = this;
    //
    //     if (webgl_detect() && !isIE11()) {
    //
    //         self.popups.forEach(function (popup) {
    //
    //             popup.remove();
    //         });
    //
    //     }
    //
    //     else {
    //
    //         map.closePopup();
    //
    //     }
    // }

}

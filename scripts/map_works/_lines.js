
class Lines {

    constructor(map,config,render_environment) {

        this._map = map;
        this._config = config;

        this.speedFactor = 30; // number of frames per longitude degree
        this.animation = ''; // to store and cancel the animation
        this.startTime = 0;
        this.progress = 0; // progress = timestamp - startTime
        this.resetTime = false; // indicator of whether time reset is needed for the animation
        this.layerId = (render_environment === 'amstelveenlijn') ? 'cjc4zc40d13wa2wqskv9bk020' : 'cjx2un0mm0eoi2or5sh8tc7fe';
    }

    init() {

        let self = this;
        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://api.mapbox.com/datasets/v1/wijnemenjemee/' + self.layerId + '/features?access_token=' + self._config.accessToken);
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {

                    let data = JSON.parse(xhr.response);

                    data.features = data.features.filter( (feature) => {
                        // console.log(feature.geometry.type);
                        return feature.geometry.type == 'LineString';
                    });
                    self._draw(data);
                }
            }
        }
    }

    _draw(lines) {

        let self = this;

        self._map.addSource("lines", {
            "type": "geojson",
            "data": lines
        });

        self._map.addLayer({
            "id": "Amstelveenlijn",
            "type": "line",
            "source": "lines",
            "layout": {
                "line-join": "miter",
                "line-cap": "square"
            },
            "paint": {
                "line-color": {
                    property: 'name',
                    type: 'categorical',
                    stops: [
                        ['amstelveenlijn', '#000'],
                        ['uithoornlijn', geel]
                    ]
                },
                "line-width": 8,
                "line-dasharray": [.5,.5]
            },
        },'haltes');
    }
}

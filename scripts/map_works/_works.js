
class Works {

    constructor(map,config) {

        this._map = map;
        this._config = config;
    }

    init(interactionPopup) {

        let self = this;
        let xhr = new XMLHttpRequest();
        let url = 'https://amsteltram.nl/assets/geojson/uithoornlijn_works.geojson';
        xhr.open('GET', url);
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {

                    let data = JSON.parse(xhr.response);
                    let now = new Date();
                    // filter actueel
                    data.features = data.features.filter( (feature) => {
                        return new Date(feature.properties.endDate) > now;
                    });

                    for (let feature of data.features) {
                        feature.properties.timeCategory = (new Date(feature.properties.endDate) > now && now > new Date(feature.properties.startDate)) ? 'now' : 'future';
                    }

                    self._draw(data,'uithoornlijn',interactionPopup);
                }
            }
        }




    }

    _draw(points,renderEnv,interactionPopup) {

        let self = this;

        self._map.addSource("works", {
            "type": "geojson",
            "data": points
        });

        self._map.addLayer({
            "id": "works",
            "type": "circle",
            "source": "works",
            "paint": {
                "circle-color": {
                    property: 'timeCategory',
                    type: 'categorical',
                    stops: [['now', paars], ['future', donkerblauw]]
                },
                "circle-radius": 16,
                "circle-opacity": 1,
            }
        });

        self._map.addLayer({
            "id": "works-active",
            "type": "circle",
            "source": "works",
            "paint": {
                "circle-color": blauw,
                "circle-radius": 10,
                "circle-opacity": 1,
                "circle-stroke-width": 6,
                "circle-stroke-color": blauw,
                "circle-stroke-opacity": 1
            },
            "filter": ['all',
                ["==", 'slug', '']
            ]
        });


        // self._map.addLayer({
        //     "id": "worklabels",
        //     "type": "symbol",
        //     "source": "works",
        //     "layout": {
        //         "text-font": ["Cabrito Sans W01 Norm Bold"],
        //         "text-field": "{label}",
        //         "symbol-placement": "point",
        //         "text-size": 20,
        //         "text-anchor": "left",
        //         "text-offset": [1.5, 0],
        //         "text-max-width": 30
        //     },
        //     "paint": {
        //         'text-color': '#000000'
        //     }
        // });

        if(!isIE11()) {

            self._map.addLayer({
                "id": "workicons",
                "type": "symbol",
                "source": "works",
                "layout": {
                    "visibility": "visible",
                    "icon-image": {
                        property: 'type',
                        type: 'categorical',
                        stops: [['vegetatie', "works-trees"], ['water', "works-water"], ['kabels', "works-cables"], ['voorbereiding', "works_preparation"]]
                    },
                    "icon-padding": 0,
                    "icon-text-fit": 'both',
                    "icon-allow-overlap": true
                }
            });
        }

        self._map.on("mouseenter", "works", function (e) {

            if (e.features.length > 0) {

                self._map.getCanvas().style.cursor = 'pointer';

                let html = interactionPopup.createPopup(e.features[0]);
                let offset = [0, -20];
                let lngLat = e.features[0].geometry.coordinates;
                let type = e.features[0].properties.type;

                interactionPopup.openPopup(self._map, html, lngLat, type, offset);
            }
        });

        self._map.on('mouseleave', 'works', function () {

            self._map.getCanvas().style.cursor = '';
            interactionPopup.closePopup();
        });

    }
}

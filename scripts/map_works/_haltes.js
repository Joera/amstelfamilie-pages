
class Haltes {

    constructor(map,config) {

        this._map = map;
        this._config = config;
    }

    init() {

        let self = this;
        let xhr = new XMLHttpRequest();
        let url = 'https://amsteltram.nl/assets/geojson/uithoornlijn_locations.geojson';
        xhr.open('GET', url);
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {

                    let data = JSON.parse(xhr.response);

                    // data.features = data.features.filter( (feature) => {
                    //     // console.log(feature.geometry.type);
                    //     return feature.geometry.type == 'LineString';
                    // });
                    self._draw(data);
                }
            }
        }


    }

    _draw(points,renderEnv) {

        let self = this;

        self._map.addSource("haltes", {
            "type": "geojson",
            "data": points
        });

        self._map.addLayer({
            "id": "haltes",
            "type": "circle",
            "source": "haltes",
            "paint": {
                "circle-color": "#fff",
                "circle-radius": 6,
                "circle-opacity": 1,
                "circle-stroke-width": 6,
                "circle-stroke-color": {
                    property: 'mainType',
                    type: 'categorical',
                    stops: [['kruispunt', navy], ['stop', donkergeel],['werkzaamheden', rood]]
                },
                "circle-stroke-opacity": 1
            },
            "filter": ["==", "mainType", "stop"]
        },'works');


        self._map.addLayer({
            "id": "labels",
            "type": "symbol",
            "source": "haltes",
            "layout": {
                "text-font": ["Cabrito Sans W01 t Book Extended"],
                "text-field": "{name}",
                "symbol-placement": "point",
                "text-size": 20,
                "text-anchor": "left",
                "text-offset": [1.5, 0],
                "text-max-width": 30
            },
            "paint": {
                'text-color': '#000000'
            },
            "filter": ["==", "mainType", "stop"]
        });

        // self._map.addLayer({
        //     "id": "crossings",
        //     "type": "symbol",
        //     "source": "stops",
        //     "layout": {
        //         "visibility": "visible",
        //         "icon-image": "crossing",
        //         "icon-padding": 0,
        //         "icon-text-fit": 'both',
        //         "icon-allow-overlap": true
        //     },
        //     "filter": ["==", "crossing", true]
        // }, 'stops');

    }
}

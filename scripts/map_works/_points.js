
class Points {

    constructor(map,config) {

        this._map = map;
        this._config = config;
    }

    draw(points,renderEnv) {

        self._map.addSource("stops", {
            "type": "geojson",
            "data": points
        });

        if (render_environment === 'amstelveenlijn') {

            self._map.addLayer({
                "id": "stops",
                "type": "circle",
                "source": "stops",
                "paint": {
                    "circle-color": "#fff",
                    "circle-radius": 6,
                    "circle-opacity": 1,
                    "circle-stroke-width": 6,
                    "circle-stroke-color": {
                        property: 'stopType',
                        type: 'categorical',
                        stops: [['remove', donkerrood], ['inherit', donkergeel], ['nieuw', donkergeel], ['merge', navy]]
                    },
                    "circle-stroke-opacity": 1
                }
            });
        }

        if (render_environment === 'uithoornlijn') {

            self._map.addLayer({
                "id": "stops",
                "type": "circle",
                "source": "stops",
                "paint": {
                    "circle-color": "#fff",
                    "circle-radius": 6,
                    "circle-opacity": 1,
                    "circle-stroke-width": 6,
                    "circle-stroke-color": {
                        property: 'mainType',
                        type: 'categorical',
                        stops: [['kruispunt', navy], ['stop', donkergeel],['werkzaamheden', rood]]
                    },
                    "circle-stroke-opacity": 1
                }
            });
        }

        self._map.addLayer({
            "id": "labels",
            "type": "symbol",
            "source": "stops",
            "layout": {
                "text-font": ["Cabrito Semi W01 Norm E ExtraBold"],
                "text-field": "{name}",
                "symbol-placement": "point",
                "text-size": 20,
                "text-anchor": "left",
                "text-offset": [1.5, 0],
                "text-max-width": 30
            },
            "paint": {
                'text-color': (render_environment === 'amstelveenlijn') ? '#ffffff' : '#000000'
            }
        });

        self._map.addLayer({
            "id": "crossings",
            "type": "symbol",
            "source": "stops",
            "layout": {
                "visibility": "visible",
                "icon-image": "crossing",
                "icon-padding": 0,
                "icon-text-fit": 'both',
                "icon-allow-overlap": true
            },
            "filter": ["==", "crossing", true]
        }, 'stops');

    }
}

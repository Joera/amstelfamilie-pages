
class Points {

    constructor(map,config) {

        this._map = map;
        this._config = config;
    }

    draw(points,renderEnv) {

        let self = this;


        if (renderEnv != 'amstelveenlijn') {

            points.features = points.features.filter( (f) => {
               return f.properties.stopType !== 'remove' && f.properties.slug !== 'opstelterrein' && f.properties.slug !== 'stadshart-tram-5';
           });
        }


        self._map.addSource("stops", {
            "type": "geojson",
            "data": points
        });


        // if (renderEnv == 'amstelveenlijn') {
        //
        //     self._map.addLayer({
        //         "id": "stops",
        //         "type": "circle",
        //         "source": "stops",
        //         "paint": {
        //             "circle-color": "#fff",
        //             "circle-radius": 6,
        //             "circle-opacity": 1,
        //             "circle-stroke-width": 6,
        //             "circle-stroke-color": {
        //                 property: 'stopType',
        //                 type: 'categorical',
        //                 stops: [
        //                     ['remove', donkerrood],
        //                     ['inherit', donkerrood],
        //                     ['merge', donkerrood]
        //                 ]
        //             },
        //             "circle-stroke-opacity": 1
        //         }
        //     });
        //
        // } else {

            self._map.addLayer({
                "id": "stops",
                "type": "circle",
                "source": "stops",
                "paint": {
                    "circle-color": "#fff",
                    "circle-radius": 6,
                    "circle-opacity": 1,
                    "circle-stroke-width": 6,
                    "circle-stroke-color": {
                        property: 'renderEnv',
                        type: 'categorical',
                        stops: [
                            ['amstelveenlijn', donkerrood],
                            ['uithoornlijn', donkergeel]
                        ]
                    },
                    "circle-stroke-opacity": 1
                }
            });

        // }

        self._map.addLayer({
            "id": "labels",
            "type": "symbol",
            "source": "stops",
            "layout": {
                "text-font": ["Cabrito Sans W01 Norm Bold"],
                "text-field": "{name}",
                "symbol-placement": "point",
                "text-size": 18,
                "text-anchor": {
                    property: 'slug',
                    type: 'categorical',
                    stops: [
                        ['station-zuid', 'right'],
                        ['a-j-ernststraat', 'left'],
                        ['van-boshuizenstraat', 'right'],
                        ['uilenstede', 'right'],
                        ['kronenburg', 'left'],
                        ['zonnestein', 'left'],
                        ['onderuit', 'left'],
                        ['oranjebaan', 'right'],
                        ['ouderkerkerlaan', 'right'],
                        ['sportlaan', 'right'],
                        ['meent', 'right'],
                        ['brink', 'right'],
                        ['poortwachter', 'right'],
                        ['sacharovlaan', 'right'],
                        ['westwijk', 'left'],
                        ['opstelterrein', 'left'],
                        ['aan-de-zoom', 'left'],
                        ['busstation','left'],
                        ['dorpscentrum','left']
                    ]
                },
                "text-offset": {
                    property: 'slug',
                    type: 'categorical',
                    stops: [
                        ['station-zuid', [-1,0]],
                        ['parnassusweg',[0,1.5]],
                        ['a-j-ernststraat', [0,-1.5]],
                        ['van-boshuizenstraat', [0,1]],
                        ['uilenstede', [0,-1.5]],
                        ['kronenburg', [0,1.5]],
                        ['zonnestein', [0,-1.5]],
                        ['onderuit', [0,-1.5]],
                        ['oranjebaan', [0,1.5]],
                        ['ouderkerkerlaan', [0,1.5]],
                        ['sportlaan', [0,1]],
                        ['meent', [-1.5,0]],
                        ['brink', [-1,0]],
                        ['poortwachter', [-1,0]],
                        ['sacharovlaan', [-1.5,0]],
                        ['westwijk', [1.5,0]],
                        ['opstelterrein', [1.5,0]],
                        ['aan-de-zoom', [1,0]],
                        ['busstation',[1,0]],
                        ['dorpscentrum',[1,0]]
                    ]
                },
                "text-max-width": 50
            },
            "paint": {
                'text-color': '#000'
            }
        });

        // if (renderEnv == 'amstelveenlijn') {
        //
        //     self._map.addLayer({
        //         "id": "crossings",
        //         "type": "symbol",
        //         "source": "stops",
        //         "layout": {
        //             "visibility": "visible",
        //             "icon-image": "crossing",
        //             "icon-padding": 0,
        //             "icon-text-fit": 'both',
        //             "icon-allow-overlap": true,
        //         },
        //         "filter": ["==", "crossing", true]
        //     }, 'stops');
        //
        // }

    }

    zoom() {

        let self = this,
            coordinates = self._config.dataset.lines.features[1].geometry.coordinates,
            bounds = coordinates.reduce(function(bounds, coord) {
            return bounds.extend(coord);
        }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

        self._map.fitBounds(bounds, {
            padding: 100
        });
    }

    addLegend() {

        let legend = document.createElement('div');
        legend.classList.add('legend');
        self.hostContainer.appendChild(legend);
    }


}

class Map {

    constructor(element, data, renderEnv) {

        this.element = document.getElementById(element);
        this.renderEnv = renderEnv;

        if(!this.element) {
            return;
        }

        try {
            let dataConfig = this.element.getAttribute('data-config');
            this.customConfig = JSON.parse(dataConfig);
        } catch(err) {
            this.customConfig = [];
            console.log(err);
        }

        let self = this,
            activeItems = [];


        if(this.renderEnv == 'uithoornlijn') {

            this.config = {
                accessToken: 'pk.eyJ1Ijoid2lqbmVtZW5qZW1lZSIsImEiOiJjaWgwZjB4ZGwwMGdza3FseW02MWNxcmttIn0.l-4VI25pfA5GKukRQTXnWA',
                style: 'mapbox://styles/wijnemenjemee/cjx251nik0ass1cp5nhkapnuo',
                hostContainer: element,
                center: (window.innerWidth < 900) ? [4.835, 52.255] : [4.825, 52.255144], // [4.835, 52.255]
                zoom: (window.innerWidth < 600) ? 11.8 : 13,
                pitch: 0,
                bearing: (window.innerWidth < 900) ? 0 : 90,
                scrollZoom: false,
                interactive: true  // (window.innerWidth < 600) ? false : true
            }

        } else if(this.renderEnv == 'amsteltram') {

            this.config = {
                accessToken: 'pk.eyJ1Ijoid2lqbmVtZW5qZW1lZSIsImEiOiJjaWgwZjB4ZGwwMGdza3FseW02MWNxcmttIn0.l-4VI25pfA5GKukRQTXnWA',
                style: 'mapbox://styles/wijnemenjemee/cjx251nik0ass1cp5nhkapnuo',
                hostContainer: element,
                center: [4.841, 52.293], //[4.875, 52.3],
                zoom: (window.innerWidth < 600) ? 11 : 11.6,
                pitch: 0,
                bearing: (window.innerWidth < 900) ? 0 : 90,
                scrollZoom: false,
                interactive: true // (window.innerWidth < 600) ? false : true
            }
        } else if(this.renderEnv == 'amstelveenlijn') {

            this.config = {
                accessToken: 'pk.eyJ1Ijoid2lqbmVtZW5qZW1lZSIsImEiOiJjaWgwZjB4ZGwwMGdza3FseW02MWNxcmttIn0.l-4VI25pfA5GKukRQTXnWA',
                style: 'mapbox://styles/wijnemenjemee/cjx251nik0ass1cp5nhkapnuo',
                hostContainer: element,
                center: [4.85, 52.31],
                zoom: (window.innerWidth < 900) ? 11.6 : 12.2,
                pitch: 0,
                bearing: (window.innerWidth < 900) ? 0 : 90,
                scrollZoom: false,
                interactive: true // (window.innerWidth < 600) ? false : true
            }
        }

        this.options = {
            // interaction: interaction,
            filters : true,
            referenceList : true
        };

        //data from argument in footer scripts (datasets)
        if (data) {
            let d =
            this.config.dataset = this.filterData(JSON.parse(data));

        } else {
            this.config.dataset = {};
        }

       self.init()

    }

    filterData(data) {

        data.features = data.features.filter( f => {

            return f.properties.stopType !== 'remove' && f.properties.slug !== "de-boelelaan-vu" && f.properties.slug !== "stadshart-tram-5" && f.properties.slug !== "opstelterrein";
        });

        return data;
    }

    init() {

        let self = this;

        if(webgl_detect()) {

            const mapWebGL = new MapWebGL(self.config);

            self._map = mapWebGL.create();

            self._lines = new Lines(self._map, self.config);
            self._points = new Points(self._map, self.config);
            // self.interactionPage = new InteractionPage(self._map, self.config); // ,self.interactionPopup,self._activeMarkers
            // self.interactionPage.init();

            self._map.on('style.load', function () {

               self._lines.init(self.renderEnv);

                if (self.config.dataset && self.config.dataset.features.length > 0) {
                    self._points.draw(self.config.dataset,self.renderEnv);
                }
            });
        } else {

            console.log('no web gl allowed')
        }
    }
}

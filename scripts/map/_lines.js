
class Lines {

    constructor(map,config) {

        this._map = map;
        this._config = config;

        this.speedFactor = 30; // number of frames per longitude degree
        this.animation = ''; // to store and cancel the animation
        this.startTime = 0;
        this.progress = 0; // progress = timestamp - startTime
        this.resetTime = false; // indicator of whether time reset is needed for the animation
        this.layerId = 'cjx2un0mm0eoi2or5sh8tc7fe';
    }

    init() {

        let self = this;
        let xhr = new XMLHttpRequest();
        xhr.open('GET', 'https://api.mapbox.com/datasets/v1/wijnemenjemee/' + self.layerId + '/features?access_token=' + self._config.accessToken);
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4;
            var OK = 200;
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {

                    let data = JSON.parse(xhr.response);

                    data.features = data.features.filter( (feature) => {
                        // console.log(feature.geometry.type);
                        return feature.geometry.type == 'LineString';
                    });
                    self._draw(data);
                }
            }
        }
    }

    _draw(lines) {

        let self = this;

        // self.lines = lines;

        self._map.addSource("lines", {
            "type": "geojson",
            "data": lines
        });

        self._map.addLayer({
            "id": "Amstelveenlijn",
            "type": "line",
            "source": "lines",
            "layout": {
                "line-join": "miter",
                "line-cap": "square"
            },
            "paint": {
                "line-color": {
                    property: 'name',
                    type: 'categorical',
                    stops: [
                        ['amstelveenlijn', donkerrood],
                        ['uithoornlijn', geel]
                    ]
                },
                "line-width": 8,
                "line-dasharray": [.5,.5]
            },
        },'stops');


        // let startTime = performance.now();
        //
        // self._animateLine();
    }

    zoom() {

        let self = this,
            coordinates = self._config.dataset.lines.features[1].geometry.coordinates,
            bounds = coordinates.reduce(function(bounds, coord) {
            return bounds.extend(coord);
        }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

        self._map.fitBounds(bounds, {
            padding: 100
        });
    }

    addLegend() {

        let legend = document.createElement('div');
        legend.classList.add('legend');
        self.hostContainer.appendChild(legend);
    }

    _animateLine(timestamp) {

        let self = this;

        if (self.resetTime) {
            // resume previous progress
            self.startTime = performance.now() - self.progress;
            self.resetTime = false;
        } else {
            self.progress = timestamp - self.startTime;
        }

        // restart if it finishes a loop
        if (self.progress > self.speedFactor * 360) {
            self.startTime = timestamp;
            self.lines.features[0].geometry.coordinates = [];
        } else {
            var x = self.progress / self.speedFactor;
            // draw a sine wave with some math.
            var y = Math.sin(x * Math.PI / 90) * 40;
            // append new coordinates to the lineString
            self.lines.features[0].geometry.coordinates.push([x, y]);
            // then update the map
            self._map.getSource('lines').setData(self.lines);
        }
        // Request the next frame of the animation.
        self.animation = requestAnimationFrame(self._animateLine);
    }
}

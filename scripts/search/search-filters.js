var SearchFilters = function SearchFilters(config,searchUrlParameters,searchCheckboxes,language) {


    let filteredByTag = false;

    var _slugify = function _slugify(string) {
        const a = 'àáäâãåăæçèéëêǵḧìíïîḿńǹñòóöôœøṕŕßśșțùúüûǘẃẍÿź·/_,:;'
        const b = 'aaaaaaaaceeeeghiiiimnnnooooooprssstuuuuuwxyz------'
        const p = new RegExp(a.split('').join('|'), 'g')
        return string.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
            .replace(/&/g, '-and-') // Replace & with ‘and’
            .replace(/[^\w\-]+/g, '') // Remove all non-word characters
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, '') // Trim - from end of text
    }


    var _ObjectToArray = function _ObjectToArray(object) {

        let array = [],
            v;

        for (var key in object) {
            if (object.hasOwnProperty(key)) { //  && key !== 'Statische Content'
                let v = {
                    'name' : key,
                    'count' : object[key]
                }
                array.push(v);
            }
        }
        return array;
    }


    var setTypes = function setTypes(type) {

        let self = this,
            types;

        if (type === 'all' || type === null || type === undefined ) {
            types = _setAllTypes();
        } else {
            types = [{ id: type, state: true }];
        }
        return types;
    }

    var _setAllTypes = function _setAllTypes() {

        let list = [];
        config.available_types.forEach( function(item) {
            list.push({
                id: item,
                state: true
            })
        });
        return list;
    }

    var _setAllEnvs = function _setAllEnvs() {

        let list = [];
        config.render_environments.forEach( function(item) {
            list.push({
                id: item,
                state: true
            })
        });
        return list;
    }

    var initFilter = function initFilter(filter,query,news) {

        filter.envs = readUrlEnvParameters([],query);
        filter.tags = [];
        filter.types = readUrlContentParameters([],query);

        return filter;
    }

    var readUrlEnvParameters = function readUrlEnvParameters(list,query) {

        // console.log(query)

            list = _setAllEnvs();



        return list;
    }

    var readUrlContentParameters = function readUrlContentParameters(types,query) {

       // console.log(query)

        if(query == '') {
            types = [{
                id: 'post',
                state: true
            }]
        } else {
            types = _setAllTypes();
        }


        return types;
    }

    var concatString = function concatString(filter,language) {

        let self = this,
            i,
            facetFilter = '';


        // lijkt mis te gaan als er geen eerste type is

        facetFilter = facetFilter.concat('language.code:' + language) ;

        //
        // filter.envs.forEach(function (env) {
        //
        //     if (facetFilter !== '') {
        //         facetFilter = facetFilter.concat(' AND ')
        //     }
        //     facetFilter = facetFilter.concat('render_environments:' + env);
        //     self.filteredByTag = true;
        // });

        filter.tags.forEach(function(tag) {

            if (facetFilter !== '') {
                facetFilter = facetFilter.concat(' AND ')
            }
            facetFilter = facetFilter.concat('taxonomies.tags.slug:' + tag);
            self.filteredByTag = true;
        });


        let trueTypeCount = 0;

        for (i = 0; i < filter.envs.length; i++) {

            if(filter.envs[i].state) {
                trueTypeCount++;
            }

            if (facetFilter !== '' && i == 0) {
                facetFilter = facetFilter.concat(' AND ');
            }

            if (i == 0 && filter.envs.length > 1) {
                facetFilter = facetFilter.concat('(');
            }

            if (facetFilter !== '' && filter.envs[i].state === true && i > 0 && trueTypeCount > 1) {
                facetFilter = facetFilter.concat(' OR ');
            }

            if(filter.envs[i].state === true) {
                facetFilter = facetFilter.concat('render_environments:' + filter.envs[i].id);
            }

            if(filter.envs.length > 1 && (filter.envs.length - 1) == i  ) {
                facetFilter = facetFilter.concat(')');
            }

        }

        let hasType = false;

        for (i = 0; i < filter.types.length; i++) {

            if(filter.types[i].state) {
                trueTypeCount++;
            }

            if (facetFilter !== '' && i == 0) {
                facetFilter = facetFilter.concat(' AND ');
            }

            if (i == 0 && filter.types.length > 1) {
                facetFilter = facetFilter.concat('(');
            }

            if (hasType && facetFilter !== '' && filter.types[i].state === true && i > 0 && trueTypeCount > 1) {
                facetFilter = facetFilter.concat(' OR ');
            }

            if(filter.types[i].state === true) {
                facetFilter = facetFilter.concat('type:' + filter.types[i].id);
                hasType = true;
            }

            if(filter.types.length > 1 && (filter.types.length - 1) == i  ) {
                facetFilter = facetFilter.concat(')');
            }

        }

        return facetFilter;
    }

    var addHTML = function addHTML(content,elements,parameters) {

        let self = this,
            tag_li,
            envList = [],
            typeList = [],
            tagList = [],
            tagSlugs = [],
            combinedList = [];

        elements.filterContainer.innerHTML = '';


        if (content.facets['render_environments']) {
            envList = _ObjectToArray(content.facets['render_environments']);
        }

        if (content.facets['type']) {
            typeList = _ObjectToArray(content.facets['type']);
        }

        if (content.facets['taxonomies.tags.name']) {
            tagList = _ObjectToArray(content.facets['taxonomies.tags.name']);
        }
        if (content.facets['taxonomies.tags.slug']) {
            tagSlugs = _ObjectToArray(content.facets['taxonomies.tags.slug']);
        }

        for (let a = 0; a < envList.length; a++) {
            envList[a]['slug'] = _slugify(envList[a]['name']);
        }

        for (let a = 0; a < typeList.length; a++) {
            typeList[a]['slug'] = _slugify(typeList[a]['name']);
        }

        for (let i = 0; i < tagList.length; i++) {
            tagList[i]['slug'] = tagSlugs[i]['name'];
        }

        console.log(typeList)

        if (envList) {
            searchCheckboxes.createCheckboxes(envList,config.render_environments, 'render_environments');
        }

        if (typeList) {
            searchCheckboxes.createCheckboxes(typeList,config.available_types, 'type');
        }

        // add tags

        if (tagList) {

            let arrayWithIrrelevantTags  = [
                'aanbesteding','afscheid','amstelveen-centrum','amstelveenlijn','amstelveenpakket','bcu','begeleiding-commissies-uitvoering',
                'besluitvorming','bestemmingsplannen','bomenkap','boringen','bouwplaats','bouwterrein','buitenveldertselaan','chroom-6','dubbele-boog',
                'factsheet','frits-mullerlaan','funderingswerkzaamheden','gasunie','geschiedenis','grasbaan','grondwater','historie',
                'kabels-en-leidingen','kettinghor','laatste-rit-51','lift','lijnnummer','mantelbuizen','metronetwerk','noord-zuidlijn',
                'nuon-warmte','onderzoeken','ov-netwerk','pendelbus','planuitwerkingsfase','retourbemaling','route-tram-5','tijdelijke-beneluxbaan',
                'voorbereidende-werkzaamheden'

            ];

            tagList = tagList.filter( (t) => {

                console.log(arrayWithIrrelevantTags.indexOf(t.slug));

               return arrayWithIrrelevantTags.indexOf(t.slug) < 0;

            });

            let tag_ul = document.createElement('ul');


            tagList = tagList.slice(0,12);

            for (let i = 0; i < tagList.length; i++) {

                tag_li = document.createElement('li');
                tag_li.classList.add('tag-tab');
                tag_li.classList.add('smallerfont');
                if (i > 3) { tag_li.classList.add('initially_hidden'); }
                tag_li.innerHTML = tagList[i].name + ' <span class="count evensmallerfont">(' + tagList[i].count + ')</span>';
                tag_li.onclick = function() { search.onFilterChange('taxonomies.tags.slug',tagList[i].slug); filteredByTag = true; };
                tag_ul.appendChild(tag_li);
                if (i === 3) {
                    let divider = document.createElement('span');
                    divider.classList.add('tag-tab');
                    divider.classList.add('divider');
                    divider.classList.add('smallerfont');
                    divider.onclick = function() { search.showMoreTags()};
                    divider.innerHTML = 'toon meer onderwerpen';
                    tag_ul.appendChild(divider);
                }
                if (i === 11) {
                    let divider = document.createElement('span');
                    divider.classList.add('tag-tab');
                    divider.classList.add('divider');
                    divider.classList.add('smallerfont');
                    divider.classList.add('less');
                    divider.onclick = function() { search.showMoreTags()};
                    divider.innerHTML = 'toon minder onderwerpen';
                    tag_ul.appendChild(divider);
                }
            }

            elements.filterContainer.appendChild(tag_ul);
        }



        if (parameters.filter.envs.length > 0 || parameters.filter.tags.length > 0) {

            let clearFilter = document.createElement('span');
            clearFilter.classList.add('tag-tab');
            clearFilter.classList.add('clear');
            clearFilter.classList.add('smallerfont');
            clearFilter.innerHTML = (language === 'en') ? 'remove filter' : 'filter verwijderen';
            clearFilter.onclick = function () {
                search.clearFilter();
                filteredByTag = false;
            };
            elements.filterContainer.appendChild(clearFilter);

        }

       
    }

    return {

        initFilter: initFilter,
        concatString : concatString,
        setTypes: setTypes,
        addHTML : addHTML
    };
}

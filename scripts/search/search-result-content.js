var SearchResultContent = function SearchResultContent() {

    var  addHTML = function addHTML(content,container) {

        let self = this,
            snippet,
            textSnippet;

        container.innerHTML = '';

        if(content === undefined) { return; }

        content.hits.forEach( function(item,i){

            if ('searchSnippet' in item) {

                snippet = document.createElement('div');
                const classes = ['card', 'row'];
                snippet.classList.add(...classes);


                snippet.innerHTML = item.searchSnippet;

                // if(content.query && item.sections) {
                //
                //     let sections = Object.keys(item.sections).map(function(key) {
                //         return [Number(key), item.sections[key]];
                //     });
                //
                //     if (sections.length > 0) {
                //         for (let i = 0; i < sections.length; i++) {
                //             if (sections[i][1].type === 'paragraph') {
                //                 item.content = item.content + sections[i][1].text;
                //             }
                //         }
                //     }
                //     // find index of first occurence of query
                //     item.content = item.content.replace(/<(?:.|\n)*?>/gm, '');
                //     let startOfHighlight = item.content.indexOf(content.query);
                //     let endOfHightlight = startOfHighlight + content.query.length
                //     item.content = item.content.slice(0,startOfHighlight) + '<b>' + item.content.slice(startOfHighlight,endOfHightlight) + '</b>' + item.content.slice(endOfHightlight);
                //     // create snippet of 60 chars - and plus index
                //     let min = startOfHighlight - 30;
                //     let max = endOfHightlight + 30;
                //
                //     if (min < 0) {
                //         min = 0;
                //         max = endOfHightlight + 50;
                //         textSnippet = item.content.slice(min, max);
                //         textSnippet = textSnippet + ' ...';
                //     } else {
                //         textSnippet = item.content.slice(min, max);
                //         textSnippet = '... ' + textSnippet + ' ...';
                //     }
                //     snippet.querySelector('.search-excerpt').innerHTML = textSnippet;
                // }
                container.appendChild(snippet);
            }
        });
    }

    return {

        addHTML : addHTML
    }
}
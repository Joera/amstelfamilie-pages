var SearchHelpers = function SearchHelpers(config,elements) {

    // var readUrlTaxonomyParameters;
    var initIndex = function initIndex(index) {

        let client = algoliasearch(config.app_id, config.api_key, {protocol: 'https:'});
        index = client.initIndex(config.index);

        // customRanking applies after query ranking from algolia UI. So without any query this ranking rules.
        // index.setSettings({
        //     customRanking: [
        //         'desc(date)'
        //     ]
        // });
        return index;
    }

    var initButton = function initButton() {
        elements.submitButton.addEventListener("click", function (e) {
            search.onQueryChange();
        }, false);
        elements.searchBox.addEventListener("keyup", function (e) {
            let key = e.which || e.keyCode;
            if (key == 13) {
                search.onQueryChange();
            }
        }, false);
    }

    // var getParameterByName = function getParameterByName(name, url) {
    //
    //     if (!url) url = window.location.href;
    //     name = name.replace(/[\[\]]/g, "\\$&");
    //     var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    //         results = regex.exec(url);
    //     if (!results) return null;
    //     if (!results[2]) return '';
    //     return decodeURIComponent(results[2].replace(/\+/g, " "));
    // }
    //
    // var readUrlQueryParameters = function readUrlQueryParameters() {
    //
    //     let urlParameters = {};
    //
    //     urlParameters.query = getParameterByName('query') || null;
    //
    //     // set search term in search input box when coming from other page
    //     if (urlParameters.query) {
    //         elements.searchBox.value = urlParameters.query;
    //     }
    //
    //     urlParameters.page = getParameterByName('page') || null;
    //
    //     console.log(urlParameters.page);
    //
    //     if (getParameterByName('error')) {
    //         let error = getParameterByName('error').replace(/^\/|\/$/g, '');
    //         urlParameters.query = error;
    //         elements.searchBox.value = error;
    //     }
    //
    //     // als er een query is .. dan in alle paginatypes zoeken
    //     // if (urlParameters.query != null) {
    //     //
    //     // }
    //
    //     return urlParameters;
    // }
    //
    // readUrlTaxonomyParameters = function readUrlTaxonomyParameters(filter) {
    //
    //     let theme = getParameterByName('theme') || false;
    //
    //     // set search term in search input box when coming from other page
    //     if (theme) {
    //         filter.themes = [theme];
    //     }
    //
    //     let tag = getParameterByName('tag') || false;
    //
    //     // set search term in search input box when coming from other page
    //     if (tag) {
    //         filter.tags = [tag];
    //     }
    //
    //     let type = getParameterByName('type') || false;
    //
    //     // set search term in search input box when coming from other page
    //     if (type) {
    //         filter.types = [{ id : type, state : true}];
    //     }
    //     return filter;
    // };
    //
    //
    //
    // var pushState = function pushState(filter) {
    //
    //     console.log(filter);
    //
    //     let params = Object.keys(filter).map(function(k) {
    //
    //         if(filter[k] != null && filter[k].length > 0) {
    //             if(Array.isArray(filter[k])) {
    //
    //                 filter[k].forEach( f => {
    //                     // doe iets man!
    //                 });
    //
    //             } else {
    //                 return encodeURIComponent(k) + '=' + encodeURIComponent(filter[k]);
    //             }
    //         }
    //     }).join('&');
    //
    //     if (history.pushState) {
    //         var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + params
    //         window.history.pushState({path:newurl},'',newurl);
    //     }
    // }
    //
    // var pushStateWithQuery = function pushStateWithQuery(urlParameters) {
    //
    //     var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname
    //
    //     if ((urlParameters.query != null || urlParameters.page != null) && history.pushState) {
    //
    //         newurl = newurl + '?';
    //
    //         if (urlParameters.query && urlParameters.query != null) {
    //
    //          newurl = newurl + 'query=' + urlParameters.query;
    //         }
    //
    //         if (urlParameters.query && urlParameters.page) {
    //
    //             newurl = newurl + '&';
    //         }
    //
    //         if (urlParameters.page && urlParameters.page != null) {
    //
    //             newurl = newurl + 'page=' + urlParameters.page;
    //         }
    //
    //         window.history.pushState({path:newurl},'',newurl);
    //     }
    // }


    return {

        initIndex: initIndex,
        initButton: initButton,

    };
}


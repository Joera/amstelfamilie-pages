// require('_definitions')
// require('_ganttitem');
// require('_lanes');
// require('_mask');
// require('_milestones');
// require('_references');
// require('_timegrid');

class Planning {


    constructor(wrapper,data) {

        this.config = {};
        this.margin = {};
        this.svg = null;
        this._wrapper = wrapper;
        // data staan in __data.js en worden bovenaan meegevoegd tijdens compile van planning-js
        this._deviceType = 'desktop';
        this.checkBreakpoints();

        data.lanes = data.map(d => {
                return parseInt(d.position);
		}).filter((value, index, self) => {
				return self.indexOf(value) === index;
		});

		this.dataset = data; // .datasetSection0;
        //
		// console.log(data);

		// this.init();
	}

	addEvent(object, type, callback) {
	    if (object == null || typeof(object) == 'undefined') return;
	    if (object.addEventListener) {
	        object.addEventListener(type, callback, false);
	    } else if (object.attachEvent) {
	        object.attachEvent("on" + type, callback);
	    } else {
	        object["on"+type] = callback;
	    }
	};



	init() {

		var self = this;

		// console.log(this.dataset);

		this.dataset.forEach(function(item){  // this.dataset.items.

            if (item.endType === '0') { item.endType = false; }
            if (item.endType === '1') { item.endType = true; }
            if (item.startType === '0') { item.startType = false; }
            if (item.startType === '1') { item.startType = true; }

        });

		this.margin = { top: 62, right: 0, bottom: 40, left: 0  }; // 406
		this.config.laneHeight = 34;

		this._hostContainer = document.getElementById(self._wrapper);
		this._hideExplanation = document.getElementById('planning-uitleg-sluit-knop');
		this._showExplanation = document.getElementById('planning-uitleg-knop');
		this._explanationContainer = document.getElementById('planning_explanation');

		this.config.chart = d3.select(this._hostContainer).append('svg');
		this.config.mainGroup = this.config.chart.append('g');
		this.config.definitions = this.config.chart.append('defs');
		this.config.clipPath = this.config.mainGroup.append('clipPath').attr("id", "finished");

        let firstDayToBeginOfMonth = this.getDayDifferance(new Date(new Date(this.getDateRange()[0]).getFullYear(), new Date(this.getDateRange()[0]).getMonth(), 1), new Date(this.getDateRange()[0])), // number of days since the month started, of the min date in the domain
            firstDayMonth = (firstDayToBeginOfMonth < 15) ? 4: 3, // the number of months that the chart will show before the first bar starts. If days to end is larger that 15 add extra month.
            firstDay = new Date(new Date(this.getDateRange()[0]).getFullYear(), new Date(this.getDateRange()[0]).getMonth() - firstDayMonth, 1), // the first day that is displayed on the chart, min chart date
            lastDayOfLastMonth = new Date(new Date(this.getDateRange()[1]).getFullYear(), new Date(this.getDateRange()[1]).getMonth() + 1, 0), // the last day of the month, of the max date in the domain
            lastDayToEndOfMonth = this.getDayDifferance(new Date(this.getDateRange()[1]), lastDayOfLastMonth), // number of days before the month ends, of the max date in the domain
            lastDayMonth = (lastDayToEndOfMonth > 15) ? 4: 5, // the number of months that the chart will show after the last bar ends. If days to end is larger that 15 add extra month.
            lastDay = new Date(new Date(this.getDateRange()[1]).getFullYear(), new Date(this.getDateRange()[1]).getMonth() + lastDayMonth, 0); // the last day that is displayed on the chart scale, max chart date

        this.config.start = new Date('2019-01-01');  // this.dataset.startdatum
	    this.config.einde = lastDay;  // this.dataset.einddatum

	    this.addEvent(window, "resize", function(event) { self.checkBreakpoints(); });

	    lanes.init(this.config,this.dataset);
	    definitions.init(this.config);
	    timegrid.init(this.config);
	    ganttitems.init(this.config,this.dataset);
	    mask.init(this.config,this.dataset);




	    this.draw();

	}

	draw() {

		var hostcontainerWidth = window.getComputedStyle(this._hostContainer).width.slice(0,-2);

      	this.config.width = hostcontainerWidth - this.margin.left - this.margin.right;

        this.config.chart
        	.attr('width', this.config.width + this.margin.left + this.margin.right);

        this.config.mainGroup
        	.attr('class','mainGroup')
        	.attr('width', this.config.width)
        	.attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

	    this.config.height = ((this.dataset.lanes.length) * this.config.laneHeight);

	    this.config.chart
	        .attr('height', this.config.height + this.margin.top + this.margin.bottom);

	    this.config.mainGroup
	        .attr('height', this.config.height);

	    this.config.yExtent = [0,this.dataset.lanes.length];
	    this.config.xScale  = d3.scaleTime().range([0,this.config.width]).domain([this.config.start, this.config.einde]);


	    lanes.draw(this.config);
	    timegrid.draw(this.config);
	    ganttitems.draw(this.config);
	    milestones.draw(this.config,this.dataset);
	    mask.draw(this.config);
	    // references.draw(this.config,this.dataset);
	}

	hideExplanation() {

		this._explanationContainer.style.height = 0;
		this._explanationContainer.style.padding = '0em 2em 0em 3em';
		this._showExplanation.style.display = 'block';
	}

	showExplanation() {

		this._explanationContainer.style.height = 'calc(100% - 59px - 2em)';
		this._explanationContainer.style.padding = '1em 2em 1em 3em';
		this._showExplanation.style.display = 'none';
	}

	checkBreakpoints() {

		this._newDeviceType = 'desktop';
		if (window.innerWidth < smaller_desktop) { this._newDeviceType = 'smaller_desktop'; }
		if (window.innerWidth < tablet_landscape) { this._newDeviceType = 'tablet_landscape'; }
		if (window.innerWidth < tablet_portrait) { this._newDeviceType = 'tablet_portrait'; }
		if (window.innerWidth < phone) { this._newDeviceType = 'phone'; }

		if (this._deviceType !== this._newDeviceType && Object.keys(this.config).length > 0) {

			this._deviceType = this._newDeviceType;
			var hostcontainerWidth = window.getComputedStyle(this._hostContainer).width.slice(0,-2);
      		this.config.width = hostcontainerWidth - this.margin.left - this.margin.right;
			this.draw();
		}
	}

    /**
     * Return the minimum and maximum date in the dataset
     * @returns {array}             [minimumDate, maximumDate]. Dates are strings formatted yyyy-mm-dd
     */
    getDateRange() {
        // set the date range
        let dates = [];

        // create array containing all the dates, from and to dates
        this.dataset.forEach(d => {
            dates.push(d.from);
            dates.push(d.to);
        });

        // get date range
        let dateRange = d3.extent(dates, (d) => {
            return d;
        });

        return dateRange;
    }


    /**
     * Returns the number of months the chart spans
     * @returns {number}
     */
    getMonthCount() {
        return new Date(this.getDateRange()[1]).getMonth() - new Date(this.getDateRange()[0]).getMonth() + (12 * (new Date(this.getDateRange()[1]).getFullYear() - new Date(this.getDateRange()[0]).getFullYear()));
    }


    /**
     * Get the differance in days between 2 dates
     * @param first             date object
     * @param second            date object
     * @returns {number}
     */
    getDayDifferance(first, second) {
        return Math.round((second-first)/(1000*60*60*24));
    }


    /**
     * Get the differance in months between 2 dates
     * @param first             date object
     * @param second            date object
     * @returns {number}
     */
    getMonthDifferance(first, second) {
        var months;
        months = (second.getFullYear() - first.getFullYear()) * 12;
        months -= first.getMonth() + 1;
        months += second.getMonth();
        return months <= 0 ? 0 : months;
    }
}


// var planning = new Planning();
// planning.init();

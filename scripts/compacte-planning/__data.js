// var data = {
//     "pageId": "28971",
//     "startdatum": "2016-01-01",
//     "einddatum": "2022-01-01",
//     "items": [{
//         "name": "Aanbestedingsprocedure hoofdaannemer",
//         "desc": "x",
//         "start": "2016-01-01 00:00:00",
//         "startType": "0",
//         "end": "2017-03-20 00:00:00",
//         "endType": "0",
//         "rij": "0",
//         "cat": "blauw"
//     }, {
//         "name": "Voorbereidende werkzaamheden",
//         "desc": "x",
//         "start": "2017-01-01 00:00:00",
//         "startType": "0",
//         "end": "2018-12-31 00:00:00",
//         "endType": "0",
//         "rij": "1",
//         "cat": "geel"
//     }, {
//         "name": "Realisatie opstelterrein",
//         "desc": "x",
//         "start": "2019-01-01 00:00:00",
//         "startType": "0",
//         "end": "2020-03-31 00:00:00",
//         "endType": "0",
//         "rij": "1",
//         "cat": "oranje"
//     }, {
//         "name": "Realisatie ongelijkvloerse kruispunten + ombouw Amstelveenlijn",
//         "desc": "x",
//         "start": "2018-11-01 00:00:00",
//         "startType": "0",
//         "end": "2020-03-31 00:00:00",
//         "endType": "0",
//         "rij": "0",
//         "cat": "oranje"
//     }, {
//         "name": "Zomerbuitendienststelling",
//         "desc": "x",
//         "start": "2019-07-14 00:00:00",
//         "startType": "0",
//         "end": "2019-08-26 00:00:00",
//         "endType": "0",
//         "rij": "2",
//         "cat": "blauw"
//     }, {
//         "name": "Huidige lijn 51 stopt",
//         "desc": "x",
//         "start": "2019-03-01 00:00:00",
//         "startType": "0",
//         "end": "2020-12-31 00:00:00",
//         "endType": "0",
//         "rij": "3",
//         "cat": "mijlpaal"
//     }, {
//         "name": "Instroom nieuwe trams",
//         "desc": "x",
//         "start": "2020-04-01 00:00:00",
//         "startType": "0",
//         "end": "2020-04-01 00:00:00",
//         "endType": "0",
//         "rij": "3",
//         "cat": "mijlpaal"
//     }, {
//         "name": "Uiterste datum oplevering VITAL",
//         "desc": "x",
//         "start": "2020-12-31 00:00:00",
//         "startType": "0",
//         "end": "2020-04-01 00:00:00",
//         "endType": "0",
//         "rij": "3",
//         "cat": "mijlpaal"
//     }, {
//         "name": "Afronding project en decharge",
//         "desc": "x",
//         "start": "2021-01-01 00:00:00",
//         "startType": "0",
//         "end": "2021-03-31 00:00:00",
//         "endType": "0",
//         "rij": "0",
//         "cat": "rood"
//     }, {
//         "name": "Buffer bouwactiviteiten, testfase en start vernieuwde Amstelveenlijn",
//         "desc": "x",
//         "start": "2020-04-01 00:00:00",
//         "startType": "0",
//         "end": "2020-12-31 00:00:00",
//         "endType": "0",
//         "rij": "1",
//         "cat": "geel"
//     }],
//     "nr_rij": "0",
//     "nr_items": "10",
//     "lanes": ["0", "1", "2", "3"]
// };

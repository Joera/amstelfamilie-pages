
class Timegrid { 

	constructor() {

      this.locale = d3.timeFormatLocale({

          dateTime: "%a %e %B %Y %T",
          date: "%Y-%m-%d",
          time: "%H:%M:%S",
          periods: ["AM", "PM"], // unused
          days: ["zondag", "maandag", "dinsdag", "woensdag", "donderdag", "vrijdag", "zaterdag"],
          shortDays: ["zo", "ma", "di", "wo", "do", "vr", "za"],
          months: ["januari", "februari", "maart", "april", "mei", "juni", "juli", "augustus", "september", "oktober", "november", "december"],
          shortMonths: ["jan", "feb", "mrt", "apr", "mei", "jun", "jul", "aug", "sep", "okt", "nov", "dec"]
      }); 
      this._months = null; 
      this._years = null; 
      this._now = moment();
    }

    init(config) {

      this._pastTimeHeader = config.mainGroup.append('rect').attr('class', 'pasttimeheader');
      this._clipRectHeader = config.clipPath.append('path'); 
      // this._todayTriangle = config.mainGroup.append('polygon').attr('class', 'todayTriangle');
      // this._yearsTop = config.mainGroup.append('g').attr('class', 'planning grid top years');
      // this._monthsTop = config.mainGroup.append('g').attr('class', 'planning grid top months');
      this._yearsBottom = config.mainGroup.append('g').attr('class', 'planning grid bottom years');
      this._monthsBottom = config.mainGroup.append('g').attr('class', 'planning grid bottom months');
    }

    draw(config){

      // this.drawTime(config);
      this.drawToday(config);
      this.drawYears(config);
      this.drawMonths(config);
    }

    // drawTime(config) {
    //
    //   this._clipRectHeader
    //     .attr("d", 'M0,0 L' + config.xScale(this._now) + ',0 ' + (config.xScale(this._now) - 30) + ',-60 0,-60 z')
    //   ;
    //
    //   this._pastTimeHeader
    //     .attr('y', -60)
    //     .attr('height',60)
    //     .attr('width', config.xScale(this._now))
    //     .attr('clip-path', 'url(#pastTimeHeader)')
    //     .style('fill', blauw)
    //   ;
    // }

    drawToday(config) {

      var trianglePoints = (config.xScale(this._now) - 140) + ' -21, ' + (config.xScale(this._now) - 120) + ' -61, ' + (config.xScale(this._now) - 120) + ' -21';
      var parseTime = this.locale.format("%e %B %Y");


        config.mainGroup.selectAll('polygon').remove();
        config.mainGroup.selectAll('.todayRect').remove();
        config.mainGroup.selectAll('.todayLine').remove();
        config.mainGroup.selectAll('.todayLabel').remove();

        config.mainGroup.append('polygon')
        .attr('points', trianglePoints)
        .attr('class', 'todayRect')
        .attr('x', config.xScale(this._now))
        .attr('width', 30)
        .attr('height', 40);


        config.mainGroup.append('rect')
            .attr('class', 'todayRect')
            .attr('x', config.xScale(this._now) - 120)
            .attr('y', -61)
            .attr('width', 120)
            .attr('height', 40);



        config.mainGroup.append('rect')
            .attr('class', 'todayLine')
            .attr('x', config.xScale(this._now) - 2)
            .attr('y', -20)
            .attr('width', 2)
            .attr('height', config.height + 20);



          config.mainGroup.append('text')
            .text(parseTime(new Date))
            .attr('class', 'todayLabel')
            .attr('x', config.xScale(this._now) - 10)
            .attr('y', -34)
            .attr('text-anchor', 'end');



    }

    drawYears(config) { 

      // var xScaleGridYears = d3.axisTop(config.xScale)
      //     .ticks(d3.timeYear, 1)
      //     .tickFormat(this.locale.format('%Y'))  // locale.nl_NL.
      //     .tickSize(-8, 2, 0)
      //     .tickSizeOuter(0);
      //
      // this._yearsTop
      //     .attr('transform', 'translate(0,' + (config.height - 10) + ')')
      //     .attr('height', config.height)
      //     .call(xScaleGridYears);
      //
      // config.mainGroup.select('.planning.grid.top.years').call(xScaleGridYears)
      //     .selectAll('text')
      //     .attr('dx', 0)
      //     .attr('dy', 28);   // -20

      var xScaleGridYearsBottom = d3.axisBottom(config.xScale)
          .ticks(d3.timeYear, 1)
          .tickFormat(this.locale.format('%Y'))  // locale.nl_NL.
          .tickSize(12, 0, 0)
          .tickSizeOuter(0);

      this._yearsBottom
          .attr('transform', 'translate(0,' + config.height + ')')
          .attr('height', 15)
          .call(xScaleGridYearsBottom);

      config.mainGroup.select('.planning.grid.bottom.years').call(xScaleGridYearsBottom)
          .selectAll('text')
          .attr('dx', 0)
          .attr('dy', 10);   // -20
      
    }

    drawMonths(config) { 

      var self = this; 

      // var xScaleGridMonths = d3.axisTop(config.xScale)
      //     .ticks(d3.timeMonth, 4)
      //     .tickFormat(this.locale.format('%b')) // locale.nl_NL.
      //     .tickSize(-8, 0, 0)
      //     .tickSizeOuter(0);
      //
      // this._monthsTop
      //     .attr('transform', 'translate(0,' + (config.height - 10)+ ')')
      //     .attr('height', config.height)
      //     .call(xScaleGridMonths);
      //
      // config.mainGroup.select('.planning.grid.top.months').call(xScaleGridMonths)
      //     .selectAll('text')
      //     .attr('dx', 0)
      //     .attr('dy', 28);

       var xScaleGridMonthsBottom = d3.axisBottom(config.xScale)
          .ticks(d3.timeMonth, 4)
          .tickFormat(this.locale.format('%b')) // locale.nl_NL.
          .tickSize(12, 0, 0)
          .tickSizeOuter(0);

      this._monthsBottom
          .attr('transform', 'translate(0,' + config.height + ')')
          .attr('height', 15)
          .call(xScaleGridMonthsBottom);

      config.mainGroup.select('.planning.grid.bottom.months').call(xScaleGridMonthsBottom)
          .selectAll('text')
          .attr('dx', 0)
          .attr('dy', 10);


      // do not show the month january // not working with types // getMonth' does not exist on type '{}'
      d3.selectAll('g.grid.months text')
          .filter(function(d){ return d.getMonth() === 0} )
          .style('display','none')
          ;
    }
}

var timegrid = new Timegrid();
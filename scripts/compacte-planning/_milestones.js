class Milestones {

	constructor(){

		this._milestonesGroup = null; 
	    this._geel = '#fcca32';
	    this._oranje = '#f79234';                                   
	    this._lichtblauw = '#199ECA';
	    this._grijs = '#D7D7D7';
	    this._donkergrijs = '#646464';
	    this._now = moment();
	    this._dataset = null;
	}

	draw(config,dataset) {

        this._dataset = dataset;

		config.yScale = d3.scaleLinear().domain([config.yExtent[0],config.yExtent[1]]).range([0,config.height]);
    	config.xScale = d3.scaleTime().range([0,config.width]).domain([config.start,config.einde]);

		if (this._milestonesGroup === null) { 

        	this._milestonesGroup = config.mainGroup.append('g')
            	.attr('class', 'milestones');
		}

		this._dataset.milestones = this._dataset.filter(function(d) {

			 if (d.class === 'milestone') {  return d; }
		});

      	var milestonesGroup = this._milestonesGroup.selectAll('.milestone')
			.data(this._dataset.milestones); // .filter(function (d) { return d.class.indexOf('gantt_item') > -1; }));

		milestonesGroup
		.enter().append('image')
            .attr('class','milestone')
            .attr('xlink:href',function (d) { 

                return 'https://amsteltram.nl/assets/icons/milestone-icon.svg';
                
            })
            .attr('height', 20)
            .attr('width', 16)
            .attr('y', function (d) { return config.yScale(d.position) + .05 * config.yScale(1) - 4; })
            .style('stroke',geel)
            .style('stroke-width', 2)
            
        .merge(milestonesGroup)
        	.style('opacity',0)
        	.attr('x', function (d) {

				// if (d.end === "" || d.end.date === undefined) { 

                	return config.xScale(moment(d.from)) - 3;

				// } else { 

				// 	return config.xScale(moment(d.end.date)) - 3;
				// }
            })
			.transition()
			.duration(200)
			.delay(0)
			.style('opacity',1)
			// .on('click', function(d,i){  scope.config.toggle(d); });
      		;

      	milestonesGroup.exit().remove();

		var labels = this._milestonesGroup.selectAll('.milestoneLabel')
          .data(this._dataset.milestones); 

      	labels
        .enter().append('text')
            .attr('class', function (d) { return 'milestoneLabel'})
            .text(function (d) { return d.name; })
            .attr('x', -10)
			.attr('text-anchor', function (d) { 

				if (d.endType === false) {
					return 'start';
				} else {
					return 'end';
				}
            })
            
            .attr('y', function (d) {
				return config.yScale(d.position) + 0.31 * config.yScale(1) + 3;
            })

        .merge(labels)

        	.attr('x', function (d,i) {

				if (d.endType === false) { 

					return config.xScale(moment(d.from))  + 8; // 24

				} else { 

					return config.xScale(moment(d.from)) - 14; // 24
              	}
        	});
        labels.exit().remove();
	}
}

var milestones = new Milestones(); 
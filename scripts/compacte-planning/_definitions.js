class Definitions {

	constructor() {}

	init(config) {

		this.gradients(config);
		// this.patterns(config); 
	}

	gradients(config) {

		var yellowGradientStart = config.definitions
        	.append('linearGradient')
        	.attr('id', 'yellow-gradient-start')
        	.attr('spreadMethod', 'pad');

      	yellowGradientStart.append('stop')
        	.attr('offset', '0%')
        	.attr('stop-color', geel)
        	.attr('stop-opacity', 0);

        yellowGradientStart.append('stop')
        	.attr('offset', '50%')
        	.attr('stop-color', geel)
        	.attr('stop-opacity', 1);

      	yellowGradientStart.append('stop')
        	.attr('offset', '100%')
        	.attr('stop-color', geel)
        	.attr('stop-opacity', 1);

      	var yellowGradientEnd = config.definitions
        	.append('linearGradient')
        	.attr('id', 'yellow-gradient-end')
        	.attr('spreadMethod', 'pad');

      	yellowGradientEnd.append('stop')
      	  	.attr('offset', '0%')
        	.attr('stop-color', geel)
        	.attr('stop-opacity', 1);

      	yellowGradientEnd.append('stop')
        	.attr('offset', '50%')
        	.attr('stop-color', geel)
        	.attr('stop-opacity', 1);

      	yellowGradientEnd.append('stop')
        	.attr('offset', '100%')
        	.attr('stop-color', geel)
        	.attr('stop-opacity', 0);

      	var orangeGradientStart = config.definitions
        	.append('linearGradient')
        	.attr('id', 'orange-gradient-start')
        	.attr('spreadMethod', 'pad');

      	orangeGradientStart.append('stop')
        	.attr('offset', '0%')
        	.attr('stop-color', oranje)
        	.attr('stop-opacity', 0);

        orangeGradientStart.append('stop')
        	.attr('offset', '50%')
        	.attr('stop-color', oranje)
        	.attr('stop-opacity', 1);

      	orangeGradientStart.append('stop')
        	.attr('offset', '100%')
        	.attr('stop-color', oranje)
        	.attr('stop-opacity', 1);

      	var orangeGradientEnd = config.definitions
        	.append('linearGradient')
        	.attr('id', 'orange-gradient-end')
        	.attr('spreadMethod', 'pad');

      	orangeGradientEnd.append('stop')
        	.attr('offset', '0%')
        	.attr('stop-color', oranje)
        	.attr('stop-opacity', 1);

      	orangeGradientEnd.append('stop')
        	.attr('offset', '50%')
        	.attr('stop-color', oranje)
        	.attr('stop-opacity', 1);

      	orangeGradientEnd.append('stop')
        	.attr('offset', '100%')
        	.attr('stop-color', oranje)
        	.attr('stop-opacity', 0);

      var greyGradientStart = config.definitions
        .append('linearGradient')
        .attr('id', 'grey-gradient-start')
        .attr('spreadMethod', 'pad');

        greyGradientStart.append('stop')
          .attr('offset', '0%')
          .attr('stop-color', grijs)
          .attr('stop-opacity', 1);

        greyGradientStart.append('stop')
          .attr('offset', '50%')
          .attr('stop-color', donkergrijs)
          .attr('stop-opacity', 1);

        greyGradientStart.append('stop')
          .attr('offset', '100%')
          .attr('stop-color', donkergrijs)
          .attr('stop-opacity', 1);

      var greyGradientEnd = config.definitions
        .append('linearGradient')
        .attr('id', 'grey-gradient-end')
        .attr('spreadMethod', 'pad');

      greyGradientEnd.append('stop')
        .attr('offset', '0%')
        .attr('stop-color', donkergrijs)
        .attr('stop-opacity', 1);

      greyGradientEnd.append('stop')
        .attr('offset', '50%')
        .attr('stop-color', donkergrijs)
        .attr('stop-opacity', 1);

      greyGradientEnd.append('stop')
        .attr('offset', '100%')
        .attr('stop-color', grijs)
        .attr('stop-opacity', 0);

      var blueGradientStart = config.definitions
        .append('linearGradient')
        .attr('id', 'blue-gradient-start')
        .attr('spreadMethod', 'pad');

        blueGradientStart.append('stop')
          .attr('offset', '0%')
          .attr('stop-color', lichtblauw)
          .attr('stop-opacity', 1);

        blueGradientStart.append('stop')
          .attr('offset', '50%')
          .attr('stop-color', blauw)
          .attr('stop-opacity', 1);

        blueGradientStart.append('stop')
          .attr('offset', '100%')
          .attr('stop-color', donkerblauw)
          .attr('stop-opacity', 1);

      var blueGradientEnd = config.definitions
        .append('linearGradient')
        .attr('id', 'blue-gradient-end')
        .attr('spreadMethod', 'pad');

        blueGradientEnd.append('stop')
          .attr('offset', '0%')
          .attr('stop-color', donkerblauw)
          .attr('stop-opacity', 1);

        blueGradientEnd.append('stop')
          .attr('offset', '50%')
          .attr('stop-color', blauw)
          .attr('stop-opacity', 1);

        blueGradientEnd.append('stop')
          .attr('offset', '100%')
          .attr('stop-color', lichtblauw)
          .attr('stop-opacity', 1);
	}

	// patterns(config) {

	// 	var pattern = config.definitions
	// 		.append('pattern')
	// 		.attr("id", "lines")
	// 		.attr("width", "3")
	// 		.attr("height", "3")
	// 		.attr("patternUnits", "userSpaceOnUse")
	// 		.attr("patternTransform", "rotate(30)")
 //        ;

	// 	pattern
	// 		.append("rect")
	// 		.attr("width", "1")
	// 		.attr("height", "3")
	// 		.style("transform","translate(0,0)")
	// 		.style("fill","#e7e7e7")
 //      	;
	// }
}

var definitions = new Definitions(); 
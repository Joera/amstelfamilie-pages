class Lanes { 

	constructor() {

		this._laneGroup = null; 
		this._dataset = null;

	}

    init(config,dataset) {

        var self = this;
        this._dataset = dataset;

        if (this._laneGroup === null) { 

            this._laneGroup = config.mainGroup.append('g')
                .attr('class', 'compactLanes');
        }
    }

	draw(config) {

    var self = this; 

        config.yScale = d3.scaleLinear().domain([config.yExtent[0], config.yExtent[1]]).range([0, config.height]);

        this._laneRects = this._laneGroup.selectAll('.laneRects')
            .data(this._dataset.lanes);

        this._laneRects
        .enter().append('rect')
            .attr('class', 'laneRects')
            .attr('x', 0)
            .attr('width','100%')
            .attr('y', function (d) { 
                    return config.yScale(d) - 1;
            })
            .attr('height', function (d) {
                  return config.laneHeight;
            })
            .style('fill', '#fff')            
            ;
        this._laneRects.exit().remove();

        this._laneLines = this._laneGroup.selectAll('.laneLines')
            .data(this._dataset.lanes);

        this._laneLines
        .enter().append('line')
            .attr('class', 'laneLines')
            .attr('x1', 0)
            .attr('y1', function (d) { 
                    return config.yScale(d) - 1;
            })
            .attr('y2', function (d) {
                  return config.yScale(d) - 1;
            })
        .merge(this._laneLines)
            .attr('x2', function (d) { return config.width; } )
            ;
        this._laneLines.exit().remove();  
    }
}

var lanes = new Lanes();
class GanttItems {
	
	constructor(){

		this._ganttGroup = null; 
		this.dataset = null;
    	this._now = new Date();
	}

	init(config,dataset) {

		this._ganttGroup = config.mainGroup.append('g').attr('class', 'ganttElements');
        this.dataset = dataset;
	}

	draw(config) {

		config.yScale = d3.scaleLinear().domain([config.yExtent[0],config.yExtent[1]]).range([0,config.height]);
    	config.xScale = d3.scaleTime().range([0,config.width]).domain([config.start,config.einde]);


      	this.rectangles(config);

      	this.endpoints(config);
      	this.startpoints(config);
      	this.startspace(config);
      	this.endspace(config);
      	this.labels(config);
	}

	rectangles(config) {

		this.dataset.ganttitems = this.dataset.filter(function(d) {

			 if (d.class !== 'milestone') {  return d; }
		});

		var rectangles = this._ganttGroup.selectAll('.gantt_item')
          .data(this.dataset.ganttitems); // .filter(function (d) { return d.class.indexOf('gantt_item') > -1; }));

      	rectangles
        .enter().append('rect')
			.attr('transform', 'translate(0,0)')
			.attr('height', function (d) { return 0.25 * config.yScale(1) + 0; })
			.attr('class', function (d) { return 'gantt_item ' + d.cat; })
			.attr('y', function (d) { return config.yScale(d.position) + 0.45 * config.yScale(1) + 3;  })  //
			.style('fill', function (d) {

				if (d.class === 'yellow') { return geel; }
				else if (d.class === 'orange') { return oranje; }
				else if (d.class === 'blue') { return blauw;  }
				else if (d.class === 'red') { return lichtrood;  }
				else { return blauw; }
			})
		.merge(rectangles)
			// .transition()
			// .duration(200)
			// .delay(function (d) { return 100 + config.xScale(new Date(d.from)) * 1.5;  })

			.attr('x', function (d) { return config.xScale(new Date(d.from.replace(/-/g, "/"))); })
			.attr('width', function (d) { let width = config.xScale(new Date(d.to.replace(/-/g, "/"))) - config.xScale(new Date(d.from.replace(/-/g, "/"))); return width > 0 ? width : 0; })
			;

      rectangles.exit().remove();
	}

	endpoints(config) {

		var self = this;

		var endpoints = this._ganttGroup.selectAll('rect.endpoint')
          .data(this.dataset.ganttitems.filter(function (d) { return d.endType === true; }))
          ;

      	endpoints
        .enter().append('rect')
			.attr('class', function (d) { return 'endpoint ' + d.cat; })
			.attr('x', 0)
			.attr('width', 4 )
			.attr('height', function (d) { return 0.4 * config.yScale(1) + 0; })
			.attr('y', function (d) { return config.yScale(d.position) + 0.35 * config.yScale(1) - 0;  })  //
			.style('fill', function (d) {

				if (new Date(d.to) < self._now) { return grijs; }
				else if (d.class === 'yellow') { return geel; }
				else if (d.class === 'orange') { return oranje; }
				else if (d.class === 'red' && d.startType) { return lichtrood;  }
				else if (d.class === 'red' && d.endType) { return lichtrood;  }
				else { return blauw; }
			})
		.merge(endpoints)
			// .transition()
			// .duration(200)
   //        	.delay(function (d) { return 100 + config.xScale(new Date(d.from)) * 1.5;  })
          	.attr('x', function (d) { return config.xScale(new Date(d.to)) - 4; })
			;
	}

	endspace(config) {

		var self = this;

		var endpoints = this._ganttGroup.selectAll('rect.endspace')
          .data(this.dataset.ganttitems.filter(function (d) { return d.endType === true; }))
          ;

      	endpoints
        .enter().append('rect')
			.attr('class', function (d) { return 'endspace ' + d.cat; })
			.attr('x', 0)
			.attr('width', 4 )
			.attr('height', function (d) { return 0.4 * config.yScale(1) + 1; })
			.attr('y', function (d) { return config.yScale(d.rij) + 0.35 * config.yScale(1) - 0;  })  //
			.style('fill', '#fff')
		.merge(endpoints)
			// .transition()
			// .duration(200)
   //        	.delay(function (d) { return 100 + config.xScale(new Date(d.from)) * 1.5;  })
          	.attr('x', function (d) { return config.xScale(new Date(d.to)) - 0 })
			;
	}

	startspace(config) {

		var self = this;

		var endpoints = this._ganttGroup.selectAll('rect.startspace')
          .data(this.dataset.ganttitems.filter(function (d) { return d.endType === true; }))
          ;

      	endpoints
        .enter().append('rect')
			.attr('class', function (d) { return 'startspace ' + d.cat; })
			.attr('x', 0)
			.attr('width', 4 )
			.attr('height', function (d) { return 0.4 * config.yScale(1) + 1; })
			.attr('y', function (d) { return config.yScale(d.position) + 0.35 * config.yScale(1) - 0;  })  //
			.style('fill', '#fff')
		.merge(endpoints)
			// .transition()
			// .duration(200)
   //        	.delay(function (d) { return 100 + config.xScale(new Date(d.from)) * 1.5;  })
          	.attr('x', function (d) { return config.xScale(new Date(d.from)) - 4; })
			;
	}

	startpoints(config) {

		var self = this;

		var startpoints = this._ganttGroup.selectAll('rect.startpoint')
          .data(this.dataset.ganttitems.filter(function (d) { return d.startType === true; }))
          ;

      	startpoints
        .enter().append('rect')
          	.attr('class', function (d) { return 'startpoint ' + d.cat; })
          	.attr('x', 0)
          	.attr('width', 4 )
          	.attr('height', function (d) { return 0.35 * config.yScale(1) + 0; })
          	.attr('y', function (d) { return config.yScale(d.rij) + 0.35 * config.yScale(1) - 0;  })  //
          	.style('fill', function (d) {

            	if (new Date(d.from) < self._now) { return grijs; }
            	else if (d.class === 'yellow') { return geel; }
            	else if (d.class === 'orange') { return oranje; }
            	else if (d.class === 'red') { return lichtrood; }
            	else { return blauw; }
          	})
        .merge(startpoints)
			.attr('x', function (d) { return config.xScale(new Date(d.from)); })
			;
	}

	labels(config) {

		var labels = this._ganttGroup.selectAll('.stationLabel')
          .data(this.dataset.ganttitems); // .filter(function (d) { return d.class.indexOf('gantt_item') > -1; }));

      	labels
        .enter().append('text')
            .attr('class', function (d) { return 'stationLabel'})
            .text(function (d) { return d.name; })
            .attr('x', 0)
			.attr('text-anchor', function (d) {

				if (d.endType === false) {

					return 'start';

				} else {

					return 'end';
				}
            })

            .attr('y', function (d) {

				// if(d.labelPosition === 'top') {

					return config.yScale(d.position) + 0.31 * config.yScale(1) + 3;

				// } else {

				// 	return config.yScale(d.position) + 0.90 * config.yScale(1);
				// }
            })
        .merge(labels)
        	// .transition()
         //    .duration(200)
         //    .delay(function (d) { return 100 + config.xScale(new Date(d.from)) * 1.5;  })
        	    .attr('x', function (d,i) {

				if (d.endType === false) {

					return config.xScale(new Date(d.from)) + 10; // 24

				} else {

					return config.xScale(new Date(d.to)) - 10; // 24
              	}
        	});
        labels.exit().remove();
    }
}

var ganttitems = new GanttItems(); 


class Countdown {

    constructor(){}

    init() {
        setInterval( () => this.loop(),1);
    }

    loop() {
        const then = new Date('2020-12-13T06:07:00.000Z');
        const now = new Date();
        const { d, h, m, s } = this.secondsToDhms((Math.abs(then - now)) / 1000);
        this.populate( d, h, m, s);
    }

    secondsToDhms(seconds) {

        seconds = Number(seconds);
        var d = [0,0]; //this.addDigit(Math.floor(seconds / (3600*24)));
        var h = [0,0]; //this.addDigit(Math.floor(seconds % (3600*24) / 3600));
        var m = [0,0]; //this.addDigit(Math.floor(seconds % 3600 / 60));
        var s = [0,0]; //this.addDigit(Math.floor(seconds % 60));

        return { d, h, m, s }
    }

    populate(d, h, m, s ) {

        let el = document.getElementById('countdown');

        if(el) {
            el.querySelector('div:nth-child(1) > span:first-child > span:first-child' ).innerText = d[0];
            el.querySelector('div:nth-child(1) > span:first-child > span:last-child').innerText = d[1];

            el.querySelector('div:nth-child(3) > span:first-child > span:first-child').innerText = h[0];
            el.querySelector('div:nth-child(3) > span:first-child > span:last-child').innerText = h[1];

            el.querySelector('div:nth-child(5) > span:first-child > span:first-child').innerText = m[0];
            el.querySelector('div:nth-child(5) > span:first-child > span:last-child').innerText = m[1];

            el.querySelector('div:nth-child(7) > span:first-child > span:first-child').innerText = s[0];
            el.querySelector('div:nth-child(7) > span:first-child > span:last-child').innerText = s[1];
        }
    }

    addDigit(n) {

        let d =  (n < 10) ? '0' + n.toString() : n.toString();
        return d.split('');
    }





}

const countdown = new Countdown();
countdown.init();

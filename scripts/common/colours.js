var lichtgrijs = "rgb(230,230,230)";
var grijs = "rgb(220,220,220)";
var donkergrijs = "rgb(100,100,100)";
var zwart = "rgb(0,0,0)";
var wit = "rgb(255,255,255)";

var lichtgeel = "rgb(255,224,99)";
var geel = "rgb(255,203,0)";
var donkergeel = "rgb(248,175,0)";
var lichtoranje = "rgb(242,145,0)";
var oranje = "rgb(237,114,3)";
var lichtrood = "rgb(231,78,15)";
var rood = "rgb(229,50,18)";
var donkerrood = "rgb(209,10,17)";
var lichtblauw = "rgb(156,198,212)";
var blauw = "rgb(0,154,200)";
var donkerblauw = "rgb(0,111,157)";
var navy = "rgb(0,85,120)";

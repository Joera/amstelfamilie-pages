class Twitter {	

	constructor() {}

	createHTML(tweet) {

		var a = document.createElement('a');
		a.href = 'https://twitter.com/' + tweet.user_screen_name + '/status/' + tweet.id_str;
		a.setAttribute('target', '_blank');

        var container = document.createElement('div');
        container.classList.add('tweet_container');

        var upper = document.createElement('div');
        upper.classList.add('tweet_container_upper');

        var img = document.createElement('img');
        img.classList.add('tweet_thumb');
        img.src = tweet.user_image;
        img.alt = 'profielfoto van ' + tweet.user_name;
		img.title = 'profielfoto van ' + tweet.user_name;

		var time = document.createElement('div');
        time.classList.add('tweet_time');
        time.classList.add('light');
        var d = new Date(tweet.created_at);
        var monthname = ['januari', 'februari', 'maart', 'april', 'mei', 'juni', 'juli', 'augustus', 'september', 'oktober', 'november', 'december'];
        time.innerText = d.getDate()+" "+ monthname[d.getMonth()] +" "+d.getFullYear() + " | " + d.getHours()+":"+d.getMinutes();  // needs formatting


		var author = document.createElement('span');
        author.classList.add('tweet_auteur');
        author.classList.add('vet');
        author.innerText = tweet.user_name;

        upper.appendChild(img);
        upper.appendChild(time);
        upper.appendChild(author);

        var author_ = document.createElement('span');
        author_.innerText = ' @' + tweet.user_screen_name;

        upper.appendChild(author_);
        container.appendChild(upper);

        var body = document.createElement('div');
        body.classList.add('tweet_container_body');
        body.classList.add('tweet_tekst');
        body.innerText = tweet.text;


        container.appendChild(body);
        a.appendChild(container);

        return a;

	}

	init(env) {

		var self = this,
			result = '';
        var tweetcontainer = document.getElementById("tweet_wrapper");
        // var dateFormats = {
	    //    time: "D MMMM YYYY | hh:mm"
	    // };

		// get tweets
		var xhr = new XMLHttpRequest();
		xhr.open('GET', 'https://amsteltram.nl/twitterfabriek/api/tweets/' + env);
		xhr.send();

		xhr.onreadystatechange = function () {
		  var DONE = 4; var OK = 200;
		  if (xhr.readyState === DONE) {
		   if (xhr.status === OK) {

		   		var data = JSON.parse(xhr.response);

		   		var tweets = data.slice(0,24);

               tweets.forEach( (t) => {

                    t.user_image = t.user_image.replace('http://','https://');
                    let html = self.createHTML(t);
                   	tweetcontainer.appendChild(html);
               });

		    } else {
		      console.log('twitterfabriek is ontploft'); // dan weer eruit halen // An error occurred during the request.
		   }
		  }
		};
	}

}


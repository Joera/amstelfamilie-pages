class Blocks {


    constructor(){}

    init(){

        this.blocks = [].slice.call(document.querySelectorAll('.block.expanding'));
    }

    expand(el) {

        let self = this;

        self.blocks.forEach((block) => {
            block.classList.remove('expanded');
            block.classList.add('dimmed');
        });

        el.parentNode.classList.add('expanded');
        el.parentNode.classList.remove('dimmed');
    }

    collapse() {

        let self = this;

        self.blocks.forEach((block) => {
            block.classList.remove('expanded');
            block.classList.remove('dimmed');
        });
    }
}


var blocks = new Blocks();
blocks.init();

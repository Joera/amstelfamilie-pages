const argv = require('minimist')(process.argv.slice(2))

const gulp = require('gulp')
const sass = require('gulp-sass')
const uglify = require('gulp-uglify')
const rename = require('gulp-rename')
const streamify = require('gulp-streamify')
const source = require('vinyl-source-stream')

const budo = require('budo')
const browserify = require('browserify')
const resetCSS = require('node-reset-scss').includePath
const babelify = require('babelify').configure({
  presets: ['es2015'] 
})


const outfile = 'planning-work.js';

//the development task
gulp.task('watch', function(cb) {
  //watch SASS
  // gulp.watch('src/sass/*.scss', ['sass'])

  //dev server
  budo('dist/init.js', {
    serve: 'scripts/bundle.js',     // end point for our <script> tag
    stream: process.stdout, // pretty-print requests
    live: true,             // live reload & CSS injection
    dir: 'app',             // directory to serve
    open: argv.open,        // whether to open the browser
    browserify: {
      transform: babelify,
      standalone: 'vra-graph-module'
    }
  }).on('exit', cb)
})

//the distribution bundle task
gulp.task('bundle', function() {
  var bundler = browserify('dist/init.js', {
      transform: babelify,
      standalone: 'vra-graph-module'
  })
  .bundle()
  return bundler
    .pipe(source('index.js'))
    .pipe(streamify(uglify()))
    .pipe(rename(outfile))
    .pipe(gulp.dest('../../assets/scripts/'))
})

import { ChartObjects, ChartSVG, ChartDimensions, ChartScale, ChartAxes } from '../chart-basics/module';
import { PastItems, FutureItems, GridTime, Rows, Today } from "../chart-elements/module";

import * as d3 from 'd3';

export class Planning  {

    element;
    radios;
    dimensions;
    svg;

    chartDimensions;
    chartSVG;
    chartXScale;
    chartYScale;
    chartAxis;

    yScale;
    xScale;
    topAxis;

    pastItems;
    futureItems;
    gridTime;
    rows;
    today

    constructor(
        private data,
        private elementID,
        private config,
    ) {
        this.element = d3.select(elementID).node();
    }

    extendEndDate(date) {

        var d = new Date(date);
        var year = d.getFullYear();
        var month = d.getMonth();
        var day = d.getDate();
        var c = new Date(year + 1, month, day);
        return c;
    }

    init() {

        this.data = this.data.filter( (item) => {
            return new Date(item.end) >= new Date()
        });

        let minDate = this.data.reduce((min, item) => new Date(this.data.start) < new Date(min) ? new Date(item.start) : min, new Date(this.data[0].start));
        let maxDate = this.data.reduce((max, item) => new Date(item.end) > new Date(max) ? new Date(item.end) : max, new Date(this.data[0].end));

        this.config.extra.startDate = minDate;
        this.config.extra.endDate = this.extendEndDate(maxDate);

        let self = this;
        let chartObjects = ChartObjects();
        this.config = Object.assign(chartObjects.config(),this.config);
        this.dimensions = chartObjects.dimensions();
        this.svg = chartObjects.svg();

        this.config.paddingInner = 0;
        this.config.paddingOuter = 0;

        // get dimensions from parent element
        this.chartDimensions = new ChartDimensions(this.elementID, this.config);
        this.dimensions = this.chartDimensions.get(this.dimensions);

        // create svg elements without data
        this.chartSVG = new ChartSVG(this.elementID, this.config, this.dimensions, this.svg);
        this.chartXScale = new ChartScale(this.config.xScaleType, this.config, this.dimensions);
        this.chartYScale = new ChartScale(this.config.yScaleType, this.config, this.dimensions);
        this.topAxis = new ChartAxes(this.config, this.svg, 'top',this.chartXScale);

        this.pastItems = new PastItems(this.config, this.svg.layers);
        this.futureItems = new FutureItems(this.config, this.svg.layers);
        this.gridTime = new GridTime(this.config, this.svg.layers);
        this.rows = new Rows(this.config, this.svg.layers);
        this.today = new Today(this.config, this.svg.layers);

        self.update(this.data);
    }


    prepareData(newData)  {


        return newData;
    }


    redraw(data) {

        this.yScale = this.chartYScale.set(data.map( (d) => d._id), undefined);

        // on redraw chart gets new dimensions
        this.dimensions = this.chartDimensions.get(this.dimensions);
        this.chartSVG.redraw(this.dimensions);
        // new dimensions mean new scales
        this.xScale = this.chartXScale.reset('horizontal', this.dimensions,this.xScale);
        this.yScale = this.chartYScale.reset('vertical', this.dimensions,this.yScale);
        // new scales mean new axis
        this.topAxis.redraw(this.config.xScaleType, this.dimensions, this.xScale);
        this.pastItems.redraw(this.dimensions,this.xScale,this.yScale);
        this.futureItems.redraw(this.dimensions,this.xScale,this.yScale);
        this.gridTime.redraw(this.dimensions,this.xScale,this.yScale);
        this.rows.redraw(this.dimensions,this.xScale,this.yScale);
        this.today.redraw(this.dimensions,this.xScale,this.yScale);
    }

    draw(data) {

        this.xScale = this.chartXScale.set([this.config.extra.startDate,this.config.extra.endDate]);
        this.pastItems.draw(data);
        this.futureItems.draw(data);
        this.gridTime.draw(data);
        this.rows.draw(data);
        this.today.draw(data);
    }

    update(newData) {

        let self = this;
        let data = this.prepareData(newData);
        this.draw(data);
        this.redraw(data);
        window.addEventListener("resize", () => self.redraw(data), false);

    }
}

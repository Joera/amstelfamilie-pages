// import 'babel-polyfill';
import 'isomorphic-fetch';
import { graphs } from './charts/module';
import * as d3 from 'd3';
import {GraphObject} from "./types/graphObject";
import {ResponseData} from "./types/responseData";



export class InitGraph {

    constructor(){

        const config: any = {

            "graphType": "Planning",
            "xScaleType" : "time",
            "yScaleType" : "band",
            "xParameter" : "date",
            "yParameter" : "",

            "padding": {
                "top": 0,
                "bottom": 0,
                "left": 0,
                "right": 0
            },
            "margin": {
                "top": 0,
                "bottom": 0,
                "left": 0,
                "right": 0
            },
            "extra": {
                "xScaleTicks": "timeMonth",
                "barHeight" : 80
            }
        }

        let url = 'https://uithoornlijn.nl/werkzaamheden-en-planning/planning-data.json';

        d3.json<ResponseData>(url)
            .then((data) => {

                let planning = new graphs[config.graphType](data.datasetSection0, '#datavisplanning', config);
                planning.init();

            })
            .catch((error) => {
                console.log(error);
            });
    }
}




import { colours} from "../_styleguide/_colours";

export class PastItems {

    bars;
    barsEnter;

    barLabels;
    barLabelsEnter

    constructor(
        private config,
        private svgLayers
    ){}

    draw(data) {

        this.bars = this.svgLayers.data.selectAll(".bar-past")
            .data(data);

        this.bars.exit().remove();

        this.barsEnter = this.bars
            .enter()
            .append("rect")
            .attr("class", "bar-past")
            .attr("fill", colours.lichtgeel)
        ;
    }

    redraw(dimensions,xScale,yScale) {

        let self = this;

        this.bars
            .merge(this.barsEnter)
            .attr("x", (d) => {
                return new Date(d.start) > new Date(this.config.extra.startDate) ? xScale(new Date(d.start)) : xScale(new Date(this.config.extra.startDate));
            })
            .attr("y", (d,i) => (i * this.config.extra.barHeight) + (.375 * this.config.extra.barHeight ))
            .attr("height", (d) => .5 * this.config.extra.barHeight)
            .attr("width", (d) => (xScale(new Date(d.start)) - xScale(new Date()) > 0) ? 0 :  xScale(new Date()) - xScale(new Date(d.start)) )
        ;

    }
}



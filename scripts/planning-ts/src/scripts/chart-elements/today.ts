import { colours} from "../_styleguide/_colours";

export class Today {

    bar;
    todayLine;
    label;

    barLabels;
    barLabelsEnter;
    dataLength;

    constructor(
        private config,
        private svgLayers
    ){}

    draw(data) {

        this.dataLength = data.length;

        this.todayLine = this.svgLayers.data
            .append("rect")
            .attr("class", "today-line")
            .attr("fill", colours.zwart)
        ;

        this.bar = this.svgLayers.data
            .append("rect")
            .attr("class", "today-bar")
            .attr("fill", colours.zwart)
        ;

        this.barLabels = this.svgLayers.data
            .append('text')
            .attr('class','today-label')
            .attr('x', 0)
            .attr('dx', '6px')
            .attr('dy', '17px')
            .attr("stroke", colours.wit)
            .style("font-size", "1rem")
            .style("text-anchor", "start")
    }

    redraw(dimensions,xScale,yScale) {

        let self = this;
        let height = this.dataLength * this.config.extra.barHeight;

        this.todayLine
            .attr("x", (d) => {
                return xScale(new Date());
            })
            .attr("y", (d,i) => 0 )
            .attr("height", (d) => height + 30)
            .attr("width", (d) => 1)
        ;

        this.bar
            .attr("x", (d) => {
                return xScale(new Date());
            })
            .attr("y", (d,i) => height + 15 )
            .attr("height", (d) => 30)
            .attr("width", (d) => 80);

        this.barLabels
            .text((d) => 'vandaag')
            .attr('transform', (d,i) => 'translate(' + (xScale(new Date())) + ',' + (height + 17) + ')')
            .attr('fill-opacity', 1);

    }
}



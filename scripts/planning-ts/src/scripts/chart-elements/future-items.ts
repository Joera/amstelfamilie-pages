import { colours} from "../_styleguide/_colours";

export class FutureItems {

    bars;
    barsEnter;

    barLabels;
    barLabelsEnter

    constructor(
        private config,
        private svgLayers
    ){}

    draw(data) {

        this.bars = this.svgLayers.data.selectAll(".bar-future")
            .data(data);

        this.bars.exit().remove();

        this.barsEnter = this.bars
            .enter()
            .append("rect")
            .attr("class", "bar-future")
            .attr("fill", "rgb(248,175,0)")

        this.barLabels = this.svgLayers.data.selectAll(".barLabel")
            .data(data);

        this.barLabels.exit().remove();

        this.barLabelsEnter = this.barLabels
            .enter()
            .append('text')
            .attr('class','barLabel')
            .attr('x', 0)
            .attr('dx', '5px')
            .attr('dy', '-7px')
            .style("font-size", "1rem")
            .style("text-anchor", "start")
        ;
    }

    redraw(dimensions,xScale,yScale) {

        let self = this;
      //  let barHeight = 30; // yScale.bandwidth();

        this.bars
            .merge(this.barsEnter)
            .attr("x", (d) => {
                return xScale(new Date());
            })
            .attr("y", (d,i) => (i * this.config.extra.barHeight) + ( .375 * this.config.extra.barHeight))
            .attr("height", (d) => .5 * this.config.extra.barHeight )
            .attr("width", (d) => (xScale(new Date(d.end)) - xScale(new Date()) > 0) ? xScale(new Date(d.end)) - xScale(new Date()) : 0)
        ;

        this.barLabels
            .merge(this.barLabelsEnter)
            .text((d) => d.title)
            .attr('transform', (d,i) => 'translate(' + (xScale(new Date())) + 3 + ',' + ((i * this.config.extra.barHeight) + (this.config.extra.barHeight * .375)) + ')')
            .attr('fill-opacity', 1);


    }
}



import { colours} from "../_styleguide/_colours";

export class Rows {

    bars;
    barsEnter;

    barLabels;
    barLabelsEnter

    constructor(
        private config,
        private svgLayers
    ){}

    draw(data) {

        this.bars = this.svgLayers.underData.selectAll(".row-line")
            .data(data);

        this.bars.exit().remove();

        this.barsEnter = this.bars
            .enter()
            .append("rect")
            .attr("class", "row-line")
            .attr("fill", colours.lichtgrijs)
        ;
    }

    redraw(dimensions,xScale,yScale) {

        let self = this;

        this.bars
            .merge(this.barsEnter)
            .attr("x", (d) => {
                return xScale(new Date(this.config.extra.startDate));
            })
            .attr("y", (d,i) => ((i + 1) * this.config.extra.barHeight) - 1 )
            .attr("height", (d) => 1)
            .attr("width", (d) => dimensions.width )
        ;
    }
}



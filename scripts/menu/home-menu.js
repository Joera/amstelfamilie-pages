class HomeMenu {

    constructor() {}

    init() {

        // bij xxl multiple active containers

        let self = this;

        this.homeContainer = document.querySelector('.home_grid');

        if (window.innerWidth > 900) {

            this.homeMenu = document.getElementById('dashboard-menu');
            this.menuItems = [].slice.call(this.homeMenu.querySelectorAll('li:not(.active-background)'));

        } else {

            this.homeMenu = document.getElementById('home-menu');
            this.menuItems = [].slice.call(this.homeMenu.querySelectorAll('li:not(.active-background)'));
        }

        this.options = this.menuItems.map( (el) =>  el.getAttribute('data-option'));
        this.containers = [].slice.call(this.homeContainer.children).filter ( c => c.hasAttribute('data-option'));
        this.commentWrapper = document.querySelector('#comments_wrapper');
        this.commentElements = [].slice.call(document.querySelectorAll('#comments_wrapper > .comment'));
        this.timelines = {};
        this.swipeElements = {};
        this.current = 0;

        if(900 < window.innerWidth && window.innerWidth < 2000 ) {
            this.setTimelines();
        } else {
            this.setSwipeListeners();
        }
        this.setMenuListeners();
        this.setCommentListeners();


        // when dom is rendered
        window.addEventListener("load", function(event) {

            // add a perceived delay for smoothness
            // setTimeout( () => {
                // when desktop remove news from containers
                if(900 < window.innerWidth && window.innerWidth < 2000 ) {

                    self.containers = self.containers.filter( c => c.getAttribute('data-option') !== 'news');

                    for (let container of self.containers) {
                        // temp set all containers to relative .. so open function removes them at init.
                        container.style.position = 'relative';
                    }

                 //   self.open('comments');
                }

            // },50)
        });

        // when swiching from portrait to landscape on a tablet and vice versa
        window.addEventListener('resize', function(e){

            // self.setMenuListeners();
            //
            // if(900 < window.innerWidth && window.innerWidth < 2000 ) {
            //     // self.setTimelines();
            // } else {
            //     self.setSwipeListeners();
            // }
        });

        setTimeout( () => {

            if (document.querySelector('#comments_wrapper > p')) {
                this.open('tweets');
            }

        },2000);


    }

    setSwipeListeners() {

        let self = this;
        let index, prev, next, current, wrapper;


        this.containers.forEach(  (container,index) =>  {

            // index  = this.options.indexOf(container.getAttribute('data-option'));
            wrapper = container.querySelector('div');

            this.swipeElements[container.getAttribute('data-option')] = new Hammer(wrapper);

            this.swipeElements[container.getAttribute('data-option')].get('swipe').set({
                threshold:	10,
                velocity: .05,
                direction: Hammer.DIRECTION_HORIZONTAL
            });

            this.swipeElements[container.getAttribute('data-option')]

                .on("swipeleft", function(e) {

                    e.preventDefault();
                    self.rotate('next');
                })
                .on("swiperight", function(e) {

                    e.preventDefault();
                    self.rotate('prev')
                });
        });
    }

    setMenuListeners() {

        let self = this;

        let menuListener = event => {
            var targetElement = event.target || event.srcElement;
            (window.innerWidth > 900) ? self.open(self._getOptionFromEl(targetElement)) : self.rotate(self._getOptionFromEl(targetElement));
        }

        for (let menuItem of this.menuItems) {
            menuItem.addEventListener('click', menuListener, false)
        }
    }

    setCommentListeners() {

        let self = this;
        for (let comment of this.commentElements) {

            let links = [].slice.call(comment.querySelectorAll('.comment-relations_thread'));

            for (let link of links) {

                link.addEventListener('click', function (event) {

                    let targetElement = event.target || event.srcElement;

                    if (!targetElement.classList.contains('comment-relations_thread')) {
                        targetElement = targetElement.parentNode;
                    }
                    if (!targetElement.classList.contains('comment-relations_thread')) {
                        targetElement = targetElement.parentNode;
                    }
                    if (!targetElement.classList.contains('comment-relations_thread')) {
                        targetElement = targetElement.parentNode;
                    }

                    //self.showThread(targetElement.getAttribute('data-thread-id'));
                    self.scrollThread(targetElement);
                }, false)

            }
        }
    }

    setTimelines() {

        this.timelines.dashboardIn = {};
        this.timelines.dashboardOut = {};

        this.timelines.mobilePrev = anime.timeline({
            autoplay: false
        })
            .add({

            });

        this.timelines.mobileNext = anime.timeline({
            autoplay: false
        })
            .add({

            });

        for (let container of this.containers) {

            this.timelines.dashboardOut[container.getAttribute('data-option')] = anime.timeline({
                autoplay: false
            })
            .add({
                targets: container.querySelector('.aside_wrapper'),
                duration: 100,
                easing: 'easeOutSine',
                // elasticity: 250,
                translateX: [0, -500],
                opacity: [1, 0],

                complete: () => { if (window.innerWidth < 900) { container.style.right = 0;  } else { container.style.zIndex = 1 }}
            });

            if(container.getAttribute('data-option') === 'comments') {

                this.timelines.dashboardIn['comments'] = anime.timeline({
                    autoplay : false
                })
                    .add({
                        targets: container.querySelector('.aside_wrapper'),
                        duration: 100,
                        easing: 'easeOutSine',
                        // elasticity: 250,
                        opacity: [1, 1],
                        translateX: [0, 0],
                        complete: () => { if (window.innerWidth > 899) { container.style.zIndex = 2 }}
                        //   complete: (anim) => activetimeline.remove(active.querySelector('.aside_wrapper'))
                    });


            } else {

                this.timelines.dashboardIn[container.getAttribute('data-option')] = anime.timeline({
                    autoplay : false
                })
                    .add({
                        targets: container.querySelector('.aside_wrapper'),
                        duration: 250,
                        easing: 'easeOutSine',
                        // elasticity: 250,
                        opacity: [0, 1],
                        translateX: [500, 0],
                        complete: () => { if (window.innerWidth > 899) { container.style.zIndex = 2 }}
                        //   complete: (anim) => activetimeline.remove(active.querySelector('.aside_wrapper'))
                    });

            }


        }
    }


    _getOptionFromEl(elem) {

        if (!elem.tagName || elem.tagName === undefined) {
            return elem
        } else {
            for (; elem && elem !== document; elem = elem.parentNode) {
                if (elem.tagName === 'LI') {
                    return elem.getAttribute('data-option');
                }
            }
        }
    }

    open(option) {  // alleen waarde doorgeven? en dan ook een array met waardes?

        let self = this;

        for (let container of this.containers) {

            if(window.innerWidth < 2000 && container.hasAttribute('data-option') && container.style.position == 'relative') { //  pos relative signifies current
               self.timelines.dashboardOut[container.getAttribute('data-option')].play();
            }

            container.style.position = '';
        }

        let active = self.containers.filter((c) => {
            return c.getAttribute('data-option') === option;
        })[0];

        for (let item of this.menuItems) {

            item.classList.remove('active');

            if(item.getAttribute('data-option') === option) {
                item.classList.add('active');
            };
        }

        // setTimeout( () => {

            active.style.position = 'relative';
            self.timelines.dashboardIn[option].play();

        // },125)


    }

    rotate(direction) {

         let now;

         if (direction === 'next' && this.current === (this.options.length - 1)) now = '100vw';
         else if (direction === 'prev' && this.current === 0) now = '-600vw';
         else now = -100 * this.current + 'vw';

         if (direction === 'prev') this.current =  this.current - 1;
         else if (direction === 'next') this.current =  this.current + 1;
         else if (direction !== undefined) this.current = this.options.indexOf(direction);

        this.current = this._circlingIndex(this.current);

        let offset = -100 * this.current + 'vw';
        this._move(now,offset);
        this._changeMenu();
    }

    _circlingIndex(current) {

        if (current > this.options.length - 1) return 0;
        else if (current < 0) return this.options.length - 1;
        else return current;
    }

    _changeMenu(option) {

        for (let item of this.menuItems) {
            item.classList.remove('active');
        }

        let activeLi = this.menuItems[this.current];

        let backgroundEl = this.homeMenu.querySelector('.active-background');

        if(backgroundEl) {

            let start = backgroundEl.getBoundingClientRect();
            let end = this.menuItems[this.current].getBoundingClientRect();


            let t = anime.timeline({
                autoplay: false
            })
                .add({
                    targets: backgroundEl,
                    left: [start.x, end.x],
                    duration: 500,
                    easing: 'easeOutElastic(2, 1)',
                    // elasticity: 250,
                    complete: () => {
                        activeLi.classList.add('active');
                    }
                });

            t.play();
        }

    }

    _move(now,offset) {

        let self = this;

        let a = anime.timeline({
            autoplay: false
        })
            .add({
                targets: self.homeContainer,
                marginLeft : [now, offset],
                duration: 300,
                easing: 'easeOutSine',
            });

        a.play();
    }

    scrollThread(el) {

        let containerPosition = this.commentWrapper.scrollTop;
        let targetPos = document.querySelector('#comment_' + el.getAttribute('data-link')).offsetTop;
        let startPos = el.parentNode.parentNode.offsetTop;
        this.commentWrapper.scrollTo(0, containerPosition + targetPos - startPos);
    }

    showThread(threadID) {

        let self = this;
        
        if(threadID) {

            for (let el of this.commentElements) {

                el.classList.add('inactive');

                if (el.getAttribute('data-thread-id') == parseInt(threadID)) {

                    el.classList.remove('inactive');
                }
            }
        }

        self.setRevertListeners();
    }

    setRevertListeners() {

        let self = this;

        this.inactiveElements = [].slice.call(document.querySelectorAll('#comments_wrapper > .comment.inactive'));

        let revert = function () {
            console.log('revert');
            for (let el of self.commentElements) {
                el.classList.remove('inactive');
                el.removeEventListener('click', revert, false);
            }
            // self.setListeners();
        }

        for (let el of this.inactiveElements ) {
            el.addEventListener('click', revert, false);
        }
    }
}

var homeMenu = new HomeMenu;
homeMenu.init();
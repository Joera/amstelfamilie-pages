class Menu {

    constructor() {}

    init() {


        this.openMenuButton = document.getElementById('open-mobile-menu');
        this.closeMenuButton = document.getElementById('close-mobile-menu');
        this.header = document.getElementById('header');
        this.nav = document.getElementById('nav');
        this.navItems = [].slice.call(self.nav.querySelectorAll('li'));

        this.body = document.getElementsByTagName("body")[0];
        this.searchInput = document.getElementById('search-input');

        this.language = document.documentElement.lang;


        this.addOpenMenuListener();
    }

    addOpenMenuListener() {

        var self = this;

        let listener = event => {

            console.log('mobiie');

            event.preventDefault();
            self.open();
            event.target.removeEventListener("click",listener, false);
        }

        if(this.openMenuButton) {
            this.openMenuButton.addEventListener('click', listener, false)
        }

    }

    addCloseMenuListener() {

        var self = this;

        let listenerClose = event => {
            event.preventDefault();
            self.close();
            event.target.removeEventListener("click",listenerClose, false);
        }

        if(this.closeMenuButton) {
            this.closeMenuButton.addEventListener('click', listenerClose, false)
        }
    }

    open() {

        let self = this;

        this.nav.style.display = "flex";
        this.openMenuButton.style.display = "none";

        // let headerHeightOpen = window.innerHeight - 35;


        anime.timeline()
            .add({
                targets: self.header,
                height: [95,window.innerHeight],
                easing: 'easeOutExpo',
                duration: 100

            })
            .add({
                targets: self.navItems,
                opacity: [0,1],
                easing: 'easeInSine',
                duration: 50,
                // delay: anime.stagger(50, {start : 0}),
                complete: function() {
                    self.closeMenuButton.style.display = "flex";
                    self.addCloseMenuListener();
                   // self.searchBox.classList.add('active');
                }
            },'350')
        ;

    }

    close() {

        const self = this;
        self.closeMenuButton.style.display = "none";

        let reversedItems = self.navItems.reverse();

        anime.timeline()
            .add({
                targets: reversedItems,
                opacity: [1,0],
                easing: 'easeInSine',
                duration: 50,
            })
            .add({
                targets: self.header,
                height: [window.innerHeight,95],
                easing: 'easeInSine',
                duration: '100',
                complete: function() {
                    self.openMenuButton.style.display = "flex";
                    self.addOpenMenuListener();
                }
            },'-=200')

        ;
    }

    onEnterSearch(e) {
        let self = this,
            key = e.which || e.keyCode;
        if(key == 13) {

            let renderEnv = 'amstelveenlijn';

            if (this.body.classList.contains('amsteltram')){
                renderEnv = 'amsteltram';
            } else if (this.body.classList.contains('uithoornlijn')){
                renderEnv = 'uithoornlijn';
            }

            self.search(renderEnv);
        }
    }

    search(renderEnv) {

        if (this.language === 'en') {
            window.location.href = '/search/?query=' + this.searchInput.value + '&filter[env]=' + renderEnv;
        } else {
            window.location.href = '/zoeken/?query=' + this.searchInput.value  + '&filter[env]=' + renderEnv;
        }

    }

    // openInput() {
    //
    //
    //     document.querySelector('#search-box').classList.add('active');
    //     document.querySelector('#search-box #search-input').focus();
    //     let button = document.querySelector('#search-box button');
    //
    //     setTimeout( function() {
    //         button.setAttribute('onclick','menu.search()');
    //     },200)
    // }
}

var menu = new Menu;
menu.init();
'use strict';


/**
 *
 */
class Planning {


    constructor(element, data) {
        //
        this.element = element;
        this.dataset = data;
        this.layers = {}; // set layer property
        this.xScroll = 0; // the scroll position on the x axis of the viewport group
        this.scrollNumberOfMonths = 2; // the number of months that are scrolled on the timeline when one of the scroll buttons is pressed
        this.xDragStart = 0; // the scroll position on the x axis of the viewport group on start of drag

        // default configuration
        this.config = {
            margin: { // margins around the plot area
                top: 0,
                right: 3,
                bottom: 20,
                left: 3
            },
            padding: { // padding inside the plot area
                top: 60,
                right: 0,
                bottom: 10,
                left: 0
            },
            controls: {
                scrollButton: {
                    width: 32, // width of the scroll button
                    margin: 10 // margin between the button and the chart
                }
            },
            row: {
                margin: 20, // space between the rows
                futureHeight: 6, // height of the future bars
                pastHeight: 6 // height of the past bars
            },
            monthWidth: 35, // number of pixels on the x axis that 1 month spans
            // perspectiveWidth: 13, // the with of the perspective element, the transition at the cursor between the past and the future bars
            cursor: {
                position: new Date(), // set cursor position to today
                // position: new Date('2018-02-15'),
                labelMargin: { // margin around the cursor label, the cursor label background is x pixels larger then the actual cursor label
                    top: 1,
                    right: 5,
                    bottom: 1,
                    left: 5
                }
            }
        };

        // render chart
        this.render();
    }


    /**
     * Render control function
     * Will render the complete chart
     */
    render() {
        this.setDatasetIds();
        this.setSize();
        this.setScale();
        this.setDefaultScrollPosition();
        this.renderSvg();
        this.renderLayers();
        this.renderScrollControls();
        this.renderBackground();
        this.renderBars(this.config.row.pastHeight, 'past-bars');
        this.renderBars(this.config.row.futureHeight, 'future-bars');
        // this.renderGradientDefinition('gradientPerspective', [{
        //     offset: '0%',
        //     color: '#000',
        //     opacity: 0.3
        // }, {offset: '100%', color: '#000', opacity: 0.15}]);
        this.renderGradientDefinition('gradientSideLeft', [{offset: '0%', color: '#000', opacity: 0.2}, {
            offset: '100%',
            color: '#000',
            opacity: 0
        }]);
        this.renderGradientDefinition('gradientSideRight', [{offset: '0%', color: '#000', opacity: 0}, {
            offset: '100%',
            color: '#000',
            opacity: 0.2
        }]);
        // this.renderPerspective('future-perspective');
        this.renderCursor(this.config.cursor.position);
        this.renderClipMaskBars(this.config.cursor.position, 'future-bars');
        this.renderClipMaskChart();
        this.renderViewportFadingSides();
        this.renderAxis();
        this.setScrollDragEvents();
    }


    /***************************************************************************************************
     * Init functions
     ***************************************************************************************************/

    /**
     * Set id's for all values in the dataset
     * Id's are needed for later reference to the dom nodes
     */
    setDatasetIds() {
        this.dataset = this.dataset.map((d, i) => {
            d.id = i;
            return d;
        });
    }


    /**
     * Calculate the width and height of the plot area
     */
    setSize() {
        let containerSize = d3.select(this.element).node().getBoundingClientRect(), // get the size of the container html element as provided to the constructor function
            rowHeight = d3.max([this.config.row.futureHeight, this.config.row.pastHeight]) + this.config.row.margin; // calculate the height of a row

        // set the width of the chart container
        this.containerWidth = containerSize.width - this.config.margin.left - this.config.margin.right;

        // set the number of pixels one month spans on the plot area
        let spansMonths = this.getMonthDifferance(new Date(this.getDateRange()[0]), new Date(this.getDateRange()[1])); // the number of months the chart spans
        this.config.monthWidth = Math.ceil(this.containerWidth / spansMonths);
        // this.config.monthWidth = Math.ceil(this.containerWidth / 20);
        if (this.config.monthWidth > 40) this.config.monthWidth = 40; // width of month cannot be greater than 40 pixels

        // calculate the width and height of the plot area
        this.width = (this.getMonthCount() * this.config.monthWidth) + this.config.padding.left + this.config.padding.right;
        this.height = ((rowHeight + this.config.row.margin) * this.getRowCount());

        // set the width of the viewport
        this.viewportWidth = this.containerWidth - this.config.margin.left - this.config.margin.right - ((this.config.controls.scrollButton.width + this.config.controls.scrollButton.margin) * 2);

        // check if the calculated width of the chart is smaller than the chart viewport width
        // resize the chart to fit the viewport
        if(this.width < this.viewportWidth) {
            this.config.monthWidth = this.viewportWidth / this.getMonthCount();
            this.width = this.viewportWidth;
        }

    }


    /**
     * Create the scales for positioning elements on the plot area
     */
    setScale() {
        // set the first and last day of the scale
        // set first day to beginning of one month before the first month and last day to the last day of the last month
        let firstDayToBeginOfMonth = this.getDayDifferance(new Date(new Date(this.getDateRange()[0]).getFullYear(), new Date(this.getDateRange()[0]).getMonth(), 1), new Date(this.getDateRange()[0])), // number of days since the month started, of the min date in the domain
            firstDayMonth = (firstDayToBeginOfMonth < 15) ? 1: 0, // the number of months that the chart will show before the first bar starts. If days to end is larger that 15 add extra month.
            firstDay = new Date(new Date(this.getDateRange()[0]).getFullYear(), new Date(this.getDateRange()[0]).getMonth() - firstDayMonth, 1), // the first day that is displayed on the chart, min chart date
            lastDayOfLastMonth = new Date(new Date(this.getDateRange()[1]).getFullYear(), new Date(this.getDateRange()[1]).getMonth() + 1, 0), // the last day of the month, of the max date in the domain
            lastDayToEndOfMonth = this.getDayDifferance(new Date(this.getDateRange()[1]), lastDayOfLastMonth), // number of days before the month ends, of the max date in the domain
            lastDayMonth = (lastDayToEndOfMonth > 15) ? 1: 2, // the number of months that the chart will show after the last bar ends. If days to end is larger that 15 add extra month.
            lastDay = new Date(new Date(this.getDateRange()[1]).getFullYear(), new Date(this.getDateRange()[1]).getMonth() + lastDayMonth, 0); // the last day that is displayed on the chart scale, max chart date

        // x scale
        this.xScale = d3.scaleTime()
            .range([this.config.padding.left, this.width])
            .domain([firstDay, lastDay]);

        // y scale
        this.yScale = d3.scaleBand()
            .rangeRound([0, this.height])
            .domain(this.dataset.map(d => {
                return parseInt(d.position);
            }).filter((value, index, self) => {
                return self.indexOf(value) === index;
            }).sort());
    }


    /**
     * Set the default scroll start position
     */
    setDefaultScrollPosition() {
        // calculate the maximum scroll
        this.maxScroll = ((this.containerWidth - ((this.config.controls.scrollButton.width + this.config.controls.scrollButton.margin) * 2)) - this.width);

        // set scroll position. make sure the cursor is in the center of the viewport as start scroll position
        this.xScroll = this.limitScroll((this.viewportWidth / 2) - this.xScale(this.config.cursor.position));

        // set the number of months that will be scrolled on the timeline when one of the scroll buttons is pressed
        this.scrollNumberOfMonths = Math.floor((this.getMonthCount() / 10)); //
    }


    /**
     * Init the drag event
     */
    setScrollDragEvents() {
        // bind planning class context to the this variable in the drag event handler functions
        let drag = this.dragged.bind(this),
            dragStart = this.dragStarted.bind(this);

        // this.layers.viewport
        //     .on('mousemove.drag', this.mouseMove())
        //     .on('touchmove.drag', this.mouseMove())
        //     .on('mouseup.drag',   this.mouseup())
        //     .on('touchend.drag',  this.mouseup());

        //
        this.layers.chart.call(d3.drag()
            .on('start', dragStart)
            .on('drag', drag)
        );

    }


    /***************************************************************************************************
     * Render functions
     ***************************************************************************************************/

    /**
     * Render the svg container
     */
    renderSvg() {
        this.svg = d3.select(this.element)
            .append('svg')
            // .attr('width', (this.width + this.config.margin.left + this.config.margin.right + this.config.padding.left + this.config.padding.right))
            .attr('width', (this.containerWidth + this.config.margin.left + this.config.margin.right + this.config.padding.left + this.config.padding.right))
            .attr('height', (this.height + this.config.margin.top + this.config.margin.bottom + this.config.padding.top + this.config.padding.bottom))
            .append('g')
            .attr('transform', 'translate(' + this.config.margin.left + ',' + this.config.margin.top + ')');
    }


    /**
     * Render al the layer groups of the chart
     */
    renderLayers() {
        // vieport
        this.layers.viewport = this.svg.append('g')
            .attr('class', 'viewport')
            .attr('transform', 'translate(' + (this.config.controls.scrollButton.width + this.config.controls.scrollButton.margin) + ', 0)');

        // layer contains all the chart renders
        this.layers.chart = this.layers.viewport.append('g')
            .attr('class', 'chart')
            .attr('transform', 'translate(' + this.xScroll + ', 0)');

        // layer contains all the chart controls: scroll buttons
        this.layers.chartControls = this.svg.append('g')
            .attr('class', 'chart-controls');

        //
        this.layers.background = this.layers.chart.append('g')
            .attr('class', 'background');

        //
        this.layers.axis = this.layers.chart.append('g')
            .attr('class', 'axis');

        //
        this.layers.plot = this.layers.chart.append('g')
            .attr('class', 'plot');

        //
        this.layers.legend = this.layers.chart.append('g')
            .attr('class', 'legend');

        // add defs
        this.layers.defs = this.svg.selectAll('defs')
            .data([0])
            .enter()
            .append('defs');
    }


    /**
     * Render scroll buttons
     */
    renderScrollControls() {
        // bind planning class context to the this variable in the scroll event handler function
        let scroll = this.clickScrollButton.bind(this);

        // add group
        let g = this.layers.chartControls.selectAll('g')
            .data(['left', 'right'])
            .enter()
            .append('g')
            .attr('class', (d) => { return 'scroll-button-container-' + d })
            .attr('transform', d => {
                let x = (d === 'right') ? (this.containerWidth - this.config.margin.right - (this.config.controls.scrollButton.width)) : 0;
                return 'translate(' + x + ', ' + ((this.height / 2) + this.config.margin.top) + ')'
            })
            .on('click', scroll);

        // left button
        let l = d3.select('.scroll-button-container-left');
        l.html('<g id="Page-3" stroke="none" fill="none" fill-rule="evenodd"><rect id="Rectangle-31" fill="#F6F5EF" stroke="#000000" stroke-width="1" x="0" y="0" width="32" height="32"></rect><polyline id="Path-2" stroke="#000000" transform="translate(15.624033, 16.000000) scale(-1, 1) translate(-15.624033, -16.000000)" points="13 11 18.2480662 16.2476579 13 21"></polyline></g>');
        l.select('rect').on('click', scroll);

        // right button
        let r = d3.select('.scroll-button-container-right');
        r.html('<g id="Page-3" stroke="none" fill="none" fill-rule="evenodd"><rect id="Rectangle-31" fill="#F6F5EF" stroke="#000000" stroke-width="1" x="0" y="0" width="32" height="32"></rect><polyline id="Path-2" stroke="#000000" points="13 11 18.2480662 16.2476579 13 21"></polyline></g>');
        r.select('rect').on('click', scroll);
    }


    /**
     * Render plot area background
     */
    renderBackground() {
        const offset = 20; // the number of pixels the plot background starts above the first bar

        this.layers.background.append('rect')
            .attr('x', this.config.margin.left)
            .attr('y', this.config.margin.top)
            .attr('width', this.width)
            // .attr('height', this.height)
            .attr('height', (this.height + this.config.padding.top + this.config.padding.bottom))
            .attr('class', 'background');

        this.layers.background.append('rect')
            .attr('x', this.config.margin.left)
            .attr('y', this.config.margin.top + this.config.padding.top - offset)
            .attr('width', this.width)
            // .attr('height', this.height)
            .attr('height', (this.height + this.config.padding.bottom + offset))
            .attr('class', 'plot-background');
    }


    /**
     * Render the bars
     * @param barHeight {int}                   Height of the bars
     * @param groupClass {string}               Css class of the bar-container group
     */
    renderBars(barHeight, groupClass) {
        // add group
        let g = this.layers.plot.selectAll('.' + groupClass)
            .data(this.dataset)
            .enter()
            .append('g')
            .attr('class', groupClass);
        // .on('click', (d) => {
        //     console.log(d);
        // });

        // add bar
        let bar = g.selectAll('.bar')
            .data((d) => {
                return [d];
            })
            .enter()
            .append('rect')
            .attr('x', (d) => {
                return this.xScale(new Date(d.from));
            })
            .attr('y', (d) => {
                // let max = d3.max([this.config.row.futureHeight, this.config.row.pastHeight]),
                //     min = d3.min([this.config.row.futureHeight, this.config.row.pastHeight]),
                //     offset = (max === barHeight) ? 0 : (max - min); // calculate offset to align future and past bars at the bottom. The bars with smallest height needs offset to align to the bottom of heigher bar
                // return this.yScale(d.position) + offset + this.config.margin.top + this.config.padding.top;
                return this.yScale(d.position) + this.config.margin.top + this.config.padding.top;
            })
            .attr('width', (d) => {
                return (this.xScale(new Date(d.to)) - this.xScale(new Date(d.from)));
            })
            .attr('height', barHeight)
            .attr('class', (d) => {
                return d.class;
            });

        // add label
        let label = g.selectAll('.bar-label')
            .data((d) => {
                return [d];
            })
            .enter()
            .append('text')
            .attr('id', (d) => {
                return 'bar-label-' + d.id;
            })
            .attr('text-anchor', 'start')
            .attr('x', (d) => {
                return this.xScale(new Date(d.from));
            })
            .attr('y', (d) => {
                // return this.yScale(d.position) + (d3.max([this.config.row.futureHeight, this.config.row.pastHeight]) + this.config.margin.top + this.config.padding.top) - 5;
                // return this.yScale(d.position) + this.config.margin.top + this.config.padding.top;
                return this.yScale(d.position) + this.config.margin.top + this.config.padding.top;
            })
            .attr('class', 'bar-label')
            .text((d) => {
                return d.name
            });

        // re-position label
        label.each(d => {
            // select label
            let l = d3.select('#bar-label-' + d.id), // this reference is not working properly so using id reference to node
                bbox = l.node().getBBox(); // bounding box of text label
            d3.selectAll('#bar-label-' + d.id)
                // .attr('y', bbox.y + bbox.height + this.config.row.pastHeight);
                // .attr('y', bbox.y + this.config.row.margin + this.config.row.pastHeight + 2); // place label 2 pixels below bar
                // .attr('y', bbox.y + this.config.row.margin); // place label 2 pixels below bar
                // .attr('y', bbox.y);
                .attr('y', this.yScale(d.position) + this.config.margin.top + this.config.padding.top + bbox.height);
        });

    }


    /**
     * Render gradient definitions
     * @param id            id of the gradient
     * @param stops         stops array [{offset: '0%', color: '#000', opacity: 0}]
     */
    renderGradientDefinition(id, stops) {
        // add linear gradient to dom
        let gradient = this.layers.defs.selectAll('#' + id)
            .data([0])
            .enter()
            .append('linearGradient')
            .attr('id', id);

        // add gradient stops to dom
        let stop = gradient.selectAll('stop')
            .data(stops)
            .enter()
            .append('stop')
            .attr('offset', d => {
                return d.offset;
            })
            .attr('stop-color', d => {
                return d.color;
            })
            .attr('stop-opacity', d => {
                return d.opacity;
            });
    }


    /**
     * Render perspective path for the transition between the past and future bars
     * @param groupClass
     */
    renderPerspective(groupClass) {
        // bind the this context to the this value in the perspectivePathGenerator generator function
        let perspectivePathGenerator = this.perspectivePathGenerator.bind(this);

        // add group
        let g = this.layers.plot.selectAll('.' + groupClass)
            .data(this.dataset)
            .enter()
            .append('g')
            .attr('class', groupClass);

        // add perspective path
        let path = g.selectAll('path')
            .data((d) => {
                return [d];
            })
            .enter()
            .append('path')
            .style('display', d => {
                // only display perspective if cursor is on the bar
                if (this.isCursorOnBar(this.config.cursor.position, new Date(d.from), new Date(d.to))) {
                    return 'blocxk';
                } else {
                    return 'none';
                }
            })
            .attr('d', perspectivePathGenerator)
            .attr('class', d => {
                return d.class;
            });

        // add perspective gradient overlay
        let overlay = g.selectAll('.overlay')
            .data((d) => {
                return [d];
            })
            .enter()
            .append('path')
            .style('display', d => {
                // only display perspective if cursor is on the bar
                if (this.isCursorOnBar(this.config.cursor.position, new Date(d.from), new Date(d.to))) {
                    return 'block';
                } else {
                    return 'none';
                }
            })
            .attr('class', d => {
                return 'overlay ' + d.class;
            })
            .attr('d', perspectivePathGenerator)
            .style('fill', 'url(#gradientPerspective)');
    }


    /**
     * Render the chart cursor
     * @param position {date object}            The position of the cursor
     */
    renderCursor(position) {
        // add group
        let g = this.layers.plot.selectAll('.cursor')
            .data([position])
            .enter()
            .append('g')
            .attr('transform', (d) => {
                return 'translate(' + this.xScale(d) + ',' + '0)';
            })
            .attr('class', 'cursor');

        // cursor label
        // add cursor label group
        let labelGroup = g.selectAll('.cursor-label-group')
            .data([position])
            .enter()
            .append('g')
            .attr('transform', 'translate(0, 2)')
            .attr('class', 'cursor-label-group');

        // add cursor label background
        let labelBackground = labelGroup.selectAll('.cursor-label-background')
            .data([position])
            .enter()
            .append('rect')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', 0)
            .attr('height', 0)
            .attr('class', 'cursor-label-background');

        // add cursor label text
        let label = labelGroup.selectAll('.cursor-label')
            .data([position])
            .enter()
            .append('text')
            .attr('text-anchor', 'start')
            .attr('x', this.config.cursor.labelMargin.left)
            .attr('y', this.config.cursor.labelMargin.top)
            // .attr('dx', '-.2em')
            // .attr('dy', '.1em')
            .attr('class', 'cursor-label')
            .text('Vandaag');

        // label height for positioning label later on
        let labelHeight = 0;

        // set label background size
        // size is based on the bounding box of the text element
        labelBackground
            .attr('width', (d) => {
                let bbox = label.node().getBBox();
                labelHeight = bbox.height; // set hieght of label for positioning the label
                return bbox.width + (this.config.cursor.labelMargin.left + this.config.cursor.labelMargin.right);
            })
            .attr('height', (d) => {
                let bbox = label.node().getBBox();
                return bbox.height + (this.config.cursor.labelMargin.top + this.config.cursor.labelMargin.bottom);
            });

        // position cursor label
        label
            .attr('y', this.config.cursor.labelMargin.top + (labelHeight*0.75));

        // cursor line
        // add cursor line
        let line = g.selectAll('.cursor-line')
            .data([position])
            .enter()
            .append('line')
            .attr('x1', 0)
            .attr('y1', (d) => {
                return this.config.margin.top;
            })
            .attr('x2', 0)
            .attr('y2', (this.height + this.config.padding.top + this.config.padding.bottom))
            .attr('class', 'cursor-line');
    }


    /**
     * Render the clipping path to hide the future bar part to the right of the cursor
     * @param position {date object}            The position of the cursor
     * @param groupClass {string}               Css class of the bar-container group where the clipping path is applied to
     */
    renderClipMaskBars(position, groupClass) {
        // define clip path
        let clip = this.layers.plot.append('svg:clipPath')
            .attr('id', 'clipBars')
            .append('rect')
            .attr('x', this.xScale(position))
            .attr('y', 0)
            .attr('width', (this.width + this.config.margin.left + this.config.margin.right))
            .attr('height', (this.height + this.config.margin.top + this.config.margin.bottom + this.config.padding.top + this.config.padding.bottom));

        // apply clipping path to bar container
        this.layers.plot.selectAll('.' + groupClass)
            .attr('clip-path', (d, i) => {
                return 'url(#clipBars)';
            })
    }


    /**
     * Render the clipping path to hide elements outside the viewport layer
     */
    renderClipMaskChart() {
        // define clip path
        let clip = this.svg.append('svg:clipPath')
            .attr('id', 'clipChart')
            .append('rect')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', this.viewportWidth)
            .attr('height', (this.height + this.config.margin.top + this.config.margin.bottom + this.config.padding.top + this.config.padding.bottom));

        // apply clipping path to viewport layer
        this.svg.selectAll('.viewport')
            .attr('clip-path', 'url(#clipChart)')
    }


    /**
     * Render the fading effect sides of the viewport
     */
    renderViewportFadingSides() {
        //
        this.layers.viewport.selectAll('.fading-side-left')
            .data([0])
            .enter()
            .append('rect')
            .attr('x', 0)
            .attr('y', 0)
            .attr('width', 10)
            .attr('height', (this.height + this.config.padding.top + this.config.padding.bottom))
            .attr('class', 'fading-side-left')
            .style('fill', 'url(#gradientSideLeft)');

        //
        this.layers.viewport.selectAll('.fading-side-right')
            .data([0])
            .enter()
            .append('rect')
            .attr('x', this.viewportWidth - 10)
            .attr('y', 0)
            .attr('width', 10)
            .attr('height', (this.height + this.config.padding.top + this.config.padding.bottom))
            .attr('class', 'fading-side-right')
            .style('fill', 'url(#gradientSideRight)');
    }


    /**
     * Render the axis
     */
    renderAxis() {
        // add grid lines
        this.layers.axis.append('g')
            .attr('class', 'grid')
            .attr('transform', 'translate(0,' + (this.height + this.config.padding.top + this.config.padding.bottom) + ')')
            .call(this.renderGridLines()
                .tickSize(-(this.height + this.config.padding.top + this.config.padding.bottom))
                .tickFormat('')
            );

        // set the interval of the month labels on the x axis
        // let axisLabelInterval = (this.config.monthWidth < 30) ? 3 : 1;
        let axisLabelInterval = 1 ;
        if((this.config.monthWidth < 30)) { axisLabelInterval = 3 };
        if((this.config.monthWidth < 20)) { axisLabelInterval = 0 };

        // add x axis month labels
        this.layers.axis.append('g')
            // .attr('transform', 'translate(0,' + (this.height + this.config.padding.top + this.config.padding.bottom) + ')')
            .attr('transform', 'translate(0,20)')
            .attr('class', 'month-label')
            .call(d3.axisBottom(this.xScale).ticks(d3.timeMonth.every(axisLabelInterval)).tickSize(0).tickFormat(d3.timeFormat('%b')));

        // add x axis year labels
        this.layers.axis.append('g')
            .attr('transform', 'translate(0,2)')
            .call(d3.axisBottom(this.xScale).ticks(d3.timeYear.every(1)).tickSize(0).tickFormat(d3.timeFormat('%Y')));

        // adjust axis text label position
        this.layers.axis.selectAll('.tick text')
            .attr('text-anchor', 'middle')
            .attr('dx', (d) => {
                return (this.config.monthWidth / 2) + 'px';
            })
            .attr('dy', '.9em');
    }


    /**
     * Render the grid lines
     * @returns {*}
     */
    renderGridLines() {
        return d3.axisBottom(this.xScale)
            .ticks(d3.timeMonth.every(1)); // thick every month
    }


    /***************************************************************************************************
     * Event handlers
     ***************************************************************************************************/


    dragStarted(d) {
        // set start position on scroll start
        // needed for calculating the distance of the drag
        this.xDragStart = d3.event.x; //
    }


    dragged(d) {
        // calculate the drag distance and add it to the previous position
        this.xScroll += (d3.event.x - this.xDragStart) / 14;
        // this.xScroll += d3.event.x - this.xDragStart;

        // limit the drag to the edges of the chart
        this.xScroll = this.limitScroll(this.xScroll);

        // move the viewport group
        d3.selectAll('g.chart')
            .attr('transform', 'translate(' + this.xScroll + ',0)');
    }


    clickScrollButton(d) {
        // set new scroll position
        if (d === 'right') {
            this.xScroll -= (this.config.monthWidth * this.scrollNumberOfMonths);
        } else {
            this.xScroll += (this.config.monthWidth * this.scrollNumberOfMonths);
        }

        // limit scroll to bounds of chart
        this.xScroll = this.limitScroll(this.xScroll);

        // move the viewport group
        d3.selectAll('g.chart')
            .transition()
            .duration(300)
            .attr('transform', 'translate(' + this.xScroll + ',0)');
    }


    /***************************************************************************************************
     * Helper functions
     ***************************************************************************************************/

    /**
     * Generator function generates path for the perspective effect transition between the past and future bars
     * @param d
     * @returns {string}
     */
    perspectivePathGenerator(d) {
        let xMinBar = this.xScale(new Date(d.from)), // the minimal x position of the bars
            xMaxBar = this.xScale(new Date(d.to)), // the maximal x position of the bars
            xMin = this.xScale(new Date(this.config.cursor.position)) - this.config.perspectiveWidth, // min x of the overlay
            xMax = this.xScale(new Date(this.config.cursor.position)), // max x of the overlay
            yMin = this.yScale(d.position) + this.config.margin.top + this.config.padding.top, // min y of the overlay
            yMax = this.yScale(d.position) + this.config.margin.top + this.config.padding.top + this.config.row.futureHeight, // max y of the overlay
            yMiddle = yMax - d3.min([this.config.row.futureHeight, this.config.row.pastHeight]); // the y position of the

        // set left edge of the perspective to beginnen of bar if the is no space on the bar to use the regular width
        if (xMinBar > xMin) {
            xMin = xMinBar;
        }

        // set path
        let path = 'M' + xMin + ' ' + yMax + ' L' + xMin + ' ' + yMiddle + ' L' + xMax + ' ' + yMin + ' L' + xMax + ' ' + yMax + ' Z';

        // don't show path if cursor doesn't overlap the bar
        if (xMaxBar < xMin) {
            path = '';
        }

        return path;
    }


    /**
     * Return the number of rows
     * Scan the dataset position property and return the highest position number.
     * Minimum value to be returned is 1
     * @returns {number|*}
     */
    getRowCount() {
        // set the number of rows in the chart
        let rowCount = d3.max(this.dataset, (d) => {
            return parseInt(d.position);
        });

        // make sure that the row counts has a minimum value of 1
        if (rowCount < 1) rowCount = 1;

        return rowCount;
    }


    /**
     * Return the minimum and maximum date in the dataset
     * @returns {array}             [minimumDate, maximumDate]. Dates are strings formatted yyyy-mm-dd
     */
    getDateRange() {
        // set the date range
        let dates = [];

        // create array containing all the dates, from and to dates
        this.dataset.forEach(d => {
            dates.push(d.from);
            dates.push(d.to);
        });

        // get date range
        let dateRange = d3.extent(dates, (d) => {
            return d;
        });

        return dateRange;
    }


    /**
     * Returns the number of months the chart spans
     * @returns {number}
     */
    getMonthCount() {
        return new Date(this.getDateRange()[1]).getMonth() - new Date(this.getDateRange()[0]).getMonth() + (12 * (new Date(this.getDateRange()[1]).getFullYear() - new Date(this.getDateRange()[0]).getFullYear()));
    }


    /**
     * Get the differance in days between 2 dates
     * @param first             date object
     * @param second            date object
     * @returns {number}
     */
    getDayDifferance(first, second) {
        return Math.round((second-first)/(1000*60*60*24));
    }


    /**
     * Get the differance in months between 2 dates
     * @param first             date object
     * @param second            date object
     * @returns {number}
     */
    getMonthDifferance(first, second) {
        var months;
        months = (second.getFullYear() - first.getFullYear()) * 12;
        months -= first.getMonth() + 1;
        months += second.getMonth();
        return months <= 0 ? 0 : months;
    }


    /**
     * Returns true if the cursor is on the bar
     * @param cursor            date object
     * @param barFrom           date object
     * @param barTo             date object
     * @returns {boolean}
     */
    isCursorOnBar(cursor, barFrom, barTo) {
        return (cursor > barFrom && cursor < barTo);
    }


    /**
     * Returns true if bar is in the future compared to cursor
     * @param cursor            date object
     * @param barFrom           date object
     * @returns {boolean}
     */
    isFutureOnBar(cursor, barFrom) {
        return (cursor < barFrom);
    }


    /**
     * Make sure the scroll value does not exceed the bounds of the viewport
     * @param scrollValue
     * @returns {*}
     */
    limitScroll(scrollValue) {
        let returnValue = scrollValue;
        if (this.maxScroll > scrollValue) returnValue = this.maxScroll; // make sure scroll don't exceed the max scroll value
        if (0 < scrollValue) returnValue = 0; // make sure scroll don't have a negative value
        return returnValue;
    }


}

module.exports = Planning;
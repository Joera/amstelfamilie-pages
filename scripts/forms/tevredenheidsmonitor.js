'use strict';


var FeedbackForm = function FeedbackForm(formID) {

	var hasErrors = true;
    var form = null;
    var messageField = null;
    var inputEmail = null;
    var fields = [];
    var body = {};
    var token = null;
    var contact = {};
    let data;

    form = document.getElementById(formID);

    messageField = document.querySelector('div[role=alert]');
    fields = [].slice.call(form.getElementsByClassName('field-wrapper'));

	var validate = function validate() {

        for (var i = 0; i < fields.length; i++) {

            var input = fields[i].getElementsByTagName('input')[0];

            if (input !== undefined && input.hasAttribute("aria-invalid") && input.getAttribute("aria-invalid") === 'true') {

                messageField.textContent = 'Niet alle velden zijn compleet ingevuld.';
                input.classList.add('warning');
                return false;
            }

            // if (textarea !== undefined && textarea.hasAttribute("aria-invalid") && textarea.getAttribute("aria-invalid") === 'true') {
            //
            //     errorField.textContent = 'U bent vergeten een bericht te schrijven.'
            //     input.classList.add('warning');
            //     return false;
            // }
        }

        return true;
	}



	var collect = function collect() {

        for (var i = 0; i < fields.length; i++) {

        	if (fields[i].getElementsByClassName('checkbox').length > 1) {

                var choices = [].slice.call(fields[i].getElementsByClassName('checkbox'));
				var choice = [].slice.call(choices[0].getElementsByTagName('input'))[0];

                body[choice.getAttribute('name')] = [];

                choices.forEach( c => {

                	var input = [].slice.call(c.getElementsByTagName('input'))[0];

                	if (input.checked) {
                        body[choice.getAttribute('name')].push(input.getAttribute('id'));
                    }
				});

			} else if (fields[i].getElementsByClassName('checkbox').length > 0) {

                let checkbox = [].slice.call(fields[i].getElementsByTagName('input'))[0];

                if (checkbox) {

                    if (checkbox.value === 'true') {

                        if (checkbox.checked) {
                            body[checkbox.getAttribute('name')] = true;
                        } else {
                            body[checkbox.getAttribute('name')] = false;
                        }

                    } else {

                        if (checkbox.checked) {
                            body[checkbox.getAttribute('name')] = checkbox.value;
                        } else {
                            body[checkbox.getAttribute('name')] = 'none';
                        }
                    }
                }

            } else if(fields[i].getElementsByTagName('input').length > 0) {

                var field = [].slice.call(fields[i].getElementsByTagName('input'))[0];
                body[field.getAttribute('id')] = field.value;

			} else if (fields[i].getElementsByTagName('select').length > 0) {

                var select = [].slice.call(fields[i].getElementsByTagName('select'))[0];
                body[select.getAttribute('id')] = select.value;
			}
        }

        return body;
    }

	var submit = function submit(data) {

			console.log(data);

			var xhr = new XMLHttpRequest();
			xhr.open('POST', WP_URL + '/wp-json/wp/v2/respondent');
			xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
            xhr.send(JSON.stringify(data));

			xhr.onreadystatechange = function () {
			  var DONE = 4; // readyState 4 means the request is done.
			  var OK = 200; // status 200 is a successful return.
			  if (xhr.readyState === DONE) {
                  console.log(xhr);
			   if (xhr.status === OK) {

			    messageField.textContent = xhr.response.replace(/\"+/g,'');
			    } else {
			      console.log('oeps'); // dan weer eruit halen // An error occurred during the request.
			   }
			  }
			};
	}

	var unsubscribe = function unsubscribe(token) {

        var xhr = new XMLHttpRequest();
        xhr.open('DELETE', WP_URL + '/wp-json/wp/v2/respondent');
        xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        xhr.send(JSON.stringify({'token' : token }));

        xhr.onreadystatechange = function () {
            var DONE = 4; // readyState 4 means the request is done.
            var OK = 200; // status 200 is a successful return.
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {
                    messageField.innerHTML = JSON.parse(xhr.response).replace(/\"+/g,'');
                    window.scrollTo(0,getOffset(messageField).top - 140);

                } else {
                    console.log('oeps'); // dan weer eruit halen // An error occurred during the request.
                }
            }
        };
    }

	var getDetails = function getDetails(token) {

        var xhr = new XMLHttpRequest();
        xhr.open('GET', WP_URL + '/wp-json/wp/v2/respondent?token=' + token);
        xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4; // readyState 4 means the request is done.
            var OK = 200; // status 200 is a successful return.
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {
                    console.log(xhr.response);
                    populateFields(JSON.parse(xhr.response).data);
                } else {
                    console.log('oeps'); // dan weer eruit halen // An error occurred during the request.
                }
            }
        };
    }
    var populateFields = function populateFields(contact) {

        window.scrollTo(0,getOffset(form).top - 140);

        ['subscription','panel_data'].forEach (g => {

            Object.keys(contact[g]).forEach( prop => {

                let el = form.querySelector('#' + prop);

                if (el && el.type === 'checkbox') {

                        if (el.value === contact[g][prop] || contact[g][prop] === true || contact[g][prop] === 'true') {
                            el.checked = true;
                        }

                } else if (el) {

                    form.querySelector('#' + prop).value = contact[g][prop];

                } else {

                    if (Array.isArray(contact[g][prop])) {
                        contact[g][prop].forEach( checkbox => {
                            form.querySelector('input#' + checkbox).checked = true;
                        });
                    }
                }
            });
        })

        form.querySelector('input[type=submit]').innerHTML = 'Wijzig uw gegevens';

    }

    var getOffset = function getOffset( el ) {
        var _x = 0;
        var _y = 0;
        while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
            _x += el.offsetLeft - el.scrollLeft;
            _y += el.offsetTop - el.scrollTop;
            el = el.offsetParent;
        }
        return { top: _y, left: _x };
    }


    var getParameterByName = function getParameterByName(name) {

        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };

	var validateEmail = function validateEmail(email) {
	  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return re.test(email);
	}

	var addEventListenerOnce = function addEventListenerOnce(target, type, listener) {
    	target.addEventListener(type, function fn(event) {
	        target.removeEventListener(type, fn);
	        listener(event);
	    });
    }


    if (getParameterByName('action') === 'unsubscribe') {

        var token = getParameterByName('token');
        if(token) {
            unsubscribe(token);
        }

    } else if (getParameterByName('token')) {

        var token = getParameterByName('token');
        if(token) {
            document.getElementById('token').value = token;
            getDetails(token);
        }
    }

    let listener = event => {

        event.preventDefault();
        data = collect();
        if(validate(data)) {
            submit(data,event.target);
            event.target.removeEventListener("submit",listener, false);
            event.target.querySelector('input[type=submit]').disabled = true;
        }
    };


    if(form) {
        form.addEventListener("submit", listener, false);
    }

}

let feedbackForm = FeedbackForm('feedbackform');



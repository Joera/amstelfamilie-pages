

class ContactForm {

	constructor() { 

		this.hasErrors = true; 
	}

	init() { 

		var self = this;

		this.form = document.getElementById('contactform');
        this.message = {};

        let listen = function(event) {

   			 	event.preventDefault(); // Prevent form submission and contact with server
				let content =  self.collect(event.target);
				let valid = self.validate(content);
				if(valid) {
					self.submit(event.target,content);
				}
		}

		if(this.form) {
            this.form.addEventListener("submit", listen, false);
        }
	}

	collect(form) {

		return {

			'name' : form.querySelector('input[type=text').value,
			'email' : form.querySelector('input[type=email').value,
			'message' : form.querySelector('textarea').value
		}
	}

	validate(content) {

		return (content.name != '' && this.validateEmail(content.email) && content.message != '') ? true : false;

	}

	submit(form,content) {

			var self = this;

			// console.log(form.getAttribute('data-render-env'));

        	form.querySelector('div[role=alert]').textContent = "Uw bericht wordt verzonden.";

			var xhr = new XMLHttpRequest();
			xhr.open('POST', WP_URL + '/wp-json/wp/v2/form?type=contact&env=' + form.getAttribute('data-render-env'));
			xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
			xhr.send(JSON.stringify(content));

			xhr.onreadystatechange = function () {
			  var DONE = 4; // readyState 4 means the request is done.
			  var OK = 200; // status 200 is a successful return.
			  if (xhr.readyState === DONE) {
			   if (xhr.status === OK) {
			      	form.querySelector('div[role=alert]').textContent = "Uw bericht is succesvol verzonden.";
                   	form.querySelector('textarea').value = "";
                   	form.querySelector('input[type=text').value = "";
                   	form.querySelector('input[type=email').value = "";


			    } else {
                   form.querySelector('div[role=alert]').textContent = "Er is iets misgegaan. De server is tijdelijke onbereiakbaar.";
			   }
			  }
			};
	}

	validateEmail(email) {
	  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return re.test(email);
	}
}

var contactForm = new ContactForm;
contactForm.init(); 
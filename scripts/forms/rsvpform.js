'use strict';


var RSVPForm = function RSVPForm(formID) {

	var hasErrors = true;
    var form = null;
    var messageField = null;
    var loader = null;
    var inputEmail = null;
    var fields = [];
    var body = {};
    var token = null;
    var contact = {};
    let data;
    const WP_URL = 'https://cms.amsteltram.nl'

    form = document.getElementById(formID);

    messageField = document.querySelector('div[role=alert]');
    loader = document.querySelector('.loader');
    fields = [].slice.call(form.getElementsByClassName('field-wrapper'));

	var validate = function validate() {

        for (var i = 0; i < fields.length; i++) {

            var input = fields[i].getElementsByTagName('input')[0];

            if (input !== undefined && input.hasAttribute("aria-invalid") && input.getAttribute("aria-invalid") === 'true') {

                messageField.textContent = 'Niet alle velden zijn compleet ingevuld.';
                input.classList.add('warning');
                return false;
            }



            // if (textarea !== undefined && textarea.hasAttribute("aria-invalid") && textarea.getAttribute("aria-invalid") === 'true') {
            //
            //     errorField.textContent = 'U bent vergeten een bericht te schrijven.'
            //     input.classList.add('warning');
            //     return false;
            // }
        }

        return true;
	}



	var collect = function collect() {

        for (var i = 0; i < fields.length; i++) {

        	if (fields[i].getElementsByClassName('checkbox').length > 1) {

                var choices = [].slice.call(fields[i].getElementsByClassName('checkbox'));
				var choice = [].slice.call(choices[0].getElementsByTagName('input'))[0];

                body[choice.getAttribute('name')] = [];

                choices.forEach( c => {

                	var input = [].slice.call(c.getElementsByTagName('input'))[0];

                	if (input.checked) {
                        body[choice.getAttribute('name')].push(input.getAttribute('id'));
                    }
				});

			} else if (fields[i].getElementsByClassName('checkbox').length > 0) {



                let checkbox = [].slice.call(fields[i].getElementsByTagName('input'))[0];

                if (checkbox) {

                    if (checkbox.value === 'true') {

                        if (checkbox.checked) {
                            body[checkbox.getAttribute('name')] = true;
                        } else {
                            body[checkbox.getAttribute('name')] = false;
                        }

                    } else {

                        if (checkbox.checked) {
                            body[checkbox.getAttribute('name')] = checkbox.value;
                        } else {
                            body[checkbox.getAttribute('name')] = 'none';
                        }
                    }
                }

            } else if (fields[i].getElementsByClassName('radio').length > 0) {

                var input = fields[i].getElementsByTagName('input')[0];

                if (input.checked) {
                    body[input.getAttribute('name')] = input.value;
                }

            } else if(fields[i].getElementsByTagName('input').length > 0) {

                var field = [].slice.call(fields[i].getElementsByTagName('input'))[0];
                body[field.getAttribute('id')] = field.value;

			} else if (fields[i].getElementsByTagName('select').length > 0) {

                var select = [].slice.call(fields[i].getElementsByTagName('select'))[0];
                body[select.getAttribute('id')] = select.value;
			}
        }

        return body;
    }

    var confetti = function confetti() {

        var canvas = document.querySelector("canvas"),
            context = canvas.getContext("2d");

        canvas.width = window.outerWidth;
        canvas.height = window.outerHeight;

        var particles = [],
            colors =  [geel, oranje, rood, blauw, navy, donkergeel]; // [geel, oranje, rood, blauw, navy, donkergeel];

        function addParticles() {
            for(var i = 0; i < 100; i++) {
                particles.push({
                    x: Math.random()*window.outerWidth,
                    y: Math.random()*window.outerHeight,
                    z: Math.random()*4+7,
                    xd: Math.random()*4-2,
                    zd: Math.random() < 0.5 ? -1 : 1,
                    cl: colors[parseInt(Math.random()*colors.length)]
                })
            }
        }

        function renderAnimation() {
            context.fillStyle = "#fff";
            context.rect(0, 0, canvas.width, canvas.height);
            context.fill();

        //    console.log(particles);

            for(var i = 0; i < particles.length; i++) {

                context.beginPath();
                context.fillStyle = particles[i].cl; // +parseInt(particles[i].z);
                context.arc(particles[i].x, particles[i].y, particles[i].z, 0, 10);
                context.fill();
                context.closePath();

                particles[i].x += particles[i].xd;

                if(particles[i].xd < -1) {
                    particles[i].xd += 0.1;
                }

                if(particles[i].xd > 1) {
                    particles[i].xd -= 0.1;
                }

                if(particles[i].xd > -1 && particles[i].xd < 1) {
                    particles[i].xd = Math.random()*4-2;
                }

                particles[i].z += particles[i].zd;

                if(particles[i].z < 4) {
                    particles[i].zd = 0.5;
                }

                if(particles[i].z > 7) {
                    particles[i].zd = -0.5;
                }

                particles[i].y += particles[i].z;

                if(particles[i].y > window.outerHeight) {
                    particles[i].x = Math.random()*window.outerWidth;
                    particles[i].y = Math.random()*50-25;
                }
            }

            window.requestAnimationFrame(renderAnimation);
        }

        addParticles();
        renderAnimation();
    }

	var submit = function submit(data) {

			var xhr = new XMLHttpRequest();
			xhr.open('POST', WP_URL + '/wp-json/wp/v2/rsvp');
			xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
            xhr.send(JSON.stringify(data));

			xhr.onreadystatechange = function () {
			  var DONE = 4; // readyState 4 means the request is done.
			  var OK = 200; // status 200 is a successful return.
			  if (xhr.readyState === DONE) {
			   if (xhr.status === OK) {

			       var ifPresent = "Bedankt voor uw gegevens en wat fijn dat u erbij bent op 13 december! We streven ernaar de <i>lijn 25 is er! celebration box</i> rond 9 december bij u thuis te bezorgen. U ontvangt enkele dagen voorafgaand aan de online opening aanvullende informatie en een link naar de livestream van de officiële opening van lijn 25.";
			       var boxOnly = "Bedankt voor uw gegevens en wat jammer dat u er niet bij bent op 13 december! We streven ernaar de <i>lijn 25 is er! celebration box</i> rond 9 december bij u thuis te bezorgen. Kunt u genieten van dit pakket op een voor u geschikt moment."
                   var none = 'Bedankt voor uw gegevens en wat jammer dat u er niet bij bent op 13 december!';

                   if (data.rsvp === 'full') {
                       messageField.innerHTML = ifPresent;
                       confetti();
                   } else if (data.rsvp === 'boxonly') {
                       messageField.innerHTML = boxOnly;
                       confetti();
                   } else {
                       messageField.innerHTML = none;
                   }

                   readonly();

			   } else {
			      console.log('oeps'); // dan weer eruit halen // An error occurred during the request.
			   }
			  }
			};
	}

	// var unsubscribe = function unsubscribe(token) {
    //
    //     var xhr = new XMLHttpRequest();
    //     xhr.open('DELETE', WP_URL + '/wp-json/wp/v2/respondent');
    //     xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    //     xhr.send(JSON.stringify({'token' : token }));
    //
    //     xhr.onreadystatechange = function () {
    //         var DONE = 4; // readyState 4 means the request is done.
    //         var OK = 200; // status 200 is a successful return.
    //         if (xhr.readyState === DONE) {
    //             if (xhr.status === OK) {
    //                 messageField.innerHTML = JSON.parse(xhr.response).replace(/\"+/g,'');
    //                 window.scrollTo(0,getOffset(messageField).top - 140);
    //
    //             } else {
    //                 console.log('oeps'); // dan weer eruit halen // An error occurred during the request.
    //             }
    //         }
    //     };
    // }

	var getDetails = function getDetails(token) {


        var xhr = new XMLHttpRequest();
        xhr.open('GET', WP_URL + '/wp-json/wp/v2/rsvp?token=' + token);
        xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4; // readyState 4 means the request is done.
            var OK = 200; // status 200 is a successful return.
            if (xhr.readyState === DONE) {
                loader.style.display = 'none';
                if (xhr.status === OK) {
                    var res = JSON.parse(xhr.response).response;

                    if (res === 'token not recognized') {
                        document.getElementById('wrong-token').style.display = 'flex';
                    } else {
                        form.style.display = 'flex';
                        contact = res;
                        populateFields(res);
                    }
                } else {
                    console.log('oeps'); // dan weer eruit halen // An error occurred during the request.
                }
            }
        };
    }
    var populateFields = function populateFields() {

      //  window.scrollTo(0,getOffset(form).top - 140);

        Object.keys(contact).forEach( prop => {

            let el = form.querySelector('#' + prop) || form.querySelector('#' + contact[prop]);

            if (el && el.type === 'checkbox') {

                if (el.value === contact[prop] || contact[prop] === true || contact[prop] === 1 || contact[prop] === 'true') {
                    el.checked = true;
                }

            } else if (el && el.type === 'radio') {

                if (el.value === contact[prop] || contact[prop] === true || contact[prop] === 1 || contact[prop] === 'true') {
                    el.checked = true;
                }

            } else if (el) {

                form.querySelector('#' + prop).value = contact[prop];

            } else {

                // if (Array.isArray(contact[prop])) {
                //     contact[prop].forEach( checkbox => {
                //         form.querySelector('input#' + checkbox).checked = true;
                //     });
                // }
            }
        });


     //   form.querySelector('input[type=submit]').innerHTML = 'Wijzig uw gegevens';

    }

    var readonly = function readonly() {
        Object.keys(contact).forEach( prop => {
            let el = form.querySelector('#' + prop);
            if(el && (el.tagName === 'SELECT' || el.type === 'checkbox')) {
                el.disabled = true;
            } else if(el) {
                el.readOnly = true;
            }
        });
    }

    var convertBoolToInt = function convertBoolToInt(bool) {
	    return bool ? 1 : 0;
    }

    var compare = function compare(data,contact) {

	    if(
	        data.aanhef === contact.aanhef &&
            data.voornaam === contact.voornaam &&
            data.achternaam === contact.achternaam &&
            contact.rsvp === data.rsvp &&
            data.straatnaam === contact.straatnaam &&
            data.huisnummer === contact.huisnummer &&
            data.postcode === contact.postcode &&
            data.woonplaats === contact.woonplaats &&
            contact.privacy === convertBoolToInt(data.privacy)

        ) {
	        return true
        } else {
	        return false;
        }
    }

    var getParameterByName = function getParameterByName(name) {

        var url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };

	var validateEmail = function validateEmail(email) {
	  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return re.test(email);
	}

	var addEventListenerOnce = function addEventListenerOnce(target, type, listener) {
    	target.addEventListener(type, function fn(event) {
	        target.removeEventListener(type, fn);
	        listener(event);
	    });
    }


    // if (getParameterByName('action') === 'unsubscribe') {
    //
    //     var token = getParameterByName('token');
    //     if(token) {
    //         unsubscribe(token);
    //     }
    //
    // } else

    if (getParameterByName('token')) {

        var token = getParameterByName('token');
        if(token) {
            document.getElementById('token').value = token;
            getDetails(token);

        }
    }

    let listener = event => {

        event.preventDefault();
        data = collect();
        if (compare(data,contact)) {
            messageField.textContent = 'U heeft nog geen velden gewijzigd.';
        } else if (data.rsvp === 'none') {
            messageField.textContent = 'U heeft ons nog niet laten weten of u wel of niet komt.';
        } else {
            if (validate(data)) {
                submit(data, event.target);
                event.target.removeEventListener("submit", listener, false);
                event.target.querySelector('input[type=submit]').disabled = true;
            }
        }
    };

    if(form) {
        console.log('0');
        form.addEventListener("submit", listener, false);
    }
}

let rSVPForm = RSVPForm('rsvpform');



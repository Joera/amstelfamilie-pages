

class NewsletterForm {

    constructor() {

        this.hasErrors = true;
    }

    init() {

        var self = this;

        this.forms = [].slice.call(document.querySelectorAll('form.newsletter'));
        this.form = false;
        let email;
        this.message = {};

        let listen = function(event) {

            let formData = {};

            event.preventDefault();

            let needsName = event.target.querySelector('#first_name') ? true : false;

            if (needsName) {

                formData.first_name = event.target.querySelector('#first_name').value;
                formData.last_name = event.target.querySelector('#last_name').value;
            }

            formData.email = event.target.querySelector('input[type=email]').value;
            if(self.validate(formData)) {
                self.submit(formData,event.target);
                event.target.removeEventListener("submit",listen, false);
                event.target.querySelector('input[type=submit]').disabled = true;
            }
        }

        if(this.forms) {
            for (let form of this.forms) {
                form.addEventListener("submit", listen, false);
            }
        }
    }

    validate(formData) {

        if (formData.email.length > 0 && this.validateEmail(formData.email)) {
            return true;
        } else {
            return false;
        }
    }

    submit(formData,form) {

        let self = this;
        let url;
        let env = form.getAttribute('data-env');

        if (formData.first_name) {

            url = WP_URL + '/wp-json/wp/v2/form?type=newsletter&env=' + env + '&email=' + formData.email + '&first_name=' + formData.first_name + '&last_name=' + formData.last_name;

        } else {

            url = WP_URL + '/wp-json/wp/v2/form?type=newsletter&env=' + env + '&email=' + formData.email;
        }



        var xhr = new XMLHttpRequest();
        xhr.open('POST',url);
        xhr.setRequestHeader('Content-type','application/x-www-form-urlencoded');
        xhr.send();

        xhr.onreadystatechange = function () {
            var DONE = 4; // readyState 4 means the request is done.
            var OK = 200; // status 200 is a successful return.
            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {
                    console.log(xhr);
                    form.querySelector('span.message').textContent = "Uw aanvraag is succesvol verzonden.";
                    let inputs = [].slice.call(form.querySelectorAll('input'));

                    for (let input of inputs) {
                        input.value = "";
                    }
                } else {
                    form.querySelector('span.message').textContent = "De website heeft geen verbinding kunnen maken met de server.";
                }
            }
        };
    }

    validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

}

var newsletterForm = new NewsletterForm;
newsletterForm.init();
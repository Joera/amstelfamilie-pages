class Dialoog {

	constructor() {

		this.commentsOrder = 'asc';
		this.commentList = document.getElementById('dialoog_wrapper');
	}

	init() {

		if (window.localStorage && localStorage.getItem('commentsOrder')) {

			this.commentsOrder = localStorage.getItem('commentsOrder');

			// if (this.commentsOrder === 'desc') { this.switchCommentsOrder() }
		}
	}

	switchCommentsOrder() {

    	if (this.commentsOrder ==='desc') { this.commentsOrder = 'asc'; } else { this.commentsOrder = 'desc'; }
    	if (window.localStorage) { localStorage.setItem('commentsOrder', this.commentsOrder); }

		var i = this.commentList.childNodes.length;
		while (i--)
  		this.commentList.appendChild(this.commentList.childNodes[i]);
    }

    highlightParticipant(name){

    	let commentElements= this.commentList.getElementsByClassName('reactie');

    	console.log(name);

    	for (let i = 0; i < commentElements.length; i++) { 

    			console.log(commentElements[i].classList)
		    
		    if (commentElements[i].classList.contains(name)){

				commentElements[i].classList.add('highlight');

		    } else {

		    	commentElements[i].classList.remove('highlight')
		    }
		}
    }
} 

class Respond extends Dialoog {

	constructor() {

		super(); 
		this.response = {};
		this.activeParentId = null; 
		this.activeParentSlug = null; 
		this.activeParentTitle = null; 
		this.respondField = null;
	}

	init(){ 

		var self = this; 

		this.commentsOrder = 'desc';

		if (window.localStorage && localStorage.getItem('commentsOrder')) {

			this.commentsOrder = localStorage.getItem('commentsOrder');
		}

		this.respondForm = document.getElementById("respondform");
		this.openButton = document.getElementById("openform");
		this.closeButton = document.getElementById("closeform");

		this.validateForm();

		// var updateLoop = setTimeout(function(){ 

		// 	self.getCommentStreamList(this.commentsOrder); 

		// }, 1 * 1000);

	}

	open(parentId,parentTitle,parentSlug,parentYear,parentCommentCount) {

		let self = this; 

		let titleSpan = document.getElementById("parent_title");

		if (this.openButton) { this.openButton.style.display ="none"; } 
		if (this.closeButton) { this.closeButton.style.display ="block"; }

		this.activeParentId = parentId;
		this.activeParentSlug = parentSlug;
		this.activeParentTitle = parentTitle;
		this.activeParentYear = parentYear;
		this.respondForm.style.display = 'block';
		
		if (titleSpan) { titleSpan.innerHTML = parentTitle; } 

	}

	close() {

		this.respondForm.style.display = 'none';
		if (this.openButton) { this.openButton.style.display ="block"; } 
		if (this.closeButton) { this.closeButton.style.display ="none"; }
	}

	validateForm() {

		var self = this; 

		this.respondForm.addEventListener('submit', function(event,errors) {

			if (errors) {

				event.preventDefault();
				document.getElementById('formerror').textContent = 'Wilt u alle drie de velden invullen?';
			
			} else {

				event.preventDefault();

				if (self.respondForm.parentNode.id === 'comments') {

					self.submitFromDashboard();

				} else { 
					
					self.submitFromPost();
				}
			}
		})

	}

	submitFromDashboard() { 

		var self = this;

		this.response.author = document.getElementById("respondAuthor").value;;
		this.response.email = document.getElementById("respondEmail").value;
		this.response.content = document.getElementById("respondMessage").value;
		this.response.parentId = this.activeParentId;
		this.response.parentSlug= this.activeParentSlug;
		this.response.parentTitle = this.activeParentTitle;
		this.response.parentYear = this.activeParentYear;
		this.response.commentCount = this.activeParentCommentCount;
		this.response.context = 'dashboard';

		// splitsen voor homepage = alles en post is single thread 

		// node api aanroepen ... 
		var xhr = new XMLHttpRequest();
		xhr.open('POST', LIVE_URL + 'node-api/comments/newfromdashboard'); 
		xhr.setRequestHeader('Content-type','application/json');
		xhr.send(JSON.stringify(self.response));

		xhr.onreadystatechange = function () {
		  var DONE = 4; // readyState 4 means the request is done.
		  var OK = 200; // status 200 is a successful return.
		  if (xhr.readyState === DONE) {
		   if (xhr.status === OK) {
		      	
		      	//oudelijst vervangen door nieuwe
		      	var lijstcontainer = document.getElementById("dialoog_wrapper");
		      	lijstcontainer.innerHTML = xhr.response;

		    } else {
		      console.log('fuck'); // dan weer eruit halen // An error occurred during the request.
		   }
		  }
		};

		this.respondForm.style.display = 'none';
		document.getElementById("respondMessage").value = ''; 
		this.activeParent = null; 
	}

	submitFromPost() { 

		var self = this;

		this.response.author = document.getElementById("respondAuthor").value;
		this.response.email = document.getElementById("respondEmail").value;
		this.response.content = document.getElementById("respondMessage").value;
		this.response.parentId = document.getElementById("respondPostId").value;
		this.response.parentSlug = document.getElementById("respondPostSlug").value;
		this.response.parentTitle = document.getElementById("respondPostTitle").value;
		this.response.parentYear = document.getElementById("respondPostYear").value;
		this.response.parentCommentCount = document.getElementById("respondPostCommentCount").value;
		this.response.context = 'post';


		// node api aanroepen ... 
		var xhr = new XMLHttpRequest();
		xhr.open('POST', LIVE_URL + 'node-api/comments/new'); 
		xhr.setRequestHeader('Content-type','application/json');
		xhr.send(JSON.stringify(self.response));

		xhr.onreadystatechange = function () {
		  var DONE = 4; // readyState 4 means the request is done.
		  var OK = 200; // status 200 is a successful return.
		  if (xhr.readyState === DONE) {
		   if (xhr.status === OK) {
		      
		      	//oudelijst vervangen door nieuwe
		      	var lijstcontainer = document.getElementById("dialoog_wrapper");
		      	lijstcontainer.innerHTML = xhr.response;

		    } else {
		      console.log('fuck'); // dan weer eruit halen // An error occurred during the request.
		      console.log(xhr);
		   }
		  }
		};
		
		document.getElementById("respondMessage").value = ''; 
		this.close();
	}

	// getCommentStreamList(order) {

	// 	var self = this;

	// 	// Kan ik hier volgorde / en of filter meegeven? 

	// 	// node api aanroepen ... 
	// 	var xhr = new XMLHttpRequest();
	// 	xhr.open('GET', LIVE_URL + 'node-api/commentstream?order=' + order,true);
	// 	xhr.send();

	// 	xhr.onreadystatechange = function () {
	// 	  var DONE = 4; // readyState 4 means the request is done.
	// 	  var OK = 200; // status 200 is a successful return.
	// 	  if (xhr.readyState === DONE) {
	// 	   if (xhr.status === OK) {
		  
	// 	      	//oudelijst vervangen door nieuwe
	// 	      	var lijstcontainer = document.getElementById("dialoog_wrapper");
	// 	      	lijstcontainer.innerHTML = xhr.response;

	// 	    } else {

	// 	      console.log(xhr.status);
	// 	      console.log('error receiving compiled comment list'); 
	// 	   }
	// 	  }
	// 	};
	// }

	addListener(elem, type, fn) {
        if (elem.addEventListener) {
            elem.addEventListener(type, fn, false);

        } else if (elem.attachEvent) {
            elem.attachEvent("on" + type, function() {
                
                return fn.call(elem, window.event);
        	});
        } else {
            elem["on" + type] = fn;
        }
    }

    subscribeToComments() {


    }

    switchCommentsOrder() {

    	if (this.commentsOrder ==='desc') { this.commentsOrder = 'asc'; } else { this.commentsOrder = 'desc'; }

    	if (window.localStorage) { localStorage.setItem('commentsOrder'); }

    	this.getCommentStreamList(this.commentsOrder);

    }
}

let dialoog = new Dialoog();
dialoog.init();

let respond = new Respond();
respond.init();

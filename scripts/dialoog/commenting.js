class Commenting {

    constructor() {
        this.threadIndex = null;
        this.commentParent = null;
    }
    init() {}
}


class Respond extends Commenting {

    constructor() {
        super();
        this.response = {};
        this.commentParent === null
    }

    init(){

        var self = this;

        this.respondForm = document.getElementById("respondform");
        this.formTextArea = this.respondForm.querySelector('textarea');
        this.formInputFields = this.respondForm.querySelectorAll('input');
        this.theRestofForm = [].slice.call(this.respondForm.querySelectorAll('.half'));
        this.formSubmitButton = this.respondForm.querySelector('button');
        // this.formSubmitWarning = this.respondForm.querySelector('#warning');
        this.commentTemplateNew = document.getElementById("comment-template-new");
        this.commentTemplateReply = document.getElementById("comment-template-reply");
        this.commentsWrapper = document.getElementById("comments-wrapper");
        this.respondPencil = document.getElementById("respond-pencil");
        this.validateForm();
    }

    open() {

        let self = this;

        // let titleSpan = document.getElementById("parent_title");

        this.respondForm.style.display = 'block';

        if (this.openButton) { this.openButton.style.display ="none"; }
        if (this.closeButton) { this.closeButton.style.display ="block"; }

        // this.activeParentId = parentId;
        // this.activeParentSlug = parentSlug;
        // this.activeParentTitle = parentTitle;
        // this.activeParentYear = parentYear;

        //
        // if (titleSpan) { titleSpan.innerHTML = parentTitle; }

    }

    close() {

        this.respondForm.style.display = 'none';
        if (this.openButton) { this.openButton.style.display ="block"; }
        if (this.closeButton) { this.closeButton.style.display ="none"; }
    }

    openThread(el) {

        let self = this;

        this.commentParent = el.parentNode.getAttribute('data-thread-id');
        this.threadElement = document.querySelector('.thread[data-thread-id="' + this.commentParent + '"]');

        let newForm = self.respondForm.cloneNode(true);
        this.respondForm.remove();
        newForm.style.display = 'flex';
    //    this.threadElement.insertBefore(newForm,this.threadElement.children[1]);
        this.threadElement.appendChild(newForm);
        this.onFocusOut();
        this.threadElement.querySelector('.thread-reply').style.display = 'none';
        this.init();
    }

    onFocus() {

        if (this.theRestofForm) {
            this.theRestofForm.forEach(function(el) {
                el.style.visibility = 'visible';
                el.style.opacity = '1';
            });
        }
        this.respondForm.style.height = 'auto';
        this.respondForm.style.overflow = 'visible';
        this.formTextArea.style.height = '8rem';
    }

    onFocusOut() {

        if (this.theRestofForm) {
            this.theRestofForm.forEach(function(el) {
                el.style.visibility = 'hidden';
                el.style.opacity = '0';
            });
        }
        this.formTextArea.style.height = 'auto';
        this.respondForm.style.overflow = 'hidden';
        this.respondForm.style.height = '4.5rem';
    }

    hideButtonTemporarilyAfterSubmit() {

        var self = this;
        this.formSubmitButton.style.display = 'none';
        // this.formSubmitWarning.style.display = 'block';
        setTimeout(function(){
            self.formSubmitButton.style.display = 'block';
            // self.formSubmitWarning.style.display = 'none';
        }, 15000);
    }



    validateForm() {

        var self = this;

        this.respondForm.addEventListener('submit', function(event,errors) {
            event.preventDefault();
            if (errors) {
                document.getElementById('formerror').textContent = 'Wilt u een naam invullen? Dan kunnen wij en anderen gemakkelijker op u reageren.';
            } else {
                self.submitNewComment();
            }
        })
    }

    submitNewComment() {

        // console.log(this.commentParent );

        var self = this;

        this.response.author = document.getElementById("respondAuthor").value;
        this.response.email = document.getElementById("respondEmail").value;
        this.response.content = document.getElementById("respondMessage").value;
        this.response.postId = document.getElementById("respondPostId").value;
        this.response.commentParent = this.commentParent;

        // if (self.response.author === '') { self.response.author  = 'anoniem'; }
        if (self.response.email === '') { self.response.email  = 'anoniem@anoniem.nl'; }

        document.getElementById("respondMessage").value = '';

        if (this.commentParent === null) {
            this.insertNewThread();
        } else {
            this.insertReply();
        }

        let url = WP_URL + '/wp-json/wp/v2/submit_comment?post_id=' + self.response.postId + '&name=' + encodeURIComponent(self.response.author) + '&email=' + self.response.email + '&message=' + encodeURIComponent(self.response.content) + '&comment_parent=' + self.response.commentParent;
        axios.post(url)
            .then(function(response){

                //self.logToNode(response.status,response.request.response,response.request.responseURL);

                if (response.status !== 200) {

                    // DO SOMETHING!!! HELP !!!!!!
                    console.log('foutje bedankt')
                }
            });

        this.hideButtonTemporarilyAfterSubmit();
    }

    insertNewThread() {

        let self = this;
        let tempComment = self.commentTemplateNew.cloneNode(true);
        tempComment.querySelector('h4').innerHTML = this.response.author;
        tempComment.querySelector('.datetime').innerHTML = moment().format('D/MM/YYYY | HH:mm');
        tempComment.querySelector('#commment-template-text').innerHTML = this.response.content;
        tempComment.removeAttribute('id');
        this.commentsWrapper.insertBefore(tempComment, this.commentsWrapper.firstChild);
        this.onFocusOut();
        // this.init();

        setTimeout(function(){
            tempComment.classList.add('visible');
        },100);

    }

    insertReply() {
        let self = this;
        let tempComment = self.commentTemplateReply.cloneNode(true);
        tempComment.querySelector('.datetime').innerHTML = moment().format('D/MM/YYYY | HH:mm');
        tempComment.querySelector('h4').innerHTML = this.response.author;
        tempComment.querySelector('#commment-template-text').innerHTML = this.response.content;
        tempComment.style.display = 'flex';
        // return respondForm to top
        let newForm = self.respondForm.cloneNode(true);
        this.respondForm.remove();
        document.getElementById('respondcontainer').appendChild(newForm);
        this.onFocusOut();
        this.init();
        this.commentParent = null;
        this.threadElement.appendChild(tempComment);

        setTimeout(function(){
            tempComment.classList.add('visible');
        },100);
    }

    rate(el,commentID) {

        let self = this,
            newPercentage,
            url = WP_URL + '/api/respond/rate_comment?comment_id=' + commentID;

        axios.post(url)
            .then(function(response){

                if (response.status !== 200) {
                    console.log('foutje bedankt')
                }
            });

        // console.log(el);
        // let valueContainer= el.parentNode.querySelector('.rating-value');
        // console.log(commentID);

        // this.positiveCount = this.positiveCount + 1;
        // this.totalCount = this.totalCount + 1;
        //
        // this.percentage = Math.round((this.positiveCount / this.totalCount) * 100);
        // this.drawCircle(self.percentage);

    }

    subscribeToComments() {

    }

    // switchCommentsOrder() {

    // 	if (this.commentsOrder ==='desc') { this.commentsOrder = 'asc'; } else { this.commentsOrder = 'desc'; }

    // 	if (window.localStorage) { localStorage.setItem('commentsOrder'); }

    // 	this.getCommentStreamList(this.commentsOrder);

    // }
}

let commenting = new Commenting();
commenting.init();

let respond = new Respond();
respond.init();

if (!Array.prototype.find) {
    Array.prototype.find = function(predicate) {
        if (this == null) {
            throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
            throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;

        for (var i = 0; i < length; i++) {
            value = list[i];
            if (predicate.call(thisArg, value, i, list)) {
                return value;
            }
        }
        return undefined;
    };
}

class Archive {

    constructor() {}


    init() {

        let self = this,
            types,
            item;

        this.types = [].slice.call(document.querySelectorAll('.type'));
        this.content = [].slice.call(document.querySelectorAll('#archived-content #center-column a'));
        this.contentContainer = document.querySelector('#archived-content #center-column #search-content-container');
        this.searchBox = document.querySelector('#archive-search-box');

        this.resultCount = document.querySelector('#result-count-container');
        this.resultCountContainer = document.querySelector('#result-count-container span');
        this.paginationContainer = document.querySelector('#pagination');
        this.tagContainer = document.querySelector('#tag-container');

        let client = algoliasearch('RLZJT7BFZT', '4e27f737eb761680a893fadadad785dc',{protocol: 'https:'});

        this.queryIndex = client.initIndex('AVL_core');
        this.currentIndex = client.initIndex('AVL_core');

        // this.filters = '';
        this.filteredByTag = false;

        this.filter = {};
        this.filter.search = this.getParameterByName('search') || null;
        this.filter.themes = this.getParameterByName('theme') == null ? [] : [this.getParameterByName('theme')];
        this.filter.tags = this.getParameterByName('tag') == null ? [] : [this.getParameterByName('tag')];
        this.filter.types = [];



        if( this.getParameterByName('type')) {

            this.filter.types = [];

            types = this.getParameterByName('type').split(',');
            item;

            types.forEach(function(type) {

                item = {
                    id: type,
                    state: true
                };
                self.filter.types.push(item);
            });

            this._setCheckboxes();

        } else {

            this._initCheckboxes();
        }



        // set search term in search input box when coming from other page
        if (this.filter.search) {
            this.searchBox.value = this.filter.search;
        }

        if (this.getParameterByName('error')) {

            let error = this.getParameterByName('error').replace(/^\/|\/$/g, '');
            this.filter.search = error;
            this.searchBox.value = error;
        }


        this.getSearch(this.currentIndex, 0);


    }

    changeSearch() {

        this.filter.search = this.searchBox.value;
        this.getSearch(this.currentIndex,0);

    }

    getSearch(index,page) {

        let self = this;
        let filters = this._concatFilterString();

        console.log(filters);

        this.currentIndex.search(self.filter.search, {
            facets: '*',
            filters: filters,
            attributesToRetrieve: ['title','content','sections','searchSnippet'],
            page: page,
            hitsPerPage: 16

        }, function searchDone(err, content) {

            self._paramstoTabs(content.params);
            self.closeFilters();
            self._addHTML(content,page);

        });
    }

    toggleType(element, event) {
        this.onChange(element.className, element.id, element.checked);
    };

    onChange(key, param, state) {

        let self = this,
            id,
            typeArray,
            facetFilter = '';


        if (key === 'categories.slug') {
            this.filter.themes.push(param);
        } else if(key === 'tags.slug') {
            this.filter.tags.push(param);
        } else if (key === 'type'){

            this.param = param;

            let obj = self.filter.types.find( (filter, i) => {
                if (filter.id === param) {
                    self.filter.types[i].state = state;
                    return true; // stop searching
                }
            });
        }

        self.getSearch(this.queryIndex, 0);

    }

    clearFilters() {
        this.filter.themes = [];
        this.filter.tags = [];
        this.getSearch(this.queryIndex,0);
    }

    showMoreTags() {

        this.tagContainer.classList.add('show-all');
    }

    openFilters() {
        document.getElementsByClassName('filters')[0].classList.add('show-on-portrait');
    }

    closeFilters() {
        document.getElementsByClassName('filters')[0].classList.remove('show-on-portrait');
    }

    getParameterByName(name, url) {

        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    _concatFilterString() {

        let self = this,
            i,
            facetFilter = '';

        this.filter.themes.forEach(function (theme) {

            if (facetFilter !== '') {
                facetFilter = facetFilter.concat(' AND ')
            }
            facetFilter = facetFilter.concat('categories.slug:' + theme);
            self.filteredByTag = true;
        });

        this.filter.tags.forEach(function(tag) {

            if (facetFilter !== '') {
                facetFilter = facetFilter.concat(' AND ')
            }
            facetFilter = facetFilter.concat('tags.slug:' + tag);
            self.filteredByTag = true;
        });

        for (i = 0; i < this.filter.types.length; i++) {

            if (self.filter.types[i].state) {

                if (facetFilter !== '' && i === 0) {
                    facetFilter = facetFilter.concat(' AND ')
                }
                else if (facetFilter !== '') {
                    facetFilter = facetFilter.concat(' OR ')
                }
                facetFilter = facetFilter.concat('type:' + self.filter.types[i].id);
            }


        }
        return facetFilter;
    }

    _addHTML(content,pageIndex) {

        let self = this,
            snippet,
            textSnippet,
            li,
            tag_li,
            theme_li,
            html = '',
            themeList = [],
            tagList = [],
            tagSlugs = [],
            themeSlugs = [],
            combinedList = [];

        //pageIdex is voor zoekopdract/alogolia .. page is voor li'tjes
        let page = pageIndex + 1;

        self.contentContainer.innerHTML = '';
        self.paginationContainer.innerHTML = '';
        self.tagContainer.innerHTML = '';

        if(content === undefined) { return; }

        content.hits.forEach( function(item){

            if ('searchSnippet' in item) {

                snippet = document.createElement('div');
                snippet.innerHTML = item.searchSnippet;

                if(content.query && item.sections) {

                    let sections = Object.keys(item.sections).map(function(key) {
                        return [Number(key), item.sections[key]];
                    });

                    if (sections.length > 0) {
                        for (let i = 0; i < sections.length; i++) {
                            if (sections[i][1].type === 'paragraph') {
                                item.content = item.content + sections[i][1].text;
                            }
                        }
                    }
                    // find index of first occurence of query
                    if (item.content) {
                        item.content = item.content.replace(/<(?:.|\n)*?>/gm, '');
                        let startOfHighlight = item.content.indexOf(content.query);
                        let endOfHightlight = startOfHighlight + content.query.length
                        item.content = item.content.slice(0,startOfHighlight) + '<b>' + item.content.slice(startOfHighlight,endOfHightlight) + '</b>' + item.content.slice(endOfHightlight);
                        // create snippet of 60 chars - and plus index
                        let min = startOfHighlight - 30;
                        let max = endOfHightlight + 30;

                        if (min < 0) {
                            min = 0;
                            max = endOfHightlight + 50;
                            textSnippet = item.content.slice(min, max);
                            textSnippet = textSnippet + ' ...';
                        } else {
                            textSnippet = item.content.slice(min, max);
                            textSnippet = '... ' + textSnippet + ' ...';
                        }
                        snippet.querySelector('.search-excerpt').innerHTML = textSnippet;
                    }
                }
                self.contentContainer.appendChild(snippet);
            }
        });

        if (content.hits.length < 1) {
            html ='<span>Uw zoekopdracht heeft geen resultaten opgeleverd.</span>';
        }
        // add result count
        self.resultCountContainer.innerHTML = content.nbHits;
        self.resultCount.style.display = 'block';
        // add pagination

        if (content.nbPages > 1) {

            self.paginationContainer.innerHTML = '';
            let firstInView;
            let lastInView;
            if (page < 3) {
                firstInView = 1;
                lastInView = page + 3;
            }
            else if (page === 3) {
                    firstInView = 1;
                    lastInView = 6;
            } else if (page === 4) {
                firstInView = 2;
                lastInView = 7;
            }
            else {
                firstInView = page - 1;
                lastInView = page + 2;
            }

            // items links

            if (page > 1 && page <= content.nbPages) {

                let previous = document.createElement('li');
                previous.innerHTML = '<';
                previous.onclick = function () {
                    archive.getSearch(this.currentIndex, pageIndex - 1)
                };
                self.paginationContainer.appendChild(previous);
            }

            if (page > 3 && page <= content.nbPages) {

                let first = document.createElement('li');
                first.innerHTML = '1';
                first.onclick = function () {
                    archive.getSearch(this.currentIndex, 0)
                };
                self.paginationContainer.appendChild(first);
            }

            if (page > 4 && page <= content.nbPages) {


                let points = document.createElement('li');
                points.innerHTML = '...';
                points.classList.add('points');
                self.paginationContainer.appendChild(points);

            }

            // items active
            for (let i = firstInView; i < lastInView; i++) {
                if (i <= content.nbPages) {
                    li = document.createElement('li');
                    if (i === parseInt(page)) {
                        li.classList.add('active');
                    }
                    li.innerHTML = i;
                    if (i < page) {
                        li.onclick = function () {
                            archive.getSearch(this.currentIndex, pageIndex - 1)
                        };
                    }

                    else if (i > page) {

                        li.onclick = function () {
                            archive.getSearch(this.currentIndex, pageIndex + 1)
                        };

                    }

                    self.paginationContainer.appendChild(li);
                }
            }
            // items rechts
            if (content.nbPages > 4 && page <= content.nbPages - 2) {

                let points = document.createElement('li');
                points.innerHTML = '...';
                points.classList.add('points');
                self.paginationContainer.appendChild(points);

                let last = document.createElement('li');
                last.innerHTML = content.nbPages;
                last.onclick = function () {
                    archive.getSearch(this.currentIndex, content.nbPages - 1)
                };
                self.paginationContainer.appendChild(last);

            }

            if (content.nbPages > 3 && page <= content.nbPages - 1) {


                let next = document.createElement('li');
                next.innerHTML = '>';
                next.onclick = function () {
                    archive.getSearch(this.currentIndex, pageIndex + 1)
                };
                self.paginationContainer.appendChild(next);

            }
        }


        // add themes
        if (content.facets['categories.title']) {
            themeList = this._ObjectToArray(content.facets['categories.title']);
        }
        if (content.facets['categories.slug']) {
            themeSlugs = this._ObjectToArray(content.facets['categories.slug']);
        }
        if (content.facets['categories.title']) {
            tagList = this._ObjectToArray(content.facets['tags.title']);
        }
        if (content.facets['categories.slug']) {
            tagSlugs = this._ObjectToArray(content.facets['tags.slug']);
        }

        for (let a = 0; a < themeList.length; a++) {

            themeList[a]['slug'] = themeSlugs[a]['name'];
        }

        for (let i = 0; i < tagList.length; i++) {

            tagList[i]['slug'] = tagSlugs[i]['name'];
        }

        themeList.forEach( function(item) {

            theme_li = document.createElement('span');
            theme_li .classList.add('tag-tab','smallerfont');
            theme_li .innerHTML = item.name + '<span class="count evensmallerfont">(' + item.count + ')</span>';
            theme_li .onclick = function() { archive.onChange('categories.slug',item.slug) };
            self.tagContainer.appendChild(theme_li);
        });

        // add tags

        for (let i = 0; i < tagList.length; i++) {

            tag_li = document.createElement('span');
            tag_li.classList.add('tag-tab','smallerfont');
            if (i > 2) { tag_li.classList.add('initially_hidden'); }
            tag_li.innerHTML = tagList[i].name + '<span class="count evensmallerfont">(' + tagList[i].count + ')</span>';
            tag_li.onclick = function() { archive.onChange('tags.slug',tagList[i].slug) };
            self.tagContainer.appendChild(tag_li);
            if (i === 2) {
                let divider = document.createElement('span');
                divider.classList.add('tag-tab','divider','smallerfont');
                divider.onclick = function() { archive.showMoreTags()};
                divider.innerHTML = 'toon meer onderwerpen';
                self.tagContainer.appendChild(divider);
            }
        }

        // add clear button

        if (this.filteredByTag) {

            let clearFilter = document.createElement('span');
            clearFilter.classList.add('tag-tab','clear','smallerfont');
            clearFilter.innerHTML = 'filter verwijderen';
            clearFilter.onclick = function () {
                archive.clearFilters()
            };
            self.tagContainer.appendChild(clearFilter);
            self.filteredByTag = false;
        }
    }

    _paramstoTabs(params) {}

    _setCheckboxes() {

        let self = this;
        this.types.forEach( (el) => {

            let array = self.filter.types.map(
                function(t) {
                    return t.id;
                }
            );
            if(array.indexOf(el.id) > -1) {

                el.checked = true;
                let item = {
                    id: el.id,
                    state: true
                };
            } else {
                el.checked = false;
            }
        });
    }

    _initCheckboxes() {

        this.types.forEach( (el) => {

            el.checked = true;
            let item = {
                id: el.id,
                state: true
            };
            this.filter.types.push(item)
        });
    }

    _ObjectToArray(object) {

        let array = [],
            v;

        for (var key in object) {
            if (object.hasOwnProperty(key)) {
                let v = {
                    'name' : key,
                    'count' : object[key]
                }
                array.push(v);
            }
        }
        return array;
    }
}

var archive = new Archive;
// archive.init();

/**
 * Central store for all paths that are used in tha application
 */
module.exports = function() {

    // return {
    //     projectFolder: '/opt/pages',
    //     distFolder: './../dist/',
    //     productionFolder: '/var/www/html',
    //     htmlRoot: '/var/www/html'
    // }


    return {
        projectFolder: '/opt/sg-core-amsfam/pages',
        distFolder: './../dist/',
        productionFolder: '/var/www/vhosts/wijnemenjemee.nl/amsteltram.nl',
        htmlRoot: '/var/www/vhosts/wijnemenjemee.nl/amsteltram.nl'
    }
};

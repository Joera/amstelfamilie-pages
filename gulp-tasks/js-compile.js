'use strict';

/**
 * Remove json data from the news queue
 */
module.exports = function() {

    let gulp = require('gulp'),
        babel = require('gulp-babel'),
        plumber = require('gulp-plumber'),
        concat = require('gulp-concat'),
        merge = require('merge-stream'),
        path = require('../local-path')();

    let scriptsDir = path.productionFolder + '/assets/scripts/pages/';

    gulp.task('js-compile', function() {

        var vendor = gulp.src([

            path.projectFolder + '/assets/vendorjs/__polyfill.min.js',
            path.projectFolder + '/assets/vendorjs/moment.min.js',
            path.projectFolder + '/assets/vendorjs/locale.nl.js',

            path.projectFolder + '/assets/vendorjs/lazysizes.min.js',
            path.projectFolder + '/assets/vendorjs/axios.min.js',
            path.projectFolder + '/assets/vendorjs/algoliasearchLite.min.js',
            path.projectFolder + '/assets/vendorjs/anime.min.js',

            path.projectFolder + '/assets/vendorjs/d3.v4.min.js',
            path.projectFolder + '/assets/vendorjs/hammer.min.js',
        ])
            .pipe(plumber())
            .pipe(concat('vendor.js'))
            .pipe(gulp.dest(path.productionFolder + '/assets/scripts/'));

		var page = gulp.src([
                path.projectFolder + '/scripts/common/polyfill.js',
                path.projectFolder + '/scripts/common/constants.js',
                path.projectFolder + '/scripts/storytelling/main.js',
                path.projectFolder + '/scripts/storytelling/video.js',
                path.projectFolder + '/scripts/expand-content/main.js',
                path.projectFolder + '/scripts/storytelling/appreciation.js',
                path.projectFolder + '/scripts/dialoog/commenting.js',
                path.projectFolder + '/scripts/menu/menu.js',
                path.projectFolder + '/scripts/forms/newsletter.js'
			])
			.pipe(plumber())
			.pipe(concat('page.js'))
			.pipe(babel({
				presets: ['es2015']
			}))
			.pipe(gulp.dest(scriptsDir));

        var werkzaamhedenenhinder = gulp.src([
            path.projectFolder + '/scripts/common/polyfill.js',
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/storytelling/main.js',
            path.projectFolder + '/scripts/menu/menu.js',
            path.projectFolder + '/scripts/forms/newsletter.js'
        ])
            .pipe(plumber())
            .pipe(concat('werkzaamheden-en-planning.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var overhetproject = gulp.src([
            path.projectFolder + '/scripts/common/polyfill.js',
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/storytelling/main.js',
            path.projectFolder + '/scripts/expand-content/main.js',
            path.projectFolder + '/scripts/menu/menu.js',
            path.projectFolder + '/scripts/forms/newsletter.js'
        ])
            .pipe(plumber())
            .pipe(concat('over-het-project.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var english = gulp.src([
            path.projectFolder + '/scripts/common/polyfill.js',
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/storytelling/main.js',
            path.projectFolder + '/scripts/menu/menu.js',
            path.projectFolder + '/scripts/forms/newsletter.js'
        ])
            .pipe(plumber())
            .pipe(concat('english.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var homepage = gulp.src([
            path.projectFolder + '/scripts/common/polyfill.js',
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/common/breakpoints.js',
            path.projectFolder + '/scripts/common/colours.js',
            path.projectFolder + '/scripts/twitter/main.js',
            path.projectFolder + '/scripts/menu/menu.js',
            path.projectFolder + '/scripts/menu/home-menu.js',
            path.projectFolder + '/scripts/forms/newsletter.js',
            path.projectFolder + '/scripts/scrolling/scrollmethods-home.js',
            path.projectFolder + '/scripts/countdown/countdown.js'

            ])
            .pipe(plumber())
            .pipe(concat('homepage.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var homepageAlt = gulp.src([
            path.projectFolder + '/scripts/common/polyfill.js',
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/common/breakpoints.js',
            path.projectFolder + '/scripts/common/colours.js',
            path.projectFolder + '/scripts/twitter/main.js',
            path.projectFolder + '/scripts/menu/menu.js',
            path.projectFolder + '/scripts/menu/home-menu.js',
            path.projectFolder + '/scripts/forms/newsletter.js',
            path.projectFolder + '/scripts/scrolling/scrollmethods-home.js',
            path.projectFolder + '/scripts/countdown/countdown.js'

        ])
            .pipe(plumber())
            .pipe(concat('homepage-alt.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var post = gulp.src([

                path.projectFolder + '/scripts/common/polyfill.js',
                path.projectFolder + '/scripts/common/constants.js',
                path.projectFolder + '/scripts/storytelling/social-dialogue.js',
                path.projectFolder + '/scripts/storytelling/video.js',
                path.projectFolder + '/scripts/storytelling/appreciation.js',
                path.projectFolder + '/scripts/dialoog/commenting.js',
                path.projectFolder + '/scripts/lazyloading/main.js',
                path.projectFolder + '/scripts/menu/menu.js',
                path.projectFolder + '/scripts/forms/newsletter.js'
            ])
            // .pipe(plumber())
            .pipe(concat('post.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));


        var search = gulp.src([
            path.projectFolder + '/scripts/common/polyfill.js',
            path.projectFolder + '/scripts/archive/archive.js',
            path.projectFolder + '/scripts/menu/menu.js',
				// '/scripts/lazyloading/main.js',
            ])
            .pipe(plumber())
            .pipe(concat('search.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var compactePlanning = gulp.src([

            path.projectFolder + '/scripts/common/polyfill.js',
            path.projectFolder + '/scripts/common/breakpoints.js',
            path.projectFolder + '/scripts/common/colours.js',
            path.projectFolder + '/scripts/compacte-planning/_definitions.js',
            path.projectFolder + '/scripts/compacte-planning/_ganttitem.js',
            path.projectFolder + '/scripts/compacte-planning/_lanes.js',
            path.projectFolder + '/scripts/compacte-planning/_mask.js',
            path.projectFolder + '/scripts/compacte-planning/_milestones.js',
            path.projectFolder + '/scripts/compacte-planning/_references.js',
            path.projectFolder + '/scripts/compacte-planning/_timegrid.js',
            path.projectFolder + '/scripts/compacte-planning/main.js',

        ])
            .pipe(concat('compact-planning.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var planningWerkzaamheden = gulp.src([
            path.projectFolder + '/scripts/planning/planning_werkzaamheden.js',
        ])
            .pipe(concat('planning_werkzaamheden.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var map = gulp.src([

            path.projectFolder + '/scripts/common/polyfill.js',
            path.projectFolder + '/scripts/map/__browserDetection.js',
            path.projectFolder + '/scripts/map/__caniusewebgl.js',
            path.projectFolder + '/scripts/map/__colours.js',
            path.projectFolder + '/scripts/map/__loadJSON.js',
            path.projectFolder + '/scripts/map/__polyfills',
            path.projectFolder + '/scripts/map/_mapbox-webgl.js',
            path.projectFolder + '/scripts/map/_points.js',
            path.projectFolder + '/scripts/map/_lines.js',
            path.projectFolder + '/scripts/map/map-init.js',
        ])
            .pipe(concat('map.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var map_works = gulp.src([

            path.projectFolder + '/scripts/common/polyfill.js',
            path.projectFolder + '/scripts/map_works/__browserDetection.js',
            path.projectFolder + '/scripts/map_works/__caniusewebgl.js',
            path.projectFolder + '/scripts/map_works/__colours.js',
            path.projectFolder + '/scripts/map_works/__loadJSON.js',
            path.projectFolder + '/scripts/map_works/__polyfills',
            path.projectFolder + '/scripts/map_works/_mapbox-webgl.js',
            path.projectFolder + '/scripts/map_works/_interaction_popup.js',
            path.projectFolder + '/scripts/map_works/_interaction_page.js',
            path.projectFolder + '/scripts/map_works/_points.js',
            path.projectFolder + '/scripts/map_works/_lines.js',
            path.projectFolder + '/scripts/map_works/_haltes.js',
            path.projectFolder + '/scripts/map_works/_works.js',
            path.projectFolder + '/scripts/map_works/map-init.js',
        ])
            .pipe(concat('map_works.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        var searchIndex = gulp.src([
            path.projectFolder + '/scripts/common/polyfill.js',
            './scripts/search/search-url-parameters.js',
            './scripts/search/search-checkboxes.js',
            './scripts/search/search-dropdown.js',
            './scripts/search/search-helpers.js',
            './scripts/search/search-filters.js',
            './scripts/search/search-pagination.js',
            './scripts/search/search-result-info.js',
            './scripts/search/search-result-content.js',
            './scripts/search/search-init.js'
        ])
            .pipe(concat('search-index.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(path.productionFolder + '/assets/scripts/'));

        var contact = gulp.src([
            path.projectFolder + '/scripts/forms/contact.js',
        ])
        .pipe(concat('contact.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        .pipe(gulp.dest(path.productionFolder + '/assets/scripts/'));

        var newsletter = gulp.src([
            path.projectFolder + '/scripts/forms/newsletter.js',
        ])
            .pipe(concat('newsletter.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(path.productionFolder + '/assets/scripts/'));

        var tevredenheidsmonitor = gulp.src([
            path.projectFolder + '/scripts/forms/tevredenheidsmonitor.js',
        ])
            .pipe(concat('tevredenheidsmonitor.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(path.productionFolder + '/assets/scripts/'));

        var rsvp = gulp.src([
            path.projectFolder + '/scripts/common/polyfill.js',
            path.projectFolder + '/scripts/common/constants.js',
            path.projectFolder + '/scripts/common/breakpoints.js',
            path.projectFolder + '/scripts/common/colours.js',
            path.projectFolder + '/scripts/forms/rsvpform.js',
        ])
            .pipe(concat('rsvp.js'))
            .pipe(babel({
                presets: ['es2015']
            }))
            .pipe(gulp.dest(scriptsDir));

        return merge(page, homepage, homepageAlt, post, search, compactePlanning, planningWerkzaamheden, map, map_works, werkzaamhedenenhinder, overhetproject, english, searchIndex, contact, newsletter, tevredenheidsmonitor,rsvp );  // , map

    });
}

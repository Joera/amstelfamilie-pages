'use strict';

const PagePersistence = require('../../persistence/page.persistence');
const fs = require('fs');
const appConfig = require('../../config/');
const logger = require('../../services/logger.service');
const MapDataService = require('../services/mapData.service')

/**
 * Service for getting locations for the map navigation
 */
class LocationsService {

    constructor() {}

    async createGeojson(render_environments) {

        if(render_environments && render_environments.length > 0) {

            render_environments.forEach(async (env) => {

                let allFeatures = await this._getAllLocations(env);
                let mainFeatures = await this._getMainLocations();

                await this._writeFile(allFeatures, env, 'locations');
                await this._writeFile(mainFeatures, env, 'haltes');

            });

        }
        return;
    }

    _createFeatures (objects, type) {
        let features = [];
        objects.forEach( (object) => {

            let properties = {
                "name": object.title,
                "slug": object.slug,
                "town": object.town,
                "address": object.location ? object.location.address : '',
                "types": object.types,
                "stopType": object.stopType,
                "mainType": object.mainType,
                "crossing": false,
                "url": object.url,
                "type" : type,
                "renderEnv": object.render_environments ? object.render_environments[0] : 'amstelveenlijn'
            };

            if(object.taxonomies['location-types'].indexOf('kruispunt') > -1) {
                properties.crossing = true;
            }

            let point = this._createPoint(properties, object.location.lng, object.location.lat)

            features.push(point);
        });
        return features
    }

    async _getAllLocations(render_environment){

        const pagePersistence = new PagePersistence();
        const mapDataService = new MapDataService();

        let options = {
            query: {
                type: "location",
                render_environments: {$in: [render_environment]},
            },
            "sort": {order: -1}
        };

        let locations = await pagePersistence.find(options);

        let augmentedLocations = await mapDataService._matchTopicalWorks(locations)

        let filteredLocations = augmentedLocations.filter( (location) => {
            return (location.types.indexOf('stop') > -1) || (location.works && location.works.length > 0);
        });

        return {
            "type": "FeatureCollection",
            "features": this._createFeatures(filteredLocations, 'location')
        };
    }

    async _getMainLocations(){

        const pagePersistence = new PagePersistence();

        let options = {
            query: {
                type: "location",
                types: {$in: ['stop']},
            },
            "sort": {order: -1 }
        };

        let locations = await pagePersistence.find(options);

        let features = this._createFeatures(locations, 'location');

        return {
            "type": "FeatureCollection",
            "features": features
        };
    }

    _createPoint(properties, lat, lng){
        lat = parseFloat(lat);
        lng = parseFloat(lng);
        return {
            "type": "Feature",
            "properties": properties,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    lat,
                    lng
                ]
            }
        }
    }

    async _writeFile(data, env, filename) {


        let url = appConfig.dist + 'amsteltram.nl/assets/geojson/' + env + '_' + filename + '.geojson';

        await fs.writeFile(url, JSON.stringify(data), (err) => {
            // if (err) throw err;
            logger.debug(url);
            logger.info(filename + '.geojson has been created at ' + url);
        });

        return;
    }
}

module.exports = LocationsService;

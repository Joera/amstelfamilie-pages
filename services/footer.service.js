'use strict';

const logger = require('../../services/logger.service');
const PagePersistence = require('../../persistence/page.persistence');

class FooterService {

    constructor() {

        this.pagePersistence = new PagePersistence();
    }

    async getContent(env,lan) {

        let id;

        if(env === 'uithoornlijn' && lan === 'nl') {
            id = '32679';
        } else if (env === 'amstelveenlijn' && lan === 'nl') {
            id = '32680';
        } else if (env === 'amsteltram' && lan === 'nl') {
            id = '32681';
        }

        let content = await this.pagePersistence.find({query : { "_id": id }});

        return content[0];
    }
}

module.exports = FooterService;
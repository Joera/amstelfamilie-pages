'use strict';

const logger = require('../../services/logger.service');
const PagePersistence = require('../../persistence/page.persistence');
const moment = require('moment');

class MapDataService {


    constructor() {

        this.pagePersistence = new PagePersistence();
    }


    getLocationsAll(render_environment) {

        let self = this;

        let relatedWorksOptions,
            options,
            mapData = {
                'all' : [],
                'filtered' : []
            };

        return new Promise(function(resolve, reject) {

                options = {
                    query : {
                        "type":"location",
                        "render_environments": { $in: [render_environment] }
                    },
                    "sort": {order: 1}
                };

                self.pagePersistence.find(options).then( function(locations) {
                    let mapDataAll = self._matchWorks(locations);
                    resolve(mapDataAll);
                });
        });
    }

    getLocationsFiltered(render_environment) {

        let self = this;

        let relatedWorksOptions,
            options,
            mapData = {
                'all' : [],
                'filtered' : []
            };

        return new Promise(function(resolve, reject) {

            options = {
                query : {
                    "type":"location",
                    "render_environments": { $in: [render_environment] }
                },
                "sort": {order: 1}
            };

            self.pagePersistence.find(options).then( function(locations) {

                let mapDataFiltered = self._matchTopicalWorks(locations);
                resolve(mapDataFiltered);
            });
        });
    }

    _matchWorks(locations) {

        let self = this,
            options;

        return Promise.all(locations.map(function (location) {

            return new Promise(function (resolve, reject) {

                options = {
                    query : {
                        "type":"work",
                        "list": true,
                        "taxonomies.locations.slug" : location.slug,
                        "status" : "publish"
                    },
                    "sort": { "start": 1}
                };

                self.pagePersistence.find(options).then( function(works) {

                    // logger.info(works);

                    location.works = works;
                    resolve(location);
                });
            });
        }));
    }

    _matchLocations(works) {

        let self = this,
            locationSlugs,
            options;

        return Promise.all(works.map(function (work) {

            locationSlugs = work.taxonomies.locations.map( (l) => l.slug);

            return new Promise(function (resolve, reject) {

                options = {
                    query : {
                        "type":"location",
                        "slug" : { $in: locationSlugs},
                        "status" : "publish"
                    },
                    "sort": { "start": 1}
                };

                self.pagePersistence.find(options).then( function(locations) {
                    work.locations = locations;
                    resolve(work);
                });
            });
        }));
    }


    _matchTopicalWorks(locations) {

        let self = this,
            options;

    //    let now = moment();

      //  let rubicon = new Date(now.add(3, 'months'));
        let today = new Date();

        // samenvattend . moment is leuk voor optellen. Maar het moet een js Date object zijn dat in de query naar ISOString wordt gecast

        return Promise.all(locations.map(function (location) {

            return new Promise(function (resolve, reject) {

                options = {
                    query : {
                        "type":"work",
                        "list": true,
                        "taxonomies.locations.slug" : location.slug,
                        "status" : "publish",
                        "start" : { "$lte": today.toISOString() },
                        "end" : { "$gte": today.toISOString() }
                    },
                    "sort": { "start": 1}
                };

                self.pagePersistence.find(options).then( function(works) {
                    location.works = works;
                    resolve(location);
                });
            });
        }));
    }
}

module.exports = MapDataService;

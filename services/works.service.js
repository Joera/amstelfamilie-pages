'use strict';

let fs = require('graceful-fs'),
    logger = require('../../services/logger.service');
    const config = require('../../config');
    const PagePersistence = require('../../persistence/page.persistence');
    const MapDataService = require('../services/mapData.service')


/**
 * Service for getting dataset for visualization from the post sections and saving the dataset in a separate file so it can be loaded on the static pages
 */
class WorksService {

    constructor() {
        this.pagePersistence = new PagePersistence();
    }

    async getPlanningData(renderEnvironment) {

        let self = this;
        let relatedWorksOptions,
            list,
            planningData = {
                'datasetSection0': []
            },
            options = {
                query: {
                    "type": "work",
                    "planning": {$in: ['yes', undefined]},
                    "render_environments": {$in: [renderEnvironment]}
                },
                sort: {"start": 1}
            };

        let works = await self.pagePersistence.find(options);

        works.forEach((work, i) => {

            work.name = work.title;
            work.description = work.content;
            work.from = work.start;
            work.to = work.end;
            work.class = work.colour || 'default';
            work.position = i;
            work.optional_title = work.optional_title || false;

            if (work.taxonomies && work.taxonomies.locations && work.taxonomies.locations.length > 0 && work.taxonomies.locations.length < 6) {
                work.taxonomies.locations.forEach((l, i) => {
                    i == 0 ? list = l.name : list = list + ', ' + l.name;
                });
                work.name = list + ': ' + work.title;
            }
        });

        planningData.datasetSection0 = works;

        return planningData;
    }

    async savePlanningData(path,renderEnvironment) {

        let data = await this.getPlanningData(renderEnvironment);
        let datasetPath = config.dist + path + '/planning-data.json';

        await fs.writeFile(datasetPath, JSON.stringify(data), function (error) {
            if (error) {
                logger.error('error writing json file with works');
            } else {
                logger.info('dataset voor planning gemaakt');
            }
        });

        return data;
    }

    getAllWorks(render_environment){

        return new Promise( async (resolve,reject) => {

            const pagePersistence = new PagePersistence();
            const mapDataService = new MapDataService();

            let options = {
                query: {
                    type: "work",
                    render_environments: {$in: [render_environment]},
                },
                "sort": {order: -1}
            };

            let works = await pagePersistence.find(options);
            let augmentedWorks = await mapDataService._matchLocations(works)

            resolve(augmentedWorks);

        });
    }

    _createFeatureCollection(data) {

        return {
            "type": "FeatureCollection",
            "features": this._createFeatures(data, 'location')
        };
    }

    _createFeatures (works, type) {
        let features = [];

        works.forEach( (work) => {

            work.locations.forEach( (location) => {

                let properties = {
                    "name": work.title,
                    "slug": work.slug,
                    "location": location.title,
                    "startDate" : work.start,
                    "endDate" : work.end,
                    "label": location.title,
                    "town": location.town,
                    "address": location.location ? location.location.address : '',
                    'type': work.taxonomies['work-types'][0] ? work.taxonomies['work-types'][0].slug : ''
                };

                let point = this._createPoint(properties, location.location.lng, location.location.lat)

                features.push(point);
            });
        });
        return features
    }

    _createPoint(properties, lat, lng){
        lat = parseFloat(lat);
        lng = parseFloat(lng);
        return {
            "type": "Feature",
            "properties": properties,
            "geometry": {
                "type": "Point",
                "coordinates": [
                    lat,
                    lng
                ]
            }
        }
    }


    async createGeojson(render_environments) {

        if(render_environments && render_environments.length > 0) {

            render_environments.forEach(async (env) => {

                let works = await this.getAllWorks(env);
                let allFeatures = this._createFeatureCollection(works);

                await this._writeFile(allFeatures, env, 'works');
            });
        }
        return;
    }

    async _writeFile(data, env, filename) {


        let url = config.dist + 'amsteltram.nl/assets/geojson/' + env + '_' + filename + '.geojson';

        await fs.writeFile(url, JSON.stringify(data), (err) => {
            // if (err) throw err;
            logger.info(env + '_' + filename + '.geojson has been created at ' + url);
        });

        return;
    }
}


module.exports = WorksService;

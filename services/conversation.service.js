'use strict';
const logger = require('../../services/logger.service');
const BooleanService = require('./boolean.service');

/**
 * Service for authorization of the incoming api calls
 */
class ConversationService {

    assembleThreads(posts) {

        let threads = [],
            thread = {};

        posts.forEach((post) => {

            if (post.interaction.nested_comments != null) {

                post.interaction.nested_comments.forEach((t) => {

                    if (t != null && t.length > 0) {

                        thread = {};
                        thread.comments = t;
                        thread.type = 'comment';
                        thread.post = {};
                        thread.post.id = post.id;
                        thread.post.comment_count = post.comment_count;
                        thread.post.title = post.title;
                        thread.post.url = post.url;
                        threads.push(thread);
                    }
					
                });
            }
        });

        return threads;
    }

    collectComments(posts) {

        const booleanService =  new BooleanService();

        let comments = [];

        for (let post of posts) {

            if (post.interaction && post.interaction.nested_comments != null) {

                for (let thread of post.interaction.nested_comments) {

                    for (let comment of thread) {


                        // did niet via model doen?
                        // of helemaal via algolia?

                        comment.isEditor = booleanService.correct(comment['is_editor']),
                        comment.type = 'comment';
                        comment.post = {};
                        comment.post.id = post._id;
                        comment.post.count = post.interaction.comment_count;
                        comment.post.title = post.title;
                        comment.post.url = post.url;
                        comment.thread = {};
                        comment.thread.id = thread[0].id;
                        comment.thread.count = thread.length;
                        comments.push(comment)

                    }
                }
            }
        }

        return comments;
    }

    processComments(data)  {

        const booleanService =  new BooleanService();

        if (data.interaction.nested_comments && data.interaction.nested_comments.length > 0) {

            for (let thread of data.interaction.nested_comments) {
                for (let comment of thread) {
                   comment.isEditor = booleanService.correct(comment.is_editor);
                }
            }
        }

        return data;
    }
}

module.exports = ConversationService;

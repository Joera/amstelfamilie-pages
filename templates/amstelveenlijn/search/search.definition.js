const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');

module.exports = {

    getTemplateData: async (data) => {

        const footerService = new FooterService();
        data.footer = await footerService.getContent('amstelveenlijn','nl');
        return data;
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: async (data, render_environment) => { // path generator for path/url of the page

            return render_environment + '/zoeken';
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: async (data) => {
            return [];
    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data) => {

        return;
    }
}

const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const logger = require('../../../../services/logger.service');
const LocationService = require('../../../services/location.service');
const moment = require('moment');


module.exports = {


    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId) => {
        return new Promise((resolve, reject) => {

                // let pagePersistence = new PagePersistence();
                //
                // let pageQuery = {
                //     "query" : {
                //         "type":"location"
                //     }
                // };
                // let content = pagePersistence.find(pageQuery);
                //
                // Promise.all([content]).then(values => {
                //     data.locations = values[0];
                //     logger.info('Get template data', correlationId)
                    resolve(data)
                // })
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */

    getPath: (data, correlationId, options) => {
        return new Promise( (resolve, reject) => {
                const pathService = new PathService();
                const path = '/' + pathService.cleanString(data.slug);
                resolve(path);
        })
    },

    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {
            resolve([
                { template: 'werkzaamheden-en-planning', data: null },
                { template: 'homepage', data: null }
            ]);
        })
    }
}
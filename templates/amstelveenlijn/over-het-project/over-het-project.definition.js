const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {


            const pagePersistence = new PagePersistence();
            const footerService = new FooterService();

            let id = '29293';

            let pageQuery = {
                "query" : {
                    "_id": id
                }
            };

            let childrenQuery = {
                "query" : {
                    "parent": parseInt(id)
                }
            };
            let content = pagePersistence.findOne(pageQuery);
            let children = pagePersistence.find(childrenQuery);
            let footerContent = footerService.getContent('amstelveenlijn','nl');

            Promise.all([content,children,footerContent]).then(values => {

                data = values[0];
                data.children = values[1].reverse();
                data.footer = values[2];
                logger.info('Get template data', correlationId)
                resolve(data);
            });
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            // logger.info('Generated post path: ' + path, correlationId);
            resolve(render_environment + '/over-het-project');
        })
    },

    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {
            // logger.info('Get template dependencies for ' + data.type, correlationId);
            // [{template: '', param: ''}]
            // resolve([{template: 'home', data: null}, {template: 'news', data: null}]);
            resolve([]);
        })
    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}

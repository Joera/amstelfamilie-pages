const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const ConversationService = require('../../../services/conversation.service');
const logger = require('../../../../services/logger.service');
const moment = require('moment');

module.exports = {

     getTemplateData: (data, correlationId) => {

         return new Promise((resolve, reject) => {

             const pagePersistence = new PagePersistence();
             const footerService = new FooterService();
             let footerContent = footerService.getContent(data.render_environments[0], 'nl');

             let id = '35712';

             let pageQuery = {
                 "query" : {
                     "_id": id
                 }
             };

             let content = pagePersistence.findOne(pageQuery);

             Promise.all([content,footerContent]).then(values => {

                 data = values[0];
                 data.footer = values[1];
                 resolve(data);
             });
         });
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page

			const year = moment(data.date, 'YYYY').format('YYYY');
            const pathService = new PathService();
            // create urlbase in path service
            const path = render_environment + '/lijn25iser/rsvp/';
            return path;
    },
    
    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            //logger.info('Get template dependencies for ' + data.type, correlationId);
            resolve([]);
        })
    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}

const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const ConversationService = require('../../../services/conversation.service');
const logger = require('../../../../services/logger.service');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId, options) => {

        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            const footerService = new FooterService();
            const conversationService = new ConversationService();

            data = conversationService.processComments(data);

            let childrenQuery = {
                "query" : {
                    "parent": parseInt(data._id)
                }
            };

            let options = {
                query: {
                    type: "post",
                    tagSlugs: {$in: ['lijn25iser','opening','lijn-25']},
                },
                "sort": {date: -1},
                "limit": 6
            };

            let allPostOptions = {
                query: {
                    type: "post",
                    render_environments: {$in: ['amstelveenlijn']}
                },
                "sort": {date: -1},
                "limit": 6
            };

            let homeContentOptions = {
                "query" : {
                    "_id":"33889"
                }
            };





            //    let pageContent = pagePersistence.find(options);
            let children = pagePersistence.find(childrenQuery);
            let relatedPosts = pagePersistence.find(options);
            let allPosts = pagePersistence.find(allPostOptions);
            let footerContent = footerService.getContent(data.render_environments[0],'nl');
            let homeContent = pagePersistence.findOne(homeContentOptions);

            Promise.all([children,relatedPosts,allPosts,footerContent]).then(values => {

                let uniqueArray = [];
                var uniquePosts = values[1].concat(values[2]).filter(function (post, index) {
                    if(uniqueArray.indexOf(post._id) < 0) {
                        uniqueArray.push(post._id)
                        return post;
                    }
                });

                data.children = values[0];
                data.related_posts = uniquePosts.slice(0,6);
                data.footer = values[3];

                data.homeContent = {};

                data.homeContent.main_image = {
                    external_id : 'dc44bbad-0b53-40a0-96c2-09ffae17e1e0',
                    filename : 'GVB_Oddshop_vanWechem_final_3.jpg',
                    alt : '',
                    title: 'lijn25 is er'
                };

                resolve(data);
            });
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            const pathService = new PathService();
            const path = render_environment + '/' + pathService.cleanString(data.slug);
            // logger.info('Generated post path: ' + path, correlationId);
            resolve(path);
        })
    },

    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {
            resolve([
                {template: 'over-het-project', data: null},
                {template: 'page', data: null},
                // { template: 'homepage', data: null }
            ]);
        })
    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}

const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');
const ConversationService = require('../../../services/conversation.service');
const MapDataService = require('../../../services/mapData.service');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId) => {

        return new Promise((resolve, reject) => {

			const pagePersistence = new PagePersistence();
            const conversationService = new ConversationService();
            const mapDataService = new MapDataService();
            const footerService = new FooterService();

            let homeContentOptions = {
                "query" : {
                    "_id":"33889"
                }
            };
            let homeContent = pagePersistence.findOne(homeContentOptions);

            let BlogOptions = {
                "query" : {
                    "type": { $in: ["post","interview"] },
                    "render_environments": { $in: ['amstelveenlijn'] }
                },
                "sort": {'sort_date': -1},
                "limit": 16
            };
            let findPosts = pagePersistence.find(BlogOptions);

			let stickyOptions = {
                "query" : {
                    "type": { $in: ["page","post","interview"] },
					"featured_item": { $in : [true,1,'1']},
                    "render_environments": { $in: data.render_environments }
                },
                "sort": {'date': -1}
            };

            let findStickyPost = pagePersistence.find(stickyOptions);

            let commentPostOptions = {
                query : {
                    "type":"post",
                    "render_environments": { $in: ['amstelveenlijn'] }
                }
            };

            let findCommentPosts = pagePersistence.find(commentPostOptions);
            let mapDataFiltered = mapDataService.getLocationsFiltered();
            let footerContent = footerService.getContent('amstelveenlijn','nl');


			Promise.all([homeContent,findPosts,findStickyPost,findCommentPosts,mapDataFiltered,footerContent]).then(values => {

			    data.homeContent = values[0];
				data.posts = values[1];
				data.featured = values[2][0];
                data.comments = conversationService.collectComments(values[3]);
                data.mapDataFiltered = values[4];
                data.footer = values[5];


                data.mapDataFiltered.sort(function(a, b) {
                    return a['order'] - b['order'];
                });


                data.comments.sort(function (a, b) {

                    if (a.date < b.date) {
                        return 1;
                    }
                    if (a.date > b.date) {
                        return -1;
                    }
                });

                data.campagnebeeld = {
                    external_id : 'dc28c9e0-5df7-46a0-ba4f-b2dd2db65dfd',
                    filename : 'Amstelveenlijn201213EerstedagvanDienstregeling006.jpg',
                    alt : '',
                    title: 'lijn25 is er'
                };

                data.campagne =  {

                    url : 'https://amstelveenlijn.nl/lijn25iser',
                    title: 'Lijn 25 is officieel geopend.',
                    line: '',
                    link: 'Lees verder'
                }
				resolve(data)
			})
        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            // logger.info('Generated post path: ' + path, correlationId);
            resolve(render_environment);
        })
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            resolve([]);
        })
    }
}

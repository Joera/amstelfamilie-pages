const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');
const ConversationService = require('../../../services/conversation.service');
const WorksService = require('../../../services/works.service');
const moment = require('moment');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            const conversationService = new ConversationService();
            const worksService = new WorksService();
            const footerService = new FooterService();

            let homeContentOptions = {
                "query" : {
                    "_id":"32670"
                }
            };
            let homeContent = pagePersistence.findOne(homeContentOptions);

            let BlogOptions = {
                "query" : {
                    "type": { $in: ["post","interview"] },
                    "render_environments": { $in: ['uithoornlijn'] }
                },
                "sort": {'date': -1},
                "limit": 16
            };
            let findPosts = pagePersistence.find(BlogOptions);

			let stickyOptions = {
                "query" : {
                    "type": { $in: ["page","post","interview"] },
					"featured_item": { $in : [true,1,'1']},
                    "render_environments": { $in: data.render_environments }
                },
                "sort": {'date': -1}
            };

            let findStickyPost = pagePersistence.find(stickyOptions);

            let commentPostOptions = {
                query : {
                    "type": { $in: ["post","page","interview"] },
                    "render_environments": { $in: data.render_environments }
                }
            };

            let findCommentPosts = pagePersistence.find(commentPostOptions);
            let footerContent = footerService.getContent(data.render_environments[0],'nl');

            let works = worksService.getAllWorks('uithoornlijn');



			Promise.all([homeContent,findPosts,findStickyPost,findCommentPosts,footerContent,works]).then(values => {

			    data.homeContent = values[0];
				data.posts = values[1];
				data.featured = values[2][0];
				data.comments = conversationService.collectComments(values[3]);
              //  data.threads = conversationService.assembleThreads(values[3]);
                data.footer = values[4];

                data.comments.sort(function (a, b) {

                    if (a.date < b.date) {
                        return 1;
                    }
                    if (a.date > b.date) {
                        return -1;
                    }
                });

                data.comments= data.comments.filter( (c) => {

                    return moment(c.date) > moment('2019-09-01');
                });

                let now = new Date();
                // filter actueel
                data.under_construction = values[5].filter( w => {
                    return new Date(w.end) > now && now > new Date(w.start)
                }).slice(0,3)

                data.under_construction = values[5].filter( w => {
                    return new Date(w.end) > now && now > new Date(w.start)
                });

                data.future_works = values[5].filter( w => {
                    return new Date(w.start) > now;
                });

                for (let i of data.under_construction) {
                    i.future = false;
                }

                for (let i of data.future_works) {
                    i.future = true;
                }

                data.under_construction = data.under_construction.concat(data.future_works).slice(0,3);


				resolve(data)
			})


        })
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            // logger.info('Generated post path: ' + path, correlationId);
            resolve(render_environment);
        })
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {
            logger.info('Get template dependencies for ' + data.type, correlationId);
            // [{template: '', param: ''}]
            // resolve([{template: 'home', data: null}, {template: 'news', data: null}]);
            resolve([]);
        })

    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}

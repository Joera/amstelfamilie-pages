const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');
const moment = require('moment');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId) => {

        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            const footerService = new FooterService();

            let footerContent = footerService.getContent(data.render_environments[0], 'nl');

            let options = {
                        query: {
                            type: "post",
                            tagSlugs: {$in: data.tagSlugs},
                            slug: {$ne: data.slug},
                        },
                        "sort": {date: -1},
                        "limit": 6
                    };

            let allPostOptions = {
                query: {
                    type: "post",
                    render_environments: {$in: ['uithoornlijn']}
                },
                "sort": {date: -1},
                "limit": 3
            };

            let relatedPosts = pagePersistence.find(options);
            let allPosts = pagePersistence.find(allPostOptions);

            Promise.all([relatedPosts,allPosts,footerContent]).then(values => {

                data.related_posts = values[0].concat(values[1]).slice(0,5);
                data.footer = values[2];
                resolve(data);
            });
        });
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page

			const year = moment(data.date, 'YYYY').format('YYYY');
            const pathService = new PathService();
            const path = render_environment + '/nieuws/' + year + '/' + pathService.cleanString(data.slug);
            return path;
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            //logger.info('Get template dependencies for ' + data.type, correlationId);
            resolve([
                { template: 'search', data: null },
                { template: 'homepage', data: null }
            ]);
        })
    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}

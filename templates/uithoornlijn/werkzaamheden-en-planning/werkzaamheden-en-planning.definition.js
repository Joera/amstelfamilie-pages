const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');
const MapDataService = require('../../../services/mapData.service');
const LocationService = require('../../../services/location.service');
const WorksService = require('../../../services/works.service');


module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId) => {

        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            const mapDataService = new MapDataService();
            const worksService = new WorksService();
            const footerService = new FooterService();

            let id = '35626';

            let pageQuery = {
                "query" : {
                    "_id": id
                }
            };

            let locationQuery = {
                "query" : {
                    "type":"location",
                    "render_environments": { $in: ['uithoornlijn'] }
                }
            };


            let content = pagePersistence.findOne(pageQuery);
            let mapDataAll = mapDataService.getLocationsAll('uithoornlijn');
            let mapDataFiltered = mapDataService.getLocationsFiltered('uithoornlijn');
            let locations = pagePersistence.find(locationQuery);
            let footerContent = footerService.getContent('uithoornlijn','nl');
            let works = worksService.getAllWorks('uithoornlijn');
            // let planningData = mapDataService.getPlanningData();

            Promise.all([content,mapDataAll,mapDataFiltered,locations,footerContent,works]).then( function(values) {

                    data = values[0];
                    data.mapDataAll = values[1];

                    data.mapDataFiltered = values[2];
                    // data.planningData = values[3];

                    data.mapDataAll.sort(function(a, b) {
                        return a['order'] - b['order'];
                    });

                    data.mapDataFiltered.sort(function(a, b) {
                        return a['order'] - b['order'];
                    });

                    data.timestamp = new Date().getTime();
                    data.locations = values[3];
                    data.footer = values[4];

                    let now = new Date();
                    // filter actueel
                    data.under_construction = values[5].filter( w => {
                        return new Date(w.end) > now && now > new Date(w.start)
                    });

                    data.future_works = values[5].filter( w => {
                        return new Date(w.start) > now;
                    });



                    resolve(data);
            });
        });
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            resolve(render_environment + '/werkzaamheden-en-planning');
        })
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            resolve([]);
        })

    },
    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        let locationService = new LocationService();
        await locationService.createGeojson(data)['render_environments'];
        return;
    }
}

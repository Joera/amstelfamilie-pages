const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');
const ConversationService = require('../../../services/conversation.service');
const footerService = new FooterService();
const moment = require('moment');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: async (data, correlationId) => {

        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            const conversationService = new ConversationService();
            const footerService = new FooterService();

            let homeContentOptions = {
                "query": {
                    "_id": "32675"
                }
            };

            let homeContent = pagePersistence.findOne(homeContentOptions);

            let BlogOptions = {
                "query": {
                    "type": {$in: ["post", "interview"]},
                    "render_environments": {$in: ['amsteltram']}
                },
                "sort": {'date': -1},
                "limit": 16
            };
            let findPosts = pagePersistence.find(BlogOptions);

            let stickyOptions = {
                "query": {
                    "type": {$in: ["page", "post", "interview"]},
                    "featured_item": {$in: [true, 1, '1']},
                    "render_environments": {$in: data.render_environments}
                },
                "sort": {'date': -1}
            };

            let findStickyPost = pagePersistence.find(stickyOptions);

            let commentPostOptions = {
                query: {
                    "type": {$in: ["post", "interview"]},
                    "render_environments": {$in: data.render_environments}
                }
            };

            let findCommentPosts = pagePersistence.find(commentPostOptions);
            let footerContent = footerService.getContent(data.render_environments[0], 'nl');


            Promise.all([homeContent,findPosts,findStickyPost,findCommentPosts,footerContent]).then(values => {

                data.homeContent = values[0];
                data.posts = values[1];
                data.featured = values[2][0];
                data.comments = conversationService.collectComments(values[3]);
                data.footer = values[4];

                data.comments.sort(function (a, b) {

                    if (a.date < b.date) {
                        return 1;
                    }
                    if (a.date > b.date) {
                        return -1;
                    }
                });

                data.comments= data.comments.filter( (c) => {

                    return moment(c.date) > moment('2019-09-01');
                });


                resolve(data)
            });


       });
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            // logger.info('Generated post path: ' + path, correlationId);
            resolve(render_environment);
        })
    },


    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId) => {
        return new Promise((resolve, reject) => {
          //  logger.info('Get template dependencies for ' + data.type, correlationId);
            // [{template: '', param: ''}]
            resolve([{template: 'search', data: null}]);
           // resolve([]);
        })

    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}

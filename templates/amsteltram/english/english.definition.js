const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');

module.exports = {

    getTemplateData: (data, correlationId) => {

        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            const footerService = new FooterService();

            let englishQuery = {
                query : {
                    type:"post",
                    tagSlugs : { $in : ['english','engels'] }
                },
                "sort": {date: -1},
                "limit": 6
            };

            let posts = pagePersistence.find(englishQuery);
            let footerContent = footerService.getContent(data.render_environments[0],'en');

            Promise.all([posts,footerContent]).then(values => {
                data.related_posts = values[0];
                data.footer = values[1];
                resolve(data)
            });
        });
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            // logger.info('Generated post path: ' + path, correlationId);
            resolve('english');
        })
    },

    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId) => {
        return new Promise((resolve, reject) => {
            // logger.info('Get template dependencies for ' + data.type, correlationId);
            // [{template: '', param: ''}]
            // resolve([{template: 'home', data: null}, {template: 'news', data: null}]);
            resolve([]);
        })
    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}
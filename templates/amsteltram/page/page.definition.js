const PagePersistence = require('../../../../persistence/page.persistence');
const PathService = require('../../../../services/path.service');
const FooterService = require('../../../services/footer.service');
const logger = require('../../../../services/logger.service');

module.exports = {

    /**
     * Gets the data for the template
     * @param data
     */
    getTemplateData: (data, correlationId, options) => {

        return new Promise((resolve, reject) => {

            const pagePersistence = new PagePersistence();
            const footerService = new FooterService();

            //    let pageContent = pagePersistence.find(options);
            let footerContent = footerService.getContent(data.render_environments[0],'nl');

            Promise.all([footerContent]).then(values => {

                data.footer = values[0];
                resolve(data);
            });
        });
    },

    /**
     * Path generator function for the url/path of the page
     * function creates the path/url of the page
     * @param data
     */
    getPath: (data, render_environment, correlationId) => { // path generator for path/url of the page
        return new Promise((resolve, reject) => {
            const pathService = new PathService();
            const path = render_environment + '/' + pathService.cleanString(data.slug);
            // logger.info('Generated post path: ' + path, correlationId);
            resolve(path);
        })
    },

    /**
     * Determine page dependencies
     * Return object array of templates that also need to be rendered
     * Object in array contain template property with the name of the template and data property to pass data
     * to the template that needs to be rendered.
     * @param data
     * @param correlationId
     */
    getDependencies: (data, correlationId, options) => {
        return new Promise((resolve, reject) => {
            logger.info('Get template dependencies for ' + data.type, correlationId);

            resolve([
                {template: 'over-het-project', data: null},
                { template: 'homepage', data: null },
                { template: 'search', data: null }
            ]);

            // if (data.slug === 'nieuwe-homepage') {
            //     resolve([
            //         { template: 'homepage', data: null },
            //     ]);
            // } else if(data.slug === 'english') {
            //
            //     resolve([
            //         { template: 'english', data: null },
            //     ]);
            // } else {
            //     resolve([
            //         // {template: 'homepage', data: null},
            //         // {template: 'werkzaamheden-en-hinder', data: null},
            //         // {template: 'over-het-project', data: null},
            //     ]);
            // }
        })
    },

    /**
     * Executed after rendering template
     * @param html              html of the rendered template
     * @param path              path of the rendered template
     * @param data              template data
     * @param correlationId
     */
    postRender: async (html, path, data, correlationId) => {

        return;
    }
}

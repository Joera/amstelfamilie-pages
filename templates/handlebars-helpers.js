'use strict';

const moment = require('moment');
const logger = require('../../services/logger.service');

module.exports = [{
        name: 'ifEquals',
        helper: (a, b, options) => {
            if (a === b) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        }
    },
    {
        name: 'ifNot',
        helper: (a, b, options) => {
            if (a === b) {
                return options.inverse(this);
            } else {
                return options.fn(this);
            }
        }
    },
    {
        name: 'or',
        helper: (first, second) => {
            return first || second;
        }
    },
    {
        name: 'ifValue',
        helper: (conditional, options) => {
            if (options.hash.value === conditional) {
                    return options.fn(this);
                } else {
                    return options.inverse(this);
                }
        }
    },
    {
        name: 'formatDate',
        helper: (datetime, format) => {
            let dateFormats = {
               short: "DD MMM YYYY",
               long: "dddd D MMMM YYYY",
               time: "D MMMM hh:mm:ss",
               year: "YYYY",
               comment: "D MMMM YYYY | HH:mm"
            };
            if (moment) {
                format = dateFormats[format] || format;
                return moment(datetime).locale('nl').format(format);
            }
            else {
                return datetime;
            }
        }
    },
    {
        name: 'formatUnix',
        helper: (datetime, format) => {
            let dateFormats = {
                short: "DD MMM YYYY",
                long: "dddd D MMMM YYYY",
                time: "D MMMM hh:mm:ss",
                year: "YYYY",
                comment: "D MMMM YYYY | HH:mm"
            };
            if (moment) {
                format = dateFormats[format] || format;
                return moment.unix(datetime).locale('nl').format(format);
            }
            else {
                return datetime;
            }
        }
    },
    {
        name: 'quarterify',
            helper: (startDate, endDate) => {

            let Qstart,Qend,Ystart,Yend, cStart,cEnd;

            if (moment) {
                if(moment(startDate).format('DD-MM').indexOf('01-01') > -1) {
                    logger.info('quarterify start');
                    Qstart = moment(startDate).quarter();
                    cStart = true;

                } else {
                    logger.info('normal start');
                    Qstart = moment(startDate).format('D MMM');
                    cStart = false;
                }

                Ystart = moment(startDate).year();

                if(moment(endDate).format('DD-MM').indexOf('01-01') > -1) {
                    logger.info('quarterify end');
                    Qend = moment(endDate).quarter();
                    cEnd = true;
                } else {
                    Qend = moment(startDate).format('D MMM');
                    cEnd = false;
                }

                Yend = moment(endDate).year();

                if (Ystart === Yend) {
                    if (Qend - Qstart < 1) {
                        return 'Q' + Qstart + ' ' + Ystart;
                    } else {
                        return 'Q' + Qstart + ' - Q' + Qend + ' ' + Yend;
                    }

                } else {
                    return 'Q' + Qstart + ' ' + Ystart + ' - Q' + Qend + ' ' + Yend;
                }
            }
            else {
                return datetime;
            }
        }
    },
    {
        name: 'quarterifyDate',
            helper: (date) => {

            let newDate;

            if (moment) {

                moment.locale('nl')

                if(

                    moment(date).format('DD-MM').indexOf('01-01') > -1 ||
                    moment(date).format('DD-MM').indexOf('01-04') > -1 ||
                    moment(date).format('DD-MM').indexOf('01-07') > -1 ||
                    moment(date).format('DD-MM').indexOf('01-10') > -1 ||

                    moment(date).format('DD-MM').indexOf('31-03') > -1 ||
                    moment(date).format('DD-MM').indexOf('30-06') > -1 ||
                    moment(date).format('DD-MM').indexOf('30-09') > -1 ||
                    moment(date).format('DD-MM').indexOf('31-12') > -1
                ) {

                    newDate = 'Q' + moment(date).quarter() + ' ' + moment(date).year();

                } else {

                    newDate = moment(date).format('D MMMM YYYY');
                }

                return newDate;
            }
            else {
                return date;
            }
        }
    },
    {
        name: 'if_equals',
        helper: (a, b, opts) => {
            if (a === b) {
                return opts.fn(this);
            } else {
                return opts.inverse(this);
            }
        }
    },
    {
        name: 'limit',
        helper: (arr, limit) => {
            if (arr) {
                return arr.slice(0, limit);
            } else {
                return;
            }
        }
    },
    {
        name: 'offset',
        helper: (arr, offset) => {
            if(arr && arr.constructor === Array) {
                return arr.slice(offset, arr.length);
            } else {
                return;
            }
        }
    },
    {
        name: 'parseReplies',
            helper: (arr) => {
            if (arr) {
                let newArr = [];
                for (let i = 0; i < arr.length; i++) {
                    if (i > 0 && newArr.indexOf(arr[i].name) < 0) {
                        newArr.push(arr[i].name);
                    }
                }
                return newArr;
            } else {

                return;
            }
        }
    },
    {
        name: 'sortDescBy',
        helper: (arr, variable) => {
            
            if (arr && Array.isArray(arr)) {
                    arr.sort(function(a, b) {
                        if (moment(a[variable]) < moment(b[variable])) { return 1; }
                        if (moment(a[variable]) > moment(b[variable])) { return -1; }
                        return 0;
                    });
                    return arr;
                } else {

                    return;
                }
        }
    },
    {
        name: 'counter',
        helper: (index) => {
            return index + 1;
        }
    },

    {
        name: 'sortBy',
        helper: (arr,variable,param) => {
            if (arr) {
                    arr.sort(function(a, b) {
                        if (moment(a[variable][param]) < moment(b[variable][param])) { return 1; }
                        if (moment(a[variable][param]) > moment(b[variable][param])) { return -1; }
                        return 0;
                    });
                    return arr;
                } else {
                    return;
                }
        }
    },

    {
        name: 'count',
        helper: (array) => {
            if (array) {
                return array.length;
            } else {
                return 0;
            }
        }
    },
    {
        name: 'ifIn',
            helper: (elem, list, options) => {
            if (list.indexOf(elem) === -1) {
                return options.inverse(this);
            }
            return options.fn(this);
        }
    },
    {
        name: 'unlessIn',
        helper: (elem, list, options) => {
            if (list.indexOf(elem) > -1) {
                return options.inverse(this);
            }
            return options.fn(this);
        }
    },
    {
        name: 'ifMoreThan',
        helper: (a, b, options) => {
            if (parseInt(a) > b) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        }
    },
    {
        name: 'ifLessThan',
        helper: (a, b, options) => {
            if (parseInt(a) < b) {

                return options.fn(this);
            } else {

                return options.inverse(this);
            }
        }
    },
    {
        name: 'ifLongerThan',
        helper: (a, b, options) => {
            if (Array.isArray(a) && a.length > parseInt(b)) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        }
    },
    {
        name: 'ifShorterThan',
        helper: (a, b, options) => {
            if (Array.isArray(a) && a.length < parseInt(b)) {
                return options.fn(this);
            } else {
                return options.inverse(this);
            }
        }
    },
    {
        name: 'formatDate',
        helper: (datetime, format) => {
            let DateFormats = {
                short: "DD MMM YYYY",
                long: "dddd D MMMM YYYY",
                time: "D MMMM hh:mm:ss",
                year: "YYYY",
                comment: "D MMMM YYYY | HH:mm",
                comment_small: "D/M HH:mm"
            };

            if (moment) {
                // can use other formats like 'lll' too
                format = DateFormats[format] || format;
                return moment(datetime).locale('nl').format(format);
            } else {
                return datetime;
            }
        }
    },
    {
        name: 'ifTOC',
        helper: (sections,options) => {

            if (Array.isArray(sections)) {
                for (let section of sections) {
                    if (section.type === 'chapter') return options.fn(this);
                }
            }
            return options.inverse(this);
        }
    },
    {
        name: 'containerClass',
        helper: (type,images,orientation,format) => {
            if (type === 'documents' || type === 'form' || type === 'table-of-contents-heading') {
                return 'full-width';
            } else if (type === 'datavisplanning' || (type === 'image_and_text' && format === 'large')) {
                return 'large-container';
            } else if(

                type === 'streamer' ||
                (type == 'images' && format === 'large' && Array.isArray(images) && images.length > 1) ||
                (type == 'images' && format === 'large') ) {

                return 'large-container';

            } else if(type === 'video' && format === 'larger' ) {

                logger.debug('1')
                return 'full-container';

            } else if(type === 'video' && format === 'large' ) {

                logger.debug('2')
                return 'large-container';

            } else {
                logger.debug('3')
                return 'small-container';
            }
        }
    },
    {
        name: 'imageFormat',
            helper: (index,count,orientation) => {

            if (count === 'single') {

                return '';

            } else if (count === 'duo') {

                if ((orientation === 'left' && index === 0) || (orientation === 'right' && index === 1) || orientation === 'center') {

                    return 'camera';

                } else {

                    return 'camera';
                }
            } else if (count === 'trio') {

                return'camera';

            } else {

                return'camera';
            }
        }
    },
    {
        name: 'prerendered',
        helper: (template, lan,env) => {
            if (lan && lan !== 'nl' && lan !== undefined) {
                template = template + '-' + lan;
            }
            return fs.readFileSync('/opt/sg-core/pages/templates/' + env + '/prerendered/' + template + '.html');
        }
    },
    {
        name: 'excerpt',
        helper: (content) => {
            if (content !== null && content !== undefined) {
                let strippedFromHtml = content.toString().replace(/(&nbsp;|<([^>]+)>)/ig, "");
                let sentences = strippedFromHtml.split(".");
                let newContent;
                if (sentences[0].length > 140) {
                    newContent = sentences[0] + '.';
                } else if (sentences[1] && sentences[0].length + sentences[1].length > 140) {
                    newContent = sentences[0] + '.' + sentences[1] + '.';
                } else if (sentences[1] && sentences[2] && sentences[0].length + sentences[1].length + sentences[2].length > 140) {
                    newContent = sentences[0] + '.' + sentences[1] + '.' + sentences[2] + '.';
                } else {
                    newContent = sentences[0] + '.' + sentences[1] + '.';
                }
                return newContent;
            } else {
                return '';
            }
        }
    },
    {
        name: 'searchExcerpt',
        helper: (content) => {
            if (content !== null && content !== undefined) {
                let strippedFromHtml = content.toString().replace(/(&nbsp;|<([^>]+)>)/ig, "");
                let newContent = strippedFromHtml.slice(0, 200);
                if (newContent.length > 200) {
                    newContent = newContent + ' ...';
                }
                return newContent;
            } else {
                return '';
            }
        }
    },
    {
        name: 'shortenTitle',
        helper: (content) => {
            if (content !== null && content !== undefined) {
                let strippedFromHtml = content.toString().replace(/(&nbsp;|<([^>]+)>)/ig, "");
                return strippedFromHtml.slice(0, 14) + '...';
            } else {
                return '';
            }
        }
    },
    {
        name: 'stripHTML',
        helper: (content) => {
            if (content !== null && content !== undefined) {
                let strippedFromHtml = content.toString().replace(/(&nbsp;|<([^>]+)>)/ig, "");
                return strippedFromHtml;
            } else {
                return '';
            }
        }
    },
    {
        name: 'stripFromHashTags',
        helper: (content) => {
            if (content !== null && content !== undefined) {
                let strippedFromHashTags = content.replace(/^(\s*#\w+\s*)+$/gm, "")
                return strippedFromHashTags;
            } else {
                return '';
            }
        }
    },
    {
        name: 'explode',
        helper: (array) => {
            if (array && array.length > 0) {
                let mappedArray = array.map(object => {
                    return object.slug;
                });
                return mappedArray.join(' ');
            } else {
                return '';
            }
        }
    },
    {
        name: 'capitalize',
        helper: (words) => {
            if (words !== undefined && words !== null && typeof words !== 'object') {

                words = words.toLowerCase()
                    .split(' ')
                    .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
                    .join(' ');
                return words;
            } else {
                return '';
            }
        }
    },
    {
        name: 'lowercase',
        helper: (words) => {
            if (words !== undefined && words !== null && typeof words !== 'object') {
                return words.toLowerCase();
            } else {
                return '';
            }
        }
    },
    {
        name: 'sluggify',
        helper: (words) => {
            if (words !== undefined && words !== null && typeof words !== 'object') {
                return words.toLowerCase().replace(/[^a-zA-Z ]/g, "").trim().split(' ').join('-');
            } else {
                return 'fout in naam';
            }
        }
    },
    {
        name: 'anchorify',
        helper: (words) => {
            if (words !== undefined && words !== null && typeof words !== 'object') {
                return words.toString().replace(/(&nbsp;|<([^>]+)>)/ig, "").toLowerCase().replace(/[^a-zA-Z ]/g, "").trim().split(' ').slice(0, 3).join('-');
            } else {
                return 'fout-in-anchor';
            }
        }
    },
    {
        name: 'index_of',
        helper: (context, index) => {
            return context[index];
        }
    },
    {
        name: 'log',
        helper: (data) => {
            return '<script>console.log(' + data + ');</script>';
            // return '<script></script>';
        }
    },
    {
        name: 'removeDoubleSlash',
            helper: (content) =>
        {
            if (content && content !== null && content !== 'https://amstelveenlijn.nl/') {
                let parts = content.split("//", 3);
                if(parts.length > 1) {
                    let str = parts[0] + "//" + parts[1].replace(/\/\//g, "/")
                    if(parts[2]) {
                        str = str + '/' + parts[2];
                    }
                    return str;
                } else {
                    return content;
                }
            } else if(content === 'http://amstelveenlijn.nl/' || content === 'https://amstelveenlijn.nl/' || content === 'https://amstelveenlijn.nl')  {
                return 'https://amstelveenlijn.nl';
            }  else {
                return '';
            }
        }
    },
    {
        name: 'removeDomain',
        helper: (url) => {
            if (url !== null && url !== undefined) {
                return url.replace(/^.*\/\/[^\/]+/, '');
            } else {
                return '';
            }
        }
    },
    {
        name: 'highlight',
        helper: (content, term) => {
            return content
        }
    },
    {
        name: 'json',
        helper: (context) => {
            return JSON.stringify(context);
        }
    },
    {
        name: 'concat',
        helper: (string1, string2) => {
            return string1 + string2;
        }
    },
    {
        name: 'slice',
        helper: (context, block) => {

            var ret = "";

            if (context !== undefined && block !== undefined) {
                let offset = parseInt(block.hash.offset) || 0,
                    limit = parseInt(block.hash.limit) || 5,
                    i = (offset < context.length) ? offset : 0,
                    j = ((limit + offset) < context.length) ? (limit + offset) : context.length;

                for (i, j; i < j; i++) {
                    ret += block.fn(context[i]);
                }
            }

            return ret;
        }
    },
];

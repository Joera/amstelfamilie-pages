const logger = require('../../services/logger.service');

module.exports = {


    getDefinition: () => {

        const definition = {
            name: 'over-het-project',
            template: 'over-het-project.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

            return {
                _id: data.id,
                objectID: data.id, // id for algolia search
                type: data.type,
                slug: data.slug,
                url: data.url, // replace wordpress generated url for page
                status: data.status,
                title: data.title.rendered || data.title,
                content: data.content.rendered || data.content,
                excerpt: data.excerpt.rendered || data.excerpt,
                sections: data.sections,
                date: data.date,
                modified: data.modified,
                children: data.children,
                render_environments: ['amstelveenlijn'],
                language: data.language
            };
    },

    getSearchMapping: (data) => {

        const searchObject = {

            objectID: data._id || data.id,
            template: 'search-snippet',
            type: 'post',
            slug: data.slug,
            url: data.link,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            sections: data.sections,
            render_environments: ['amstelveenlijn'],
            language: data.language
        }

        Array.isArray(searchObject.sections) ? searchObject.sections = searchObject.sections.filter( v => { return v.type == 'paragraph' }) : searchObject.sections = [];

        return searchObject;


    }
}

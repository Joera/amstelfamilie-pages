const logger = require('../../services/logger.service');
const WorksService = require('../services/works.service');
const BooleanService = require('../services/boolean.service');
const RemoteServerConnector = require('../../connectors/remoteserver.connector');




module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'work',
            template: ''
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        const booleanService =  new BooleanService();

        return {
            _id: data.id,
            objectID: data.id,
            type: data.type,
            slug: data.slug,
            url: data.url,
            status: data.status,
            title: data.title.rendered || data.title,
            content: data.content.rendered || '',
            date: data.date,
            modified: data.modified,
            start: new Date(data['time_period'].start).toISOString(),
            end: new Date(data['time_period'].end).toISOString(),
            colour: data['time_period'].colour,
            list: booleanService.correct(data['time_period'].list),
            planning: data['time_period'].planning || 'yes',
            optional_title: data['time_period'].optional_title || false,
            taxonomies: data.taxonomies,
            order: data['menu_order'],
            render_environments: data.render_environments,
        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            _id: data._id || data.id,
            objectID: data._id || data.id,
            template: ''
        }

        return searchObject;
    },

    postRender: async (data) => {

        const worksService = new WorksService();
        const remoteServerConnector = new RemoteServerConnector();
        await worksService.createGeojson(data['render_environments']);
      //  await worksService.savePlanningData('amstelveenlijn/werkzaamheden-en-planning');
     //   await remoteServerConnector.copyDataset('amstelveenlijn/werkzaamheden-en-planning','amstelveenlijn');
        await worksService.savePlanningData('uithoornlijn/werkzaamheden-en-planning','uithoornlijn');
        await remoteServerConnector.copyDataset('uithoornlijn/werkzaamheden-en-planning','uithoornlijn');
        // remote
        return data;
    }

}

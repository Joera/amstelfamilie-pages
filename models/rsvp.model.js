const logger = require('../../services/logger.service');

module.exports = {


    getDefinition: () => {

        const definition = {
            name: 'rsvp',
            template: 'rsvp.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return {
                _id: data.id,
                objectID: data.id, // id for algolia search
                type: data.type,
                slug: data.slug,
                url: data.url, // replace wordpress generated url for page
                status: data.status,
                title: data.title.rendered || data.title,
                content: data.content.rendered || data.content,
                excerpt: data.excerpt.rendered || data.excerpt,
                date: data.date,
                modified: data.modified,
                language: data.language || { 'code' : 'nl'},
                render_environments: data.render_environments,

            };
    },

    getSearchMapping: (data) => {

        const searchObject = {

        };

        // trim sections to text only

        return searchObject;
    }
}

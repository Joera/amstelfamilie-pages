const logger = require('../../services/logger.service');

module.exports = {


    getDefinition: () => {

        const definition = {
            name: 'page',
            template: 'page.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return {
                _id: data.id,
                objectID: data.id, // id for algolia search
                type: data.type,
                slug: data.slug,
                url: data.link, // replace wordpress generated url for page
                status: data.status,
                title: data.title.rendered || data.title,
                content: data.content.rendered || data.content,
                excerpt: data.excerpt.rendered || data.excerpt,
                date: data.date,
                modified: data.modified,
                main_image: data.main_image.external_id ? data.main_image : {},
                sections: data.sections,
                interaction: data.interaction,
                language: data.language || { 'code' : 'nl'},
                parent: parseInt(data.parent) || 0,
                children: data.children || [],
                faq: data.faq || [],
                render_environments: data.render_environments,

            };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data._id || data.id,
            template: 'search-snippet',
            type: 'page',
            slug: data.slug,
            url: data.link,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            sections: data.sections,
            render_environments: data.render_environments,
            language: data.language ||  { 'code' : 'nl'}

        };

        // trim sections to text only
        Array.isArray(searchObject.sections) ? searchObject.sections = searchObject.sections.filter( v => { return v.type == 'paragraph' }) : searchObject.sections = [];

        return searchObject;
    }
}

const logger = require('../../services/logger.service');
const LocationsService = require('../../pages/services/location.service');


module.exports = {


    getDefinition: () => {

        const definition = {
            name: 'location',
            template: ''
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return  {
                _id: data.id,
                objectID: data.id, // id for algolia search
                type: data.type,
                slug: data.slug,
                url: data.url,
                status: data.status,
                title: data.title.rendered || data.title,
                content: data.content.rendered || data.content,
                date: data.date,
                modified: data.modified,
                taxonomies : data.taxonomies,
                location: data.location,
                types: data.taxonomies['location-types'],
                stopType:  data.taxonomies['stop-type'],
                mainType: data.taxonomies['location-types'][0],
                order:  data['menu_order'],
                render_environments: data.render_environments,
             };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            _id: data.id,
            template: ''

        }

        return searchObject;
    },

    postRender: async(data) => {

        const locationsService =  new LocationsService();
        await locationsService.createGeojson(data['render_environments']);
        return;
    }
}


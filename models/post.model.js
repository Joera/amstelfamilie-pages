
const logger = require('../../services/logger.service');
const _ = require('lodash');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'post',
            template: 'post.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return {

            _id: data.id,
            objectID: data.id, // id for algolia search
            type: data.type,
            slug: data.slug,
            url: data.link,
            status: data.status,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            excerpt: data.excerpt.rendered || data.excerpt,
            date: data.date,
            sort_date: data.sort_date || data.date,
            modified: data.modified,
            author: data.author,
            main_image: data.main_image,
            sections: data.sections,
            interaction: data.interaction,
            taxonomies: data.taxonomies,
            language: data.language,
            catIds: (data.taxonomies && data.taxonomies.categories) ? data.taxonomies.categories.map( c => { return c.id }) : [],
            catSlugs: (data.taxonomies && data.taxonomies.categories) ? data.taxonomies.categories.map( c => { return c.slug }) : [],
            tagIds: (data.taxonomies && data.taxonomies.tags) ? data.taxonomies.tags.map( c => { return c.id }) : [],
            tagSlugs: (data.taxonomies && data.taxonomies.tags) ? data.taxonomies.tags.map( c => { return c.slug }) : [],
            render_environments: data.render_environments,
            featured_item: data.featured_item,
            updates: data.updates
        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data._id || data.id,
            template: 'search-snippet',
            type: 'post',
            slug: data.slug,
            url: (data.url && data.url !== '') ? data.url : data.link,
            title: data.title.rendered || data.title,
            date: data.date,
            sort_date: data.sort_date,
            content: data.content.rendered || data.content,
            excerpt: data.excerpt.rendered || data.excerpt,
            main_image: data.main_image,
            sections: data.sections,
            render_environments: data.render_environments,
            language: data.language,
            taxonomies: data.taxonomies || [],
            updates: data.updates
        }

     //   Array.isArray(searchObject.sections) ? searchObject.sections = searchObject.sections.filter( v => { return v.type == 'paragraph' }) : searchObject.sections = [];

        return searchObject
    }

}

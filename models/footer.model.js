const logger = require('../../services/logger.service');
const BulkrenderController = require('../../controllers/bulkrender.controller');

module.exports = {


    getDefinition: () => {

        const definition = {
            name: 'footer',
            template: ''
        }

        return definition;
    },

    getObjectMapping:  (data) => {

        return {
                _id: data.id,
                objectID: data.id, // id for algolia search
                type: data.type,
                slug: data.slug,
                url: data.url, // replace wordpress generated url for page
                status: data.status,
                title: data.title.rendered || data.title,
                content: data.content.rendered || data.content,
                excerpt: data.excerpt.rendered || data.excerpt,
                extra: data.extra,
                date: data.date,
                modified: data.modified,
                main_image: data.main_image,
                language: data.language || {},
                render_environments: data.render_environments,

            };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data.id,
            template: ''

        };

        // trim sections to text only

        return searchObject;
    },

    postRender: async (data) => {

        // do bulkrender
        const bulkrenderController = new BulkrenderController();
        await bulkrenderController.render(false,data.render_environments[0]);
        return;
    }
}

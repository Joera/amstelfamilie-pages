const logger = require('../../services/logger.service');
const _ = require('lodash');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'werkzaamheden-en-planning',
            template: 'werkzaamheden-en-planning.handlebars'
        }

        return definition;
    },

    getObjectMapping: (data) => {

        return {
            _id: data.id,
            objectID: data.id, // id for algolia search
            type: data.type,
            slug: data.slug,
            url: data.url, // replace wordpress generated url for page
            status: data.status,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            excerpt: data.excerpt.rendered || data.excerpt,
            date: data.date,
            modified: data.modified,
            sections: data.sections,
            main_image: data.main_image,
            square_image: data.square_image,
            location: data.location,
            render_environments: data.render_environments
        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            objectID: data._id || data.id,
            template: 'search-snippet',
            type: data.type,
            slug: data.slug,
            url: data.link,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            sections: data.sections,
            render_environments: ['amstelveenlijn'],
            language: data.language,
            taxonomies: data.taxonomies || [],
            main_image: data.main_image
        }

        Array.isArray(searchObject.sections) ? searchObject.sections = searchObject.sections.filter( v => { return v.type == 'paragraph' }) : searchObject.sections = [];

        return searchObject;
    }
}

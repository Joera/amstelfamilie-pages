
const logger = require('../../services/logger.service');
const BooleanService = require('../../pages/services/boolean.service');
const slugify = require('slugify');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'comment',
            template: ''
        }

        return definition;
    },

    getObjectMapping:  (data) => {},

    getSearchMapping: (comment,thread,data) => {

        const booleanService =  new BooleanService();

        const searchObject = {

            objectID : comment.id,
            date : new Date(comment.date.replace('T', ' ')).getTime(),
            type : 'comment',
            language : data.language,
            title : comment.content.slice(0,120).replace(/<(?:.|\n)*?>/gm, ''),
            slug : slugify(comment.content.slice(0,40).replace(/<(?:.|\n)*?>/gm, '')),
            content : comment.content.replace(/<(?:.|\n)*?>/gm, ''),
            author : comment.name,
            isEditor : booleanService.correct(comment['is_editor']),
            comments : thread.filter( t => { t.id != comment.id }),
            reply_count : thread.filter( t => { t.id != comment.id }).length,
            postID : data._id,

            url : (data.link && data.link !== '') ? data.link + '#dialoog' : data.url + '#' + thread[0].id,
            render_environments: data.render_environments
        }


        return searchObject
    }
}


const logger = require('../../services/logger.service');
const slugify = require('slugify');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'document',
            template: ''
        }

        return definition;
    },

    getObjectMapping:  (document) => {

        const dataObject = {

            objectID : document.id, // keep algolia id consistent
            _id : document.id,
            type : 'document',
            title: document.title.rendered || document.title,
            slug: slugify(document.title.rendered || document.title),
            date: document.file.date,
            sort_date: document.sort_date,
            url: document.cdn.url,
            render_environments: document.file.render_environments,
            language : {
                code: 'nl'
            }
        }

        return dataObject
    },

    getSearchMapping: (document) => {

        const searchObject = {

            objectID : document._id || document.id, // keep algolia id consistent
            _id : document.id,
            type : 'document',
            template: 'document-snippet',
            title: document.title.rendered || document.title, // first open is reguler publish .. second is bulksearch
            date: (document.file === undefined) ? document.date : document.file.date,
            url: document.url || document.cdn.url,
            render_environments: (document.file === undefined) ? document.render_environments : document.file.render_environments,
            language : {
                code: 'nl'
            }
        }

        // deze uitzetten bij sync

        logger.debug(searchObject.date);
      //  if(searchObject.date) { searchObject.date = new Date(searchObject.date.replace('T', ' ')).getTime(); }

        return searchObject
    }
}

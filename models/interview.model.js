const logger = require('../../services/logger.service');

module.exports = {

    getDefinition: () => {

        const definition = {
            name: 'interview',
            template: 'interview.handlebars'
        }

        return definition;
    },

    getObjectMapping:  (data) => {
        return {
            _id: data.id,
            objectID: data.id, // id for algolia search
            type: data.type,
            slug: data.slug,
            url: data.url, // replace wordpress generated url for page
            status: data.status,
            title: data.title.rendered || data.title,
            content: data.content.rendered || data.content,
            excerpt: data.excerpt.rendered || data.excerpt,
            date: data.date,
            modified: data.modified,
            author: data.author,
            main_image: data.main_image,
            sections: data.sections,
            interaction: data.interaction,
            taxonomies: data.taxonomies,
            language: data.language,
            catIds: (data.taxonomies && data.taxonomies.categories) ? data.taxonomies.categories.map( c => { return c.id }) : [],
            catSlugs: (data.taxonomies && data.taxonomies.categories) ? data.taxonomies.categories.map( c => { return c.slug }) : [],
            tagIds: (data.taxonomies && data.taxonomies.tags) ? data.taxonomies.tags.map( c => { return c.id }) : [],
            tagSlugs: (data.taxonomies && data.taxonomies.tags) ? data.taxonomies.tags.map( c => { return c.slug }) : [],

            featured_item: data.featured_item,
            quote: data.quote
        };
    },

    getSearchMapping: (data) => {

        const searchObject = {
            template: 'search-snippet',

        }

        return searchObject;
    }
}
